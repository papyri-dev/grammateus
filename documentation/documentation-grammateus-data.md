# Grammateus Data

## Contents
1. [Introduction](#introduction)
1. [Creation of the Corpus](#creation-of-the-corpus)
1. [Data Collection](#data-collection)
1. [XML Files](#xml-files)

## Introduction
This document describes the creation and processing of data for the Grammateus project. The data is gathered into matrices from various sources (e.g. papyri.info, HGV, printed editions, measures from digital facsimile). The matrices are then converted to XML files and loaded into an eXist database. Data subject to update - such as text transcriptions, date and place of origin - will be accessed directly from papyri.info to display on each papyrus display page. However, we are including HGV date and place of origin in our XML so that this information can be indexed and searched.

## Creation of the Corpus
The Grammatus project focuses on Greek documentary papyri from Greaco-Roman Egypt, which represent over 60’000 texts written between the third century BC and the seventh century AD. An exhaustive coverage of the material available is not the aim: instead, we are working on a limited corpus starting with easily identifiable clusters of papyri for which we could expect to find a representative sample of images. We are planning to progressively increase the size of the corpus as the research advances, to include documents that do not fit into a standard description.

Two clusters of papyri were first selected for the constitution of the corpus:
1. Papyri from the Petaus Archive, i.e. documents belonging to a village secretary from the second century AD. This batch offers both some degree of homogeneity (several documents follow the same pattern and can be described in the same typological terms) and some differences (there are reports, but also lists, requests etc.; documents are displayed in one or two columns; some documents are embedded within another document [e.g. a letter quoted in a report]).
2. Documents from the Zenon Archive published in the series *Papiri della Società Italiana*. Dating from the third century BC, they display also a good mixture of homogeneity and differences. They are for the most part letters and requests (the latter in a format [hypomnema] which allows comparison with the Petaus material). Some documents are written on vertical sheets of papyrus, following the direction of fibres; others were prepared on horizontal sheets, against the direction of fibres.

It was decided from the beginning that the selection of papyri to be included in the corpus had to be complete, and have a good image accessible either on-line or in print. Documents that are incomplete or for which there is no image available cannot help us assess the usual layout of their document category.

The chronological spread for the first part of the project is between 300BC and 300AD, to be possibly extended at a later time, and the geographical limit is all of Egypt.

The papyri and their metadata are first recorded in Microsoft Excel spreadsheet. For a better version control on gitlab, we have moved from Excel files to tab-separated matrices. These matrices are then transformed into TEI XML following the [EpiDoc](https://www.stoa.org/epidoc/gl/latest/) standard. The matrices are now stored on a Google drive to avoid conflicts when updating them.

### Corpus Coverage - Stage 1
The first stage of the corpus was created in the Spring of 2019. It contains 154 papyri from the Petaus Archive and the Zenon Archive, divided by type of document into 7 matrices.

**Summary**

|Nb    | Documents             | Archive| File  |
|------| --------------------- |--------| ------|
|17    | Census returns        | Petaus | [CensusReturns.txt](matrices/textfiles/CensusReturns.txt) |
|62    | Curstoms receipts     | Petaus | [CustomsReceipts.txt](matrices/textfiles/CustomsReceipts.txt) |
|12    | Libelli               | Petaus | [LibelliMatrix.txt](matrices/textfiles/LibelliMatrix.txt) |
|13    | Notifications of death| Petaus | [NotificationsOfDeath.txt](matrices/textfiles/NotificationsOfDeath.txt) |
|16    | Sales documents from Pathyris  | Petaus | [PathyrisSalesDocs.txt](matrices/textfiles/PathyrisSalesDocs.txt) |
|19    | Hypomnemata to Zenon  | Zenon  | [ZenonArchiveHypomnemataToZenon.txt](matrices/textfiles/ZenonArchiveHypomnemataToZenon.txt) |
|15    |Letters from Apollonios| Zenon  | [ZenonArchiveLettersFromApollonios.txt](matrices/textfiles/ZenonArchiveLettersFromApollonios.txt) |

### Corpus Coverage - Stage 2
Since Autumn 2019, the corpus was extended into a second phase, with documents from all of Egypt, and from all categories identified for our [typology](data/grammateus_taxonomy.xml).

The process of selecting documents is also improved by querying HGV EpiDoc files for keywords in the titles or subjects.

**Summary**

As of June 2021, there are 37 matrices, and a total of 1065 papyri in the database. All text matrices are found in this [folder](matrices/textfiles).

## Data Collection
For each papyri, the following metadata were recorded:
- identification numbers
- canonical references
- Format: type, subtype and variation as per the grammateus typology of documents
- image link
- fibre direction
- dimensions
- organisation of the text on the page (margins, blanks, text sections)

The data is recorded in spreadsheets where each row represent one papyrus, and each column represent one piece of metadata as described below. Spreadsheet are used for the convenience of recording the data. For a better version control, the final matrices are stored in TSV format.

### Identification Numbers
We have included three identification numbers, in use in digital papyrology documents.

1. **TM** -
The Trismegistos number is the main identifier of the papyrus, as it is the most stable reference.

2. **HGV** -
*Heidelberger Gesamtverzeichnis der Griechischen Papyrusurkunden Ägyptens* provides a stable reference for date and location where each document is checked and corrected if necessary. It is rare that the HGV number differs from the TM number, but it can happen (e.g. P.Adler 13 is TM 13, which corresponds to both HGV 13a and 13b). The concatenation of `https://papyri.info/hgv/`+HGV+`/source` lets us access the HGV EpiDoc source file, with the encoded date and place of origin: for instance `https://papyri.info/hgv/2293/source`

3. **DDB** -
A link to the papyrus on the Duke Database of Documentary Papyri. The concatenation of `https://papyri.info/ddbdp/`+DDB identifier+`/source` lets us access the XML source of the papyrus on [papyri.info](papyri.info): for instance `https://papyri.info/ddbdp/p.cair.zen;1;59002/source`

One problem identified with DDB identifiers is that they are subject to change. Within the course of the project, we noticed for instance that TM 11973 and 12078 at least. It will have an impact on the long term functionality of the website, since the dynamic access to the latest transcription was deemed an essential feature.


### Canonical references
This includes series, volume, and number. We also included fascicle, side and generic, and page, which may be needed to distinguish a recto from a verso for instance. Here are a few examples:

| Series       | Volume   | Fascicle | Number | Side | Generic | Page |
| ------------ |:--------:|:--------:|:------:|:----:|:-------:|:----:|
| P. Cair. Zen | 1        |          | 59002  |      |         |      |
| P. Adler     |          |          | 13     |      |         |      |
| P.Lond. 		 | 2        |          | 346    |      | a       |      |
| P.Mich. 		 | 2        |          | 121    |  v   |         |      |
| P.Gen . 		 | 1        | (2e éd.) | 18     |      |         |      |
| Claytor BASP | 50       | 2013     |        |      |         | 74   |

Depending on when, or by whom, the matrix was created, some information might be missing from the fascicle, side, generic or page column. The Series, Volume and Number columns are always supplied.

Each reference is recorded in Arabic numerals, following papyri.info format for consistency. In the case of customs receipts, which have been reprinted in a single volume, we have either their original series (P.Rein. 2 95) or the more recent P.Customs series, following again papyri.info.

### Format: Type, Subtype and Variation
We started to work with the typology developed by Joanne Vera Stolk, who kindly shared the classification scheme she created for Trismsegistos. We lightly edited the subtype list to add categories that were not present in Trismegistos' typology.

As we progress in our understanding of papyri classification, we have regularly updated this [typology](data/grammateus_taxonomy.xml).

In the first part of the project, we were still making a distinction between the format typology (our classification that takes into account material aspects of papyri), with a textual typology based on the textual content only. This explains why the early matrices have a distinction between `format-type` and `text-type`. Later on, as there was virtually no difference between the two typologies, we kept only the format one. 

### Keywords
In the matrices we record the original keywords from HGV (hgv-subjects), mostly for reference purposes. We had started to keep a simplified version of the HGV keywords in German, but it was stopped as no one was using this feature. We keep an English translation of the most important keyword, for the full-text search.

### Image Link
Most of our papyri exist as a digital facsimile, and we have included the link to the image that can be found on [papyri.info](papyri.info). This field was left empty when the image could only be accessed through the printed edition. We have ignored papyri for which the link of papyri.info was broken and no image was accessible. Although we may record as well a reference to a printed image, for the purpose of selecting the papyri that can be added to our corpus, only links will go into our EpiDoc encoding.

It can happen that there is more than one link to record. For instance TM 131 is so wide that there are two pictures, with two links, to the facsimile. In that case the two links are separated by the string ` and `.

When available, we also record a link to a IIIF manifest. The manifests currently in our database all come from the library of the University of Manchester.

### Fibre direction
The fibre direction was obtained from the observation of facsimiles. It is recorded as `fh` (horizontal), `fv` (vertical), or `fm` (mixed) in the rare cases when there is a combination of vertical and horizontal fibres, for instance if two sheets of papyrus were pasted together. Mixed fibres papyri are represented with a left and right half with different fibre direction. For a precise view of how the horizontal and vertical fibres are really arranged, the user should check the image of the papyri.

### Dimensions
Dimensions include the height and width in centimeters. Where measurements are provided via the image link, or in the printed edition, or on papyri.info directly, these are the ones recorded (in this order of preference). In case of discrepancies between those measurements, the printed edition was chosen as the reference. Where a ruler is attached to the image and no measurement given, the measurements are taken from the highest to lowest part of the papyrus, and at the widest points of the papyrus.

For convenience purposes, the shape was also recorded in the matrices, although it will not be inlcuded in the EpiDoc encoding. The shape of a papyrus is either VR (vertical), HR (horizontal), or Sq (squarish), and is calculated from the width:height aspect ratio:
- a ratio of 0.8 or lower is vertical
- a ratio of 1.2 or higher is horizontal
- a ratio between 0.8 and 1.2 is squarish

### Organisation of the Text
This category of metadata consists of several aspects that pertains to the text:
- writing position
- margins
- blanks
- seal
- hands
- text sections

Originally, the  list of lines with eisthesis (indent) and ekthesis (outdent) were also recorded, but it was left aside because the complexity and inconsistencies found in documents.

Some matrices have two additional columns with Year and Provenance. This information is here for convenience, but the data displayed in the final web resource will be fetched from HGV.

**writing position**  
This column records whether the papyrus is written along the short or long edge. This information is not recorded in the XML file, and is mostly redundant with the shape: vertical papyri are written by definition along the short edge, and horizontal papyri along the long edge.

**margins**  
Here we record the space between the text and the top, bottom, left and right edges of the document. Very large bottom margins can in reality be a blank space at the end of the document (to which further writing could have been added if required), but it is impossible to make a distinction between this blank space and the margin cf. e.g. P. Cair. Zen. III 59384, 59406. It is therefore measured as a margin. While every effort has been made to have exact measurements recorded, some margin of error must be allowed.

- Where measurements are provided on papyri.info directly, or via the image link, or in the printed edition, these are the ones recorded.
- Where a ruler is attached to the image and no measurements given, margins are measured from the obvious edge of the main text (which will not include letter flourishes) to the widest edge of papyrus; for very wide documents, such as Pathyris sales contracts, the margins can vary by as much as 1 cm – however for the sake of consistency in measurement the ink closest to the edge remains the starting point
for the margin;
- Where there is no ruler attached to the image, the margins are measured using Gimp:
  - the image is imported and the size set to 100%.
  - ektheseis, which by definition encroach into the margin, are ignored and the general alignment of the margin is measured.
  - In some instances the photo cuts off an edge – the only option is to measure to the edge of the photo.

**blank spaces**
We record lines which are followed by a blank space, as a list of comma-separated numbers. The line numbers correspond to the attribute `@n` of the element `<lb/>` in papyri.info XML edition. A 'blank height' column may record the vertical size of the blank space. The blank height was not always recorded, and is not included in the encoding. If something was added later in the blank space, we record the number of the last added line, and measure the size of what is left of the blank space.

**Seal**  
We record the presence or absence of a seal or stamp. Although this may usually appear in the papyri.info XML edition, we did find some rare occurrences of a seal that was absent from the transcription:
- TM 13741 and 28535: the seal is noted in the metadata, but not in the
transcription;
- TM 14361 and 28271: the seal is not encoded at all.

**hands**  
One column records the number of hands present in the papyrus. For each hand, a separate column includes:
- the ID of section(s) written by this hand;
- the ID of the scribe who wrote the section, if known.

The scribes ID are listed in the [authority lists file](/data/authority-lists.xml). ID numbers are separated by whitespace, and section IDs are separated from scribes ID by a slash, such as:

`#section1 #section2 #section3/S09`

**text sections**  
One column records the number of hands present in the papyrus. For each hand, a separate column includes:
- the line numbers of the section (from-to). If the section is one line long or less it will be recorded for instance as `1-1';
- the ID of the hand(s) which wrote the section;
- a section name.

The section names are listed in the [authority lists file](/data/authority-lists.xml). ID numbers are separated by whitespace, and line numbers/hand IDs/section name are separated by a slash, such as:

`1-9/#m1 #m2/main-text`

We try to follow the papyri.info numbering, but it is not always possible due to inconsistencies in the encoding. For instance, lines with only "vac.?" are sometimes counted as a line, and sometimes not. For this we decided to count them regardless of whether they are counted or not on papyri.info. Other lines that should be ignored are lines with:
* only one or more seal
* only a vacat
* only a gap of @extent unknown and @unit line
* only a gap of @unit line, @reason illegible (traces) e.g. 22479, but in other cases the traces are counted as lines (e.g. 9513)
* only a gap of @unit line, @reason lost and @precision low (ca. traces)
* only a note (219272)
* only a milestone of @rend box, @unit undefined (e.g. 8789)
* lines with a lettter (e.g 33a) 
* lines with a slash (e.g. 32/33), as they are not a unique line but a combination, not recommended as an encoding and likely to disappear from papyri.info (TM12078)
* lines which are in the margins
* lines hich are encoded twice (because within a choice/rdg/del element)

Inconsistencies on papyri.info may be due to the fact that it is a collaborative project many scholars are contributing to, which makes it difficult to avoid small inconsistencies. It can also be caused by the editions, which papyri.info is always using as the reference, and which may have differences in the way they count lines depending on the editor.

Papyri.info does not count lines with a single X glyph. However we sometimes need to include this kind of lines as we are marking them as a separate text section (official mark).

We always count lines from start to end, even if on papyri.info the numbering may start from one again in different columns or fragments. It is again an inconsistent behaviour. Some papyri that had all columns in one transcription were separated into several XML files (e.g. TM 11973), making it even more difficult.

[TODO: update when we have come up with a solution with this problem of columns]

## XML Files
### EpiDoc Template
We are following [EpiDoc guidelines version 9](http://www.stoa.org/epidoc/gl/9/). An [EpiDoc template](/transformation-script/grammateus-template.xml) was created, to be filled with data from the matrices. Each papyrus generates one EpiDoc file from the template.

Some metadata are straightforward to encode, such as identification numbers.

In other cases, we needed to include in the TEI header what would usually be part of the transcription. The reason for that choice is that we rely on papyri.info to get the most up-to-date transcription. We do not want to develop a parallel resource to papyri.info with our data. As a result, we cannot include any element in the transcription, but only point to line numbers of an existing transcription.

**fibre direction**  
Encoded as an attribute `@ana` of the element `<support>`: `<support ana="../authority-lists.xml#fh">`

**blank spaces**  
Example from TM 13951:
```xml
<ab type="blank">
    <locus from="15" to="16"/>
    <locus from="19" to="20"/>
    <locus from="23" to="24"/>
</ab>
```
In this document there are three blank spaces visible, between lines 15-16, 19-20 and 23-24. We point to line numbers with a `<locus>` element.

**text sections**  
Example from TM 1145:
```xml
<ab type="section">
    <locus xml:id="section1" from="1" to="2" corresp="#m1" ana="../authority-lists.xml#introduction"/>
    <locus xml:id="section2" from="2" to="31" corresp="#m1" ana="../authority-lists.xml#main-text"/>
    <locus xml:id="section3" from="32" to="32" corresp="#m1" ana="../authority-lists.xml#subscription"/>
</ab>
```

**hand description**  
Example from TM 13951 again:
```xml
<handDesc>
    <handNote xml:id="m1" corresp="#section1 #section2 #section3" scribeRef="../authority-lists.xml#S10"/>
    <handNote xml:id="m2" corresp="#section3" scribeRef="../authority-lists.xml#S01"/>
    <handNote xml:id="m3" corresp="#section3" scribeRef="../authority-lists.xml#S02"/>
</handDesc>
```

**type and subtype (and variation)**
To encode the format type and subtype, as well as optional variation, we are using the `<catRef>` elements:

```xml
<profileDesc>
    <textClass>
        <catRef n="format-type" scheme="#grammateus_taxonomy" target="#gt_ti"/>
        <catRef n="format-subtype" scheme="#grammateus_taxonomy" target="#gt_decl"/>
        <catRef n="format-variation" scheme="#grammateus_taxonomy" target="#gt_decl_camel"/>
        ...
    </textClass>
</profileDesc>
```

**keywords**
Keywords are also saved in the `<profileDesc>`:

```xml
<profileDesc>
    <textClass>
        ...
        <keywords scheme="grammateus">
            <term xml:lang="en">Receipt of Inheritance</term>
            <term xml:lang="en">Cheirographon</term>
        </keywords>
    </textClass>
</profileDesc>
```


**external links**  
We use XInclude to incorporate three files:
  - [Links to HGV XML](data/external_links_history.xml) files with historical data (date and place of origin)
  - [Links to digital facsimiles](data/external_links_images.xml)
  - [Links to DDbDP XML](data/external_links_texts.xml) files with text transcription

Three elements have an `@corresp`attribute that creates the link between the EpiDoc file and the corresponding elements in the external files.

```xml
<!-- historical information : get from papyri.info -->
<include xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:tei="http://www.tei-c.org/ns/1.0" href="../external_links_history.xml"/>
<history corresp="#link_history_91"/>
...
<facsimile>
    <include xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:tei="http://www.tei-c.org/ns/1.0" href="../external_links_images.xml"/>
    <!-- if image available -->
    <graphic corresp="#link_facs_91"/>
</facsimile>
...
<!--text : get from papyri.info -->
<include xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:tei="http://www.tei-c.org/ns/1.0" href="../external_links_texts.xml"/>
<div type="edition" xml:lang="greek" xml:space="preserve" corresp="#link_text_91"/>
```

The external links are encoded as items of a list, each link in a `<ref>` element. IIIF manifests have the `@type` 'iiif':
```xml
<list xmlns="http://www.tei-c.org/ns/1.0" type="external_links" subtype="img">
...
<item xml:id="link_facs_12907">
  <ref target="https://luna.manchester.ac.uk/luna/servlet/view/search?q=metadata_schema=12907"/>
  <ref target="https://luna.manchester.ac.uk/luna/servlet/iiif/m/ManchesterDev~93~3~24479~100399/manifest" type="iiif"/>
</item>
...
</list>
```

This solution was suggested by Jean-Paul Rehr. It has the benefit of making maintenance of broken links easier, and to facilitate pulling data from papyri.info.

### Authority Lists
In the [authority file](data/authority-lists.xml), we have encoded several lists:
  - People involved in the project
  - Scribes identified from the Decian Libelli
  - Fibres abbreviations
  - Text sections and their definitions

## Python Transformation
[`csv2epidoc.py`](transformation-script/csv2epidoc.py) is the Python script that transforms a list of tsv matrices into EpiDoc encoded files, as well as the external links files.

The Python installation used is version 3.7.1.final.0, shipped with the [Anaconda](https://www.anaconda.com/) distribution version 2018.12.

The main package needed for the transformation is lxml (version 4.2.5) for loading and manipulating xml files. Openpyxl (version 2.5.12) was used in the python script [`xls2epidoc.py`](transformation-script/xls2epidoc.py) to work with Excel matrices, but is not necessary for transforming TSV files.

The script follows basic principles:
1.	open each file in the `/matrices/textfiles` folder and read them line by line
2.	create a list of papyri with their metadata
3.	use that metadata to fill in the EpiDoc template
4.	save each papyrus as a new XML EpiDoc file
5.	update the the three external links file with cross-references


### Usage
Command to use in the anaconda command line:
`python csv2epidoc.py ../matrices/textfiles`

1.	`python` - tells the computer that we are using the python programming language
2.	`csv2epidoc.py` - path to the python script described above
3.	`../matrices` - optional: path to a directory that contains all matrices to be transformed. `..matrices/textfiles` is the default folder that will be used if no folder is specified.

It should be noted that the script will exit if the folder does not exist or is empty. However, errors may happen if the matrices are not tab-separated text files, or if the data is not formatted as expected by the transformation script. Errors will also be raised if the empty external link files are needed but are not in the same directory as the script.

---
All data is saved in the "data" directory of this repository. An XQuery script then imports the content of this directory directly into the eXist application. See the [app documentation](documentation-grammateus-app.md) for a description of the webapp.
