xquery version "3.1";

import module namespace console="http://exist-db.org/xquery/console";

declare namespace tei = "http://www.tei-c.org/ns/1.0";
declare namespace functx = "http://www.functx.com";

declare option exist:serialize "method=html media-type=text/html";

declare function functx:substring-before-if-contains
  ( $arg as xs:string? ,
    $delim as xs:string )  as xs:string? {

   if (contains($arg,$delim))
   then substring-before($arg,$delim)
   else $arg
 } ;

(:  Cheirographon

χειρογραφ
χαιρ  +  ομολογ

:)

let $files :=
    for $row at $i in doc("/db/apps/matrices-RL/html-tables/cheirographon.html")//tr[@class eq "result-record"] 
    let $fileinfo := $row/td[@class eq "identifier"]/a/@href
                => substring-before("?")
    
    (: papyri with multiple HGVs and TMs to be eliminated ? :)
    let $filepath := if (contains($fileinfo, "ddbdp/o.")) then ()
(:                    else if (contains($fileinfo, "p.bub;1;1")) then ():)
                    else if (contains($fileinfo, "ddbdp")) 
                    then "https://papyri.info"||$fileinfo||"source"
                    else ()
    
    return
        $filepath

return 

let $result :=
    for $f in $files
(:    let $log := console:log($f):)
    (: ddbdp document :)
    let $document := doc($f)
    
    let $hgv_nbs := $document//tei:idno[@type eq "HGV"]/text()
    let $hgv := functx:substring-before-if-contains($hgv_nbs, " ")
    
    let $hgvpath := "https://papyri.info/hgv/"||$hgv||"/source"
    
(:    let $log := console:log($ddb):)
(:    let $log := console:log($hgvpath):)
    
    (: HGV document :)
    let $doc := doc($hgvpath)

     (: identifiers, HGV info :)
let $tm := $doc//tei:idno[@type eq 'TM']
let $hgv := $doc//tei:idno[@type eq 'filename']
let $date-high := if ($doc//tei:origDate[@notBefore]) then $doc//tei:origDate/@notBefore/string() else $doc//tei:origDate
let $date-low := if ($doc//tei:origDate[@notAfter]) then $doc//tei:origDate/@notAfter/string() else $doc//tei:origDate
let $ddb := $doc//tei:idno[@type eq 'ddb-hybrid']
let $papyri-info := "https://papyri.info/ddbdp/"||$ddb
let $hgv-title := $doc//tei:titleStmt/tei:title/string()
let $hgv-provenance := $doc//tei:origPlace
let $hgv-subjects := string-join($doc//tei:term, ',')
let $image := string-join($doc//tei:graphic/@url/string(), ' and ')
let $printed-image := $doc//tei:bibl[@type eq 'illustrations']

(:let $log := if ($tm) then () else console:log($hgvpath):)

(: dimensions from apis catalogue :)
let $apis-dimension := ()
(:if (matches($ddb/string(), '[a-z0-9\.;]') and not(matches($ddb/string(), '[A-Z\s\(\)]')) and doc-available($papyri-info||'/source') and not(doc($papyri-info||'/source')//tei:ref[@type eq 'reprint-in'])) then:)
(:let $htmlpage := unparsed-text($papyri-info):)
(:let $apisID := substring-after($htmlpage, '<meta property="dc:relation" content="http://papyri.info/apis/') => substring-before('/source'):)
(:let $apis-doc := doc('https://papyri.info/apis/'||$apisID||'/source'):)
(:return $apis-doc//tei:support/string():)
(:else ():)
            
(: title info :)
let $bibl := $doc//tei:bibl[matches(@subtype, 'principal', 'i')]
let $series := $bibl//tei:title[@level eq 's' and @type eq 'abbreviated']/string()
let $volume := $bibl//tei:biblScope[@type eq 'volume']/string()
let $fascicle := $bibl//tei:biblScope[@type eq 'fascicle']/string()
let $numbers := $bibl//tei:biblScope[@type eq 'numbers']/string()
let $side := $bibl//tei:biblScope[@type eq 'side']/string()
let $generic := $bibl//tei:biblScope[@type eq 'generic']/string()
let $page := $bibl//tei:biblScope[@type eq 'pages']/string()
let $number := $bibl//tei:biblScope[@type eq 'number']/string()
            
(: typology :)
let $format-type := ""
let $format-subtype := ""
let $format-variation := ""
    
    

order by xs:integer($tm) ascending, $tm

return
    <doc>
        <TM>{$tm}</TM>
        <HGV>{$hgv}</HGV>
        <date-high>{$date-high}</date-high>
        <date-low>{$date-low}</date-low>
        <hgv-provenance>{$hgv-provenance}</hgv-provenance>
        <DDB>{$ddb}</DDB>
        <papyri-info>{$papyri-info}</papyri-info>
        <series>{$series}</series>
        <volume>{$volume}</volume>
        <fascicle>{$fascicle}</fascicle>
        <numbers>{$numbers}</numbers>
        <side>{$side}</side>
        <generic>{$generic}</generic>
        <page>{$page}</page>
        <number>{$number}</number>
        <hgv-title>{$hgv-title}</hgv-title>
        <format-type>{$format-type}</format-type>
        <format-subtype>{$format-subtype}</format-subtype>
        <format-variation>{$format-variation}</format-variation>
        <hgv-subjects>{$hgv-subjects}</hgv-subjects>
        <hgv-en></hgv-en>
        <iframe></iframe>
        <IIIF></IIIF>
        <canvasID></canvasID>
        <image>{$image}</image>
        <printed-image>{$printed-image}</printed-image>
        <image-rights></image-rights>
        <fibres></fibres>
        <height>{$apis-dimension}</height>
        <width>{$apis-dimension}</width>
        <shape></shape>
        <columns></columns>
        <seal></seal>
        <hand-nb></hand-nb>
        <section-nb></section-nb>
        <m1></m1>
        <m2></m2>
        <m3></m3>
        <m4></m4>
        <m5></m5>
        <m6></m6>
        <m7></m7>
        <m8></m8>
        <m9></m9>
        <m10></m10>
        <m11></m11>
        <section1></section1>
        <section2></section2>
        <section3></section3>
        <section4></section4>
        <section5></section5>
        <section6></section6>
        <section7></section7>
        <section8></section8>
        <section9></section9>
        <section10></section10>
        <section11></section11>
        <section12></section12>
        <section13></section13>
        <section14></section14>
        <section15></section15>
        <section16></section16>
        <section17></section17>
        <section18></section18>
        <section19></section19>
        <section20></section20>
    </doc>
    
let $filter_result := $result

return
    <html>
        <p>Total papyri: {count(collection("/db/apps/idp-data-hgv/"))}</p>
        <p>Results: {count($filter_result)}</p>
    <table>
        <tr>
            {for $elem in $filter_result[1]/*
                return
                    <th>{$elem/name()}</th>
            }
        </tr>
        {for $r in $filter_result
            return
                <tr>
                    {for $info in $r/*
                        return
                            <td>{$info}</td>
                    }
                </tr>
        }
    </table>
    </html>

