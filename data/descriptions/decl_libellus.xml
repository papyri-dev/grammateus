<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="../schema/gramm.rng"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="d10">
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title>Description of Greek Documentary Papyri: Declaration of Pagan Sacrifice
                    (libellus)</title>
                <author corresp="authority-lists.xml#SF">
                    <forename>Susan</forename>
                    <surname>Fogarty</surname>
                </author>
                <author corresp="../authority-lists.xml#PS">
                    <forename>Paul</forename>
                    <surname>Schubert</surname>
                </author>
                <respStmt>
                    <resp>Encoded in TEI P5 by</resp>
                    <name corresp="authority-lists.xml#EN">
                        <forename>Elisa</forename>
                        <surname>Nury</surname>
                    </name>
                </respStmt>
                <respStmt>
                    <resp>With the collaboration of</resp>
                    <name corresp="../authority-lists.xml#LF">
                        <forename>Lavinia</forename>
                        <surname>Ferretti</surname>
                    </name>
                </respStmt>
                <funder>
                    <orgName key="SNF">Swiss National Science Foundation</orgName> - <ref target="https://data.snf.ch/grants/grant/182205">Project 182205</ref>
                </funder>
            </titleStmt>
            <publicationStmt>
                <authority>Grammateus Project</authority>
                <idno type="DOI">10.26037/yareta:vtub4v2gyrh6xh5phsj5bqk4je</idno>
                <availability>
                    <licence target="https://creativecommons.org/licenses/by-nc/4.0/">Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)</licence>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <p>Born digital document. Encoded from a Microsoft Word document.
                    Previous versions on Yareta:
                    <list>
                        <item>
                            Ferretti, L., Fogarty, S., Nury, E., and Schubert, P. 2021. <title>Description of Greek Documentary Papyri: Declaration of Pagan Sacrifice</title>. grammateus project. DOI: 
                            <idno type="DOI">10.26037/yareta:dlyt3qvm35au5l53eq3pgyh4hm</idno>
                        </item>
                    </list>
                </p>
            </sourceDesc>
        </fileDesc>
        <encodingDesc>
            <classDecl>
                <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:tei="http://www.tei-c.org/ns/1.0" href="../grammateus_taxonomy.xml"/>
            </classDecl>
            <p>This file is encoded to comply with TEI Guidelines P5, Version 4.0.0. <ref target="https://tei-c.org/guidelines/p5/">https://tei-c.org/guidelines/p5/</ref>
            </p>
        </encodingDesc>
        <profileDesc>
            <langUsage>
                <language ident="lat">Latin</language>
                <language ident="grc">Greek</language>
            </langUsage>
            <textClass>
                <catRef n="type" scheme="#grammateus_taxonomy" target="#gt_ti"/>
                <catRef n="subtype" scheme="#grammateus_taxonomy" target="#gt_decl"/>
                <catRef n="variation" scheme="#grammateus_taxonomy" target="#gt_decl_libellus"/>
            </textClass>
        </profileDesc>
        <revisionDesc>
            <change who="../authority_lists.xml#EN" when="2023">Archived on Yareta</change>
            <change who="authority-lists.xml#EN" when="2020-07-22">TEI file created</change>
            <change who="authority-lists.xml#SF" when="2020-07-22">Word document</change>
        </revisionDesc>
    </teiHeader>
    <text>
        <body>
            <head>Declaration of Pagan Sacrifice</head>
            <head type="subtitle">
                <term xml:lang="lat">libellus</term>
            </head>
            <p xml:id="d10_p1">Declarations of pagan sacrifice, otherwise known as <term xml:lang="lat">libelli</term>, were issued by village officials as proof that
                the applicant had sacrificed to the gods in their presence. Following the edict
                issued by the emperor Decius in 249 CE, this statement of sacrifice was required by
                all in the Roman Empire, and has been interpreted by some as the first <quote>centrally
                    organised</quote> persecution of Christians <bibl>
                    <author>Rives</author>
                    <ptr target="gramm:rives1999"/>
                    <citedRange>136</citedRange>
                </bibl>.</p>
            <p xml:id="d10_p2">Although an empire-wide requirement, only a small number of <term xml:lang="lat">libelli</term> survive from Egypt and only from the summer of 250
                CE; the greatest concentration of these are from Theadelphia (34 cases), fewer from
                the wider Arsinoite nome (8 cases, 1 probable), and 4 examples from the Oxyrhynchite
                nome; see <bibl>
                    <author>Schubert</author>
                    <ptr target="gramm:schubert2016"/>
                    <citedRange>192-194</citedRange>
                </bibl>. The cluster of documents from Theadelphia may have been stored together
                (compare the similarity of the damage on [<ref type="internal" target="12907.xml">12907</ref>] and [<ref type="internal" target="13936.xml">13936</ref>]), and
                points to the possibility that they were copies preserved by the officials, not
                those kept by individual applicants, see <bibl>
                    <author>Schubert</author>
                    <ptr target="gramm:schubert2016"/>
                    <citedRange>176</citedRange>
                </bibl>. For an overview of the Decian persecution see [<ref type="external" target="https://papyri.info/biblio/68691">Rives 1999</ref>] and for a closer look
                at the papyri themselves see <bibl>
                    <author>Schubert</author>
                    <ptr target="gramm:schubert2016"/>
                </bibl>.</p>
            <div xml:id="d10_div1_structure" n="1" type="section">
                <head xml:id="structure">Structure</head>
                <p xml:id="d10_p3">As a formal declaration made by an individual to an official, the
                    structure is that of a standard <term xml:lang="grc">ὑπόμνημα</term>. This
                    consists of an address in the dative, <term xml:lang="grc">τοῖς ἐπὶ τῶν θυσιῶν
                        ᾑρημένοις</term>, <gloss>to the (officials) in charge of the
                        sacrifice</gloss>, followed by the declarant’s name <seg type="syntax" corresp="../authority_lists.xml#introduction" xml:id="d10_s1">[from <term xml:lang="grc">παρά</term> + <hi rend="italic">name</hi>
                        &lt;gen.&gt;]</seg> and place of origin or residence.</p>
                <p xml:id="d10_p4">In documents from the Arsinoite nome there follows a declaration
                    by the applicant: <term xml:lang="grc">ἀεὶ θύουσα τοῖς θεοῖς διετέλεσα</term>
                    <gloss>I have always sacrificed to the gods</gloss> e.g. [<ref type="internal" target="56431.xml" corresp="#d10_t9">56431</ref> l.4-5]. The formula at this point is slightly
                    different in declarations from the Oxyrhynchite nome where the applicant states
                    that he has always sacrificed, but has also made libations to the the gods,
                        <term xml:lang="grc">ἀεὶ μὲν θύων καὶ σπένδων τοῖς θεοῖς διετέλεσα</term>,
                    e.g. [<ref type="external" target="https://papyri.info/ddbdp/p.oxy;4;658">20403</ref> l.6-8]. See <bibl>
                        <author>Schubert</author>
                        <ptr target="gramm:schubert2016"/>
                        <citedRange>187</citedRange>
                    </bibl>.</p>
                <p xml:id="d10_p5">In all documents a statement follows that the declarant has
                    sacrificed again before the officials: <term xml:lang="grc">ἐπὶ παρόντων/παροῦσι
                        ὑμῖν</term> (Arsinoite nome) or <term xml:lang="grc">ἐνώπιον ὑμῶν</term>
                    (Oxyrhynchite nome), <gloss>in your presence</gloss>. Then a formal request is
                    made for the officials to certify the sacrifice: <term xml:lang="grc">ἀξιῶ ὑμᾶς
                        ὑποσημειώσασθαί μοι</term>, <gloss>I ask that you certify me
                    below</gloss>.</p>
                <p xml:id="d10_p6">The request ends with a <gloss corresp="#d10_s2">farewell</gloss>
                    <seg type="syntax" corresp="../authority_lists.xml#subscription" xml:id="d10_s2">[<term xml:lang="grc">διευτυχεῖτε</term>]</seg>. Officials confirm that
                    they have seen the individual make his sacrifice (<term xml:lang="grc">εἴδαμέν
                        σε θυσιάζουσαν</term>) - in the declarations from Theadelphia the two
                    officials are Aurelius Serenus and Aurelius Hermas - and this is followed by the
                        <seg corresp="../authority_lists.xml#addition" type="syntax" xml:id="d10_s4">[signature]</seg> of one of them, usually Hermas, the
                    village scribe. Finally the document is dated; those declarations where the <seg type="syntax" corresp="../authority_lists.xml#date" xml:id="d10_s3">[date@end]</seg> has been preserved have almost always been written in the
                    month of <term xml:lang="lat" xml:id="d10_t9" corresp="#d10_s3">Pauni</term> 250
                    CE, others in the following month, <term xml:lang="lat" xml:id="d10_t10" corresp="#d10_s3">Epeiph</term>, i.e. June – July [<ref target="13951.xml" type="internal" corresp="#d10_t10">13951</ref>].</p>
                <p xml:id="d10_p7">The declarations from Theadelphia are significant in that
                    different hands can be clearly distinguished in their preparation e.g. [<ref type="internal" target="11978.xml">11978</ref>, <ref type="internal" target="12907.xml">12907</ref>, <ref type="internal" target="13936.xml">13936</ref>] <list type="unordered">
                        <item>a professional scribe wrote in advance the address, statement of
                            sacrifice, request for certification, and the date at the end, in a
                            trained but fast hand;</item>
                        <item>the confirmation statement is written by the official’s scribe at the
                            time of the sacrifice;</item>
                        <item>the signature of the official, Hermas, is sometimes abbreviated and in
                            a very clumsy hand, e.g. [<ref type="internal" target="13936.xml">13936</ref> l.15, <ref type="internal" target="13940.xml">13940</ref> l.17].</item>
                    </list> For a more detailed examination of the hands involved in this group of
                    documents see <bibl>
                        <author>Schubert</author>
                        <ptr target="gramm:schubert2016"/>
                        <citedRange>179-184</citedRange>
                    </bibl>.</p>
                <p xml:id="d10_p8">The structure of the <term xml:lang="lat">libelli</term> from
                    other Arsinoite villages and towns is based on the same model as those from
                    Theadelphia, with only minor differences: <list type="unordered">
                        <item>sometimes the age and identifying features of the applicant are
                            included e.g. [<ref type="external" target="https://papyri.info/ddbdp/chr.wilck;;124">9033</ref> l. 5,
                            age and scar; <ref type="internal" target="56431.xml">56431</ref> l.
                            2-4, no father, names of mother and husband];</item>
                        <item>after the <q>farewell</q> of the main text, the applicant formally
                            submits his request - a statement common in other declarations submitted
                            to an authority in the <term xml:lang="grc">ὑπόμνημα</term> format -
                                (<term xml:lang="grc">ἐπιδέδωκα</term> - <gloss>I have
                                submitted</gloss>);</item>
                        <item>while the officials confirm the sacrifice, rarely is there a signature
                            e.g. [<ref type="internal" target="44425.xml">44425</ref>]; conversely,
                            one example has the signatures of six officials [<ref type="external" target="https://papyri.info/ddbdp/p.wisc;2;87">13730</ref> l. 14-19].</item>
                    </list>
                </p>
                <p xml:id="d10_p9">The four <term xml:lang="lat">libelli</term> preserved from the
                    Oxyrhynchite nome conform to the same structure as those from the wider
                    Arsinoite nome, apart from the difference in the declaration formula (see
                    above), and a variation in the address to the official: see for example [<ref type="external" target="https://papyri.info/ddbdp/p.oxy;4;658">204</ref>
                    <ref type="external" target="https://papyri.info/ddbdp/p.oxy;4;658">03</ref>]
                        <term xml:lang="grc">τοῖς ἐπὶ τῶν ἱερῶν [καὶ] θυσιῶν πόλ[εως]</term>
                    <gloss>to the superintendants of offerings and sacrifices of the city</gloss>;
                    and compare [<ref type="external" target="https://papyri.info/ddbdp/p.oxy;12;1464">21866</ref>] and [<ref type="external" target="https://papyri.info/ddbdp/p.oxy;58;3929">17912</ref>].</p>
            </div>
            <div xml:id="d10_div2_format" n="2" type="section">
                <head xml:id="format">Format</head>
                <p xml:id="d10_p10">The format of certificates of pagan sacrifice follows the model
                    found in other declaration types (<ref type="descr" target="#gt_decl_census">Census</ref>, <ref type="descr" target="#gt_decl_notifDeath">Notifications
                        of Death</ref>, <ref type="descr" target="#gt_decl_camel">Camel
                        Declarations</ref>), i.e. the shape of the document corresponds to the <term xml:lang="lat">pagina</term> standard, with the scribe writing along the
                    horizontal fibres [<ref type="internal" target="14001.xml">14001</ref>, <ref type="internal" target="13945.xml">13945</ref>]; there is also one with
                    vertical fibres [<ref type="internal" target="12907.xml">12907</ref>]. Of the
                    documents in our database the average height is 21.2 cm and width 7.4 cm; [<ref type="internal" target="56431.xml">56431</ref>] is wider than usual (22 x
                    12.4 cm), see <bibl type="edition">
                        <author>Schubert</author>
                        <ptr target="gramm:schubert2016"/>
                        <citedRange>185</citedRange>
                    </bibl>. Of the
                    four Oxyrhynchite certificates only one is complete [<ref type="external" target="https://papyri.info/ddbdp/p.oxy;12;1464">21866</ref>] and it is
                    shorter and wider than the Arsinoite examples at 17.2 x 9.8 cm.</p>
            </div>
            <div xml:id="d10_div3_layout" n="3" type="section">
                <head xml:id="layout">Layout</head>
                <p xml:id="d10_p11">That these certificates were prepared before the formal
                    sacrifice took place, is evidenced by the large windows left to facilitate the
                    written confirmation of the officials (by another hand), and the signature of
                    one of them (a third hand), e.g. [<ref type="internal" target="13945.xml">13945</ref>, <ref type="internal" target="11977.xml">11977</ref>, <ref type="internal" target="11978.xml">11978</ref>]. These windows facilitated
                    the different stages of the procedure and can be found in other declarations
                    such as <ref type="descr" target="#gt_decl_notifDeath">Notifications of
                        Death</ref>; on these windows see <bibl>
                        <author>Schubert</author>
                        <ptr target="gramm:schubert2018b"/>
                        <citedRange>340-343</citedRange>
                    </bibl>. The certificates from Oxyrhynchus do not display windows and it is
                    likely that the official signature was added at the end of the document, see <bibl>
                        <author>Schubert</author>
                        <ptr target="gramm:schubert2016"/>
                        <citedRange>196</citedRange>
                    </bibl>. The address to the officials is generally written in <term xml:lang="lat">ekthesis</term> to the rest of the text, and separated from
                    the <term xml:lang="grc">παρά</term> clause [<ref type="internal" target="13940.xml">13940</ref>, <ref type="internal" target="14001.xml">14001</ref>]. The date often begins with a large (<term xml:lang="grc">ἔτους</term>) symbol [<ref type="internal" target="12907.xml">12907</ref>,
                        <ref type="internal" target="12909.xml">12909</ref>].</p>
            </div>
        </body>
    </text>
</TEI>