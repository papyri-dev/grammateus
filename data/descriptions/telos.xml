<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="../schema/gramm.rng"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="d43">
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title>Description of Greek Documentary Papyri: Receipt on Tax for Catoecic
                    Land</title>
                <author corresp="../authority-lists.xml#PS">
                    <forename>Paul</forename>
                    <surname>Schubert</surname>
                </author>
                <author corresp="../authority-lists.xml#SF">
                    <forename>Susan</forename>
                    <surname>Fogarty</surname>
                </author>
                <respStmt>
                    <resp>Encoded in TEI P5 by</resp>
                    <name corresp="../authority-lists.xml#EN">
                        <forename>Elisa</forename>
                        <surname>Nury</surname>
                    </name>
                </respStmt>
                <respStmt>
                    <resp>With the collaboration of</resp>
                    <name corresp="../authority-lists.xml#LF">
                        <forename>Lavinia</forename>
                        <surname>Ferretti</surname>
                    </name>
                </respStmt>
                <funder>
                    <orgName key="SNF">Swiss National Science Foundation</orgName> - <ref target="https://data.snf.ch/grants/grant/182205">Project 182205</ref>
                </funder>
            </titleStmt>
            <publicationStmt>
                <authority>Grammateus Project</authority>
                <idno type="DOI">10.26037/yareta:rzq3dgvfzjchjjwckmkxdpbcle</idno>
                <availability>
                    <licence target="https://creativecommons.org/licenses/by-nc/4.0/">Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)</licence>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <p>Born digital document. Encoded from a Microsoft Word document.</p>
            </sourceDesc>
        </fileDesc>
        <encodingDesc>
            <classDecl>
                <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:tei="http://www.tei-c.org/ns/1.0" href="../grammateus_taxonomy.xml"/>
            </classDecl>
            <p>This file is encoded to comply with TEI Guidelines P5, Version 4.0.0.
                    <ref target="https://tei-c.org/guidelines/p5/">https://tei-c.org/guidelines/p5/</ref>
            </p>
        </encodingDesc>
        <profileDesc>
            <langUsage>
                <language ident="lat">Latin</language>
                <language ident="grc">Greek</language>
            </langUsage>
            <textClass>
                <catRef n="type" scheme="#grammateus_taxonomy" target="#gt_ee"/>
                <catRef n="subtype" scheme="#grammateus_taxonomy" target="#gt_ee_receipt"/>
                <catRef n="variation" scheme="#grammateus_taxonomy" target="#gt_telos"/>
            </textClass>
        </profileDesc>
        <revisionDesc>
            <change who="../authority_lists.xml#EN" when="2023">Archived on Yareta</change>
            <change who="../authority-lists.xml#SF" when="2022-10-17">Updated description</change>
            <change who="../authority-lists.xml#EN" when="2021-03-02">TEI file created</change>
            <change who="../authority-lists.xml#PS" when="2020-12-07">Word document</change>
        </revisionDesc>
    </teiHeader>
    <text>
        <body>
            <head>Tax on Catoecic Land</head>
            <head type="subtitle">
                <term xml:lang="grc">τέλος καταλοχισμῶν</term>
            </head>
            <p xml:id="d43_p1">In the Ptolemaic period the procedure for the transmission of
                catoecic land (known then as cleruchic land) was managed by military offices <bibl>
                    <author>Schubert</author>
                    <ptr target="gramm:schubert2019"/>
                    <citedRange>296</citedRange>
                </bibl> and conformed to the standard [<ref type="descr" target="#gt_letterOfficial">official letter</ref>] for the period <bibl>
                    <author>Ferretti et al.</author>
                    <ptr target="gramm:ferretti2020"/>
                    <citedRange>206-209</citedRange>
                </bibl>.</p>
            <p xml:id="d43_p2">In the Roman period the transmission was managed by an officer called
                    <term xml:lang="lat">syntaktikos</term> (presumably <term xml:lang="grc">συντακτικὸς γραμματεύς</term>
                <gloss>secretary of the arrangement</gloss>
                <bibl>
                    <author>Ferretti et al.</author>
                    <ptr target="gramm:ferretti2020"/>
                    <citedRange>207</citedRange>
                </bibl>, a remnant of the <term xml:lang="grc">γραμματεὺς συντάξεως</term> attested
                in the Ptolemaic period, [<ref type="external" target="https://papyri.info/ddbdp/p.koeln;16;650">754293</ref> 138 BCE,
                Herakleopolite nome]). The officer in charge of the new enrollment notified the
                    <term xml:lang="lat">syntaktikos</term> of the transfer of a plot of catoecic
                land to a new tenant. These are notices of change of registration (<term xml:lang="grc">μετεπιγραφή</term>) and are attested in the II CE [<ref type="external" target="https://papyri.info/ddbdp/stud.pal;22;44">15112</ref>
                124 CE, Arsinoite nome; <ref type="external" target="https://papyri.info/ddbdp/bgu;7;1565">9474</ref> 169 CE, Philadelphia;
                    <ref type="internal" target="12167.xml">12167</ref> 179 CE, Arsinoite nome].</p>
            <p xml:id="d43_p3">Between the late II and early III CE, in place of a notice of change
                of registration, tax receipts were issued. The transmission was now liable to a tax
                called <term xml:lang="grc">τέλος καταλοχισμῶν</term>
                <gloss>tax on enrollment</gloss>, a generic term covering <term xml:lang="grc">τέλος
                    μετεπιγραφῆς</term>
                <gloss>tax for change of registration</gloss> [<ref type="internal" target="11419.xml">11419</ref> 182-192 CE; <ref type="internal" target="11271.xml">11271</ref> 206 CE, both Arsinoite nome] and <term xml:lang="grc">τέλος γνωστείας</term>
                <gloss>tax for registration through witnesses</gloss> [<ref type="internal" target="15120.xml">15120</ref> 204 CE, Arsinoite nome; <ref type="internal" target="9494.xml">9494</ref> 222 CE, Philadelphia]; the precise label of the tax is not always
                specified [<ref type="internal" target="14637.xml">14637</ref> 181 CE, Soknopaiou
                Nesos]. A complete analysis of the processes involved can be found in <bibl>
                    <author>Schubert</author>
                    <ptr target="gramm:schubert2019"/>
                </bibl>.</p>
            <p xml:id="d43_p4">These Roman documents, the notice of change of registration (<term xml:lang="grc">μετεπιγραφή</term>) and the tax on enrollment (<term xml:lang="grc">τέλος καταλοχισμῶν</term>), are presented in an epistolary form.
                They retain the characteristics of a Ptolemaic period [<ref type="descr" target="#gt_letterOfficial">official letter</ref>], most specifically, they are
                drawn up in <term xml:lang="lat">transversa charta</term> format, unusual for the
                Roman period. This reflects the consistency of the procedure over time <bibl>
                    <author>Ferretti et al.</author>
                    <ptr target="gramm:ferretti2020"/>
                    <citedRange>209</citedRange>
                </bibl>.</p>
            <p xml:id="d43_p5">Most surviving documents come from the Arsinoite nome. One tax
                receipt survives from the Oxyrhynchite nome and is in <term xml:lang="lat">pagina</term> format [<ref type="internal" target="17440.xml">17440</ref> 196
                CE, Oxyrhynchite nome]. This could suggest that the <term xml:lang="lat">transversa
                    charta</term> format was abandoned here, but the evidence is too scarce to allow
                for any firm conclusion.</p>
            <p xml:id="d43_p6">For other types of tax on catoecic land see [<ref type="descr" target="#gt_sitologos">monartabos tax<ptr target="#monartabos"/>
                </ref>].</p>
            <div xml:id="d43_div1_structure" n="1" type="section">
                <head xml:id="structure">Structure</head>
                <p xml:id="d43_p7">The receipt for the <term xml:lang="grc">τέλος
                        καταλοχισμῶν</term> has an epistolary opening address <seg xml:id="d43_s1" type="syntax" corresp="../authority_lists.xml#introduction">[from <hi rend="italic">name (tax collector)</hi> &lt;nom.&gt;] [to <hi rend="italic">name</hi> &lt;dat.&gt;] [<term xml:lang="grc">χαίρειν</term>] </seg>[<ref type="internal" target="14637.xml" corresp="#d43_s1">14637</ref>]. The tax is often paid through a representative <seg xml:id="d43_s2" type="syntax" corresp="../authority_lists.xml#introduction">[<term xml:lang="grc">διά</term>
                        <hi rend="italic">name</hi> &lt;gen.&gt; <term xml:lang="grc">βοηθοῦ</term>
                        ]</seg>
                    <gloss corresp="#d43_s2">through <hi rend="italic">N</hi>, assistant</gloss> [<ref type="internal" target="11419.xml" corresp="#d43_s2">11419</ref>], or <seg xml:id="d43_s3" type="syntax" corresp="../authority-lists.xml#introduction">[<term xml:lang="grc">διά</term>
                        <hi rend="italic">name</hi> &lt;gen.&gt; <term xml:lang="grc">πραγματευτοῦ</term>]</seg>
                    <gloss corresp="#d43_s3">through <hi rend="italic">N</hi>, agent</gloss> [<ref type="internal" target="14637.xml" corresp="#d43_s3">14637</ref>; <ref type="internal" target="9270.xml" corresp="#d43_s3">9270</ref> 182 CE, Karanis, the same tax farmer and agent in both cases].
                    The official then acknowledges the payment of the tax, <term xml:lang="grc">διέγραψάς μοι τέλος μετεπιγραφῆς</term>
                    <gloss>you have paid to me the tax on change of registration</gloss> [<ref type="internal" target="11271.xml">11271</ref>; <ref type="internal" target="11419.xml">11419</ref>] or <term xml:lang="grc">διέγραψάς μοι τέλος
                        γνωστίας</term>
                    <gloss>you have paid to me the tax for registration through witnesses</gloss>
                        [<ref type="internal" target="15120.xml">15120</ref>; <ref type="internal" target="9494.xml">9494</ref>]. A description of the property can be added
                        [<ref type="internal" target="9270.xml">9270</ref> l.8], along with a
                    payment amount [<ref type="internal" target="13514.xml">13514</ref> l.19-20, 197
                    CE, Tebtunis].</p>
                <p xml:id="d43_p8">A closing salutation <seg xml:id="d43_s4" type="syntax" corresp="../authority_lists.xml#subscription">[<term xml:lang="grc">ἔρρωσο</term>]</seg> may be found before the <seg xml:id="d43_s5" type="syntax" corresp="../authority-lists.xml#date">[date@end]</seg> [<ref type="internal" target="11271.xml" corresp="#d43_s4 #d43_s5">11271</ref>; <ref type="internal" target="15120.xml" corresp="#d43_s4 #d43_s5">15120</ref>]. There may also be a signature [<ref type="internal" target="11271.xml">11271</ref>; <ref type="internal" target="14913.xml">14913</ref> 230 CE, Ptolemais Euergetis].</p>
            </div>
            <div xml:id="d43_div2_format" n="2" type="section">
                <head xml:id="format">Format</head>
                <p xml:id="d43_p9">These Roman period tax receipts are predominantly found in <term xml:lang="lat">transversa charta</term> format with vertical fibres [<ref type="internal" target="15120.xml">15120</ref>; <ref type="internal" target="14637.xml">14637</ref>] but there are also examples in <term xml:lang="lat">pagina</term> format, with horizontal fibres [<ref type="internal" target="13514.xml">13514</ref>; <ref type="internal" target="17440.xml">17440</ref>].</p>
            </div>
            <div xml:id="d43_div3_layout" type="section" n="3">
                <head xml:id="layout">Layout</head>
                <p xml:id="d43_p10">In many examples the text is written as a single block with no
                    distinguishing features [<ref type="internal" target="9494.xml">9494</ref>; <ref type="internal" target="41587.xml">41587</ref> 212-215 CE, Karanis]. Some
                    scribes have written the first line in <term xml:lang="lat">ekthesis</term> to
                    the rest of the text [<ref type="internal" target="14637.xml">14637</ref>]. One
                    document has a <term xml:lang="lat">vacat</term> at the end of the main text
                    preceding an indented signature [<ref type="internal" target="14913.xml">14913</ref>]. A neatly written receipt from Soknopaiou Nesos has wide
                    margins and an x-type line filler after the date [<ref type="internal" target="15120.xml">15120</ref>]. Receipts in <term xml:lang="lat">pagina</term> format appear to have writing only on one third or two-thirds
                    of the sheet [<ref type="internal" target="9270.xml">9270</ref>; <ref type="internal" target="17440.xml">17440</ref>; <ref type="internal" target="28609.xml">28609</ref> II CE, Theadelphia]. One example has very
                    long lines [<ref type="internal" target="41587.xml">41587</ref>].</p>
            </div>
        </body>
    </text>
</TEI>