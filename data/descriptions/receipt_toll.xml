<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="../schema/gramm.rng"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="d37">
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title>Description of Greek Documentary Papyri: Toll or Custom Receipt</title>
                <author corresp="../authority-lists.xml#SF">
                    <forename>Susan</forename>
                    <surname>Fogarty</surname>
                </author>
                <author corresp="../authority-lists.xml#PS">
                    <forename>Paul</forename>
                    <surname>Schubert</surname>
                </author>
                <respStmt>
                    <resp>Encoded in TEI P5 by</resp>
                    <name corresp="../authority-lists.xml#EN">
                        <forename>Elisa</forename>
                        <surname>Nury</surname>
                    </name>
                </respStmt>
                <respStmt>
                    <resp>With the collaboration of</resp>
                    <name corresp="../authority-lists.xml#LF">
                        <forename>Lavinia</forename>
                        <surname>Ferretti</surname>
                    </name>
                </respStmt>
                <funder>
                    <orgName key="SNF">Swiss National Science Foundation</orgName> - <ref target="https://data.snf.ch/grants/grant/182205">Project 182205</ref>
                </funder>
            </titleStmt>
            <publicationStmt>
                <authority>Grammateus Project</authority>
                <idno type="DOI">10.26037/yareta:6rfsfkj32bfe5fmqdijxnbxkau</idno>
                <availability>
                    <licence target="https://creativecommons.org/licenses/by-nc/4.0/">Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)</licence>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <p>Born digital document. Encoded from a Microsoft Word document.</p>
            </sourceDesc>
        </fileDesc>
        <encodingDesc>
            <classDecl>
                <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:tei="http://www.tei-c.org/ns/1.0" href="../grammateus_taxonomy.xml"/>
            </classDecl>
            <p>This file is encoded to comply with TEI Guidelines P5, Version 4.0.0. <ref target="https://tei-c.org/guidelines/p5/">https://tei-c.org/guidelines/p5/</ref>
            </p>
        </encodingDesc>
        <profileDesc>
            <langUsage>
                <language ident="lat">Latin</language>
                <language ident="grc">Greek</language>
            </langUsage>
            <textClass>
                <catRef n="type" scheme="#grammateus_taxonomy" target="#gt_os"/>
                <catRef n="subtype" scheme="#grammateus_taxonomy" target="#gt_os_receipt"/>
                <catRef n="variation" scheme="#grammateus_taxonomy" target="#gt_os_toll"/>
            </textClass>
        </profileDesc>
        <revisionDesc>
            <change who="../authority_lists.xml#EN" when="2023">Archived on Yareta</change>
            <change who="../authority-lists.xml#EN" when="2020-05-25">TEI file created</change>
            <change who="../authority-lists.xml#SF" when="2020-05-20">Word document</change>
        </revisionDesc>
    </teiHeader>
    <text>
        <body>
            <head>Customshouse / Toll Receipts</head>
            <p xml:id="d37_p1">A customshouse receipt – also called toll receipt – is a document
                that attests the payment of a tax levied on goods transiting through a toll, or
                checkpoint. This description covers toll receipts at the point of entry/exit and
                does not include customs-registers. For a detailed study of customs receipts and the
                system of customs duties in Greco-Roman Egypt the work of reference is
            <bibl>
                    <author>P.Customs</author>
                    <ptr target="gramm:sijpesteijn1987"/>
                </bibl>. For the Ptolemaic period consult <bibl>
                    <author>Préaux</author>
                    <ptr target="gramm:preaux1939"/>
                </bibl> and <bibl>
                    <author>Fraser</author>
                    <ptr target="gramm:fraser1972"/>
                </bibl> although Sijpesteijn adds some new evidence. <bibl>
                    <author>Wallace</author>
                    <ptr target="gramm:wallace1938"/>
                    <citedRange>255-276</citedRange>
                </bibl> surveys all the customs regulations from the available evidence.</p>
            <p xml:id="d37_p2">There is evidence of customs duties on goods being imported from
                Syria and Palestine at Pelusium, from the Aegean and further afield at Alexandria,
                and from Africa at ports along the Red Sea coast, as well as at Syene/Elephantine.
                Duties were also imposed on goods exported from these ports
            <bibl>
                    <author>P.Customs</author>
                    <ptr target="gramm:sijpesteijn1987"/>
                    <citedRange>1-2</citedRange>
                </bibl>.</p>
            <p xml:id="d37_p3">Internal tolls were levied on goods transported along the Nile at the
                border between Upper and Lower Egypt at Memphis, and in the Delta region at Schedia
                and Juliopolis <bibl>
                <author>P.Customs</author>
                    <ptr target="gramm:sijpesteijn1987"/>
                    <citedRange>16-18</citedRange>
                </bibl>. The majority of the evidence at village level is from the Arsinoite nome.
                Three different duties are attested: the <term xml:lang="grc">ἐρημοφυλακία</term>, a
                tax paid for protection while travelling through the desert, and perhaps also for
                the upkeep of the roads <bibl>
                <author>P.Customs</author>
                    <ptr target="gramm:sijpesteijn1987"/>
                    <citedRange>21-22</citedRange>
                </bibl>; the <term xml:lang="grc">λιμὴν Μέμφεως</term>, paid to allow goods to be
                transported from one <term xml:lang="lat">epistrategia</term> to another (whether or
                not the route was via Memphis), but paid at the customshouse of the village where
                the journey started <bibl>
                <author>P.Customs</author>
                    <ptr target="gramm:sijpesteijn1987"/>
                    <citedRange>22-23</citedRange>
                </bibl>; and the <term xml:lang="grc">ρ καὶ ν</term> (literally: <gloss>100 and
                    50</gloss>, to be understood as 1/100 + 1/50), a 3% duty on the goods
                themselves, which may have been at either an <term xml:lang="lat">ad valorem</term>
                or fixed rate <bibl>
                <author>P.Customs</author>
                    <ptr target="gramm:sijpesteijn1987"/>
                    <citedRange>23-25</citedRange>
                </bibl>. Examples of toll receipts for transportation by water are rare and most
                receipts are for the transportation of goods over land by camels and donkeys <bibl>
                <author>P.Customs</author>
                    <ptr target="gramm:sijpesteijn1987"/>
                    <citedRange>27-39</citedRange>
                </bibl>. A broad range of goods were moved, the greater number of receipts being for
                wine, olive oil, wheat and barley, vegetables and seeds, and animals <bibl>
                <author>P.Customs</author>
                    <ptr target="gramm:sijpesteijn1987"/>
                    <citedRange>58-70</citedRange>
                </bibl>.</p>
            <div xml:id="d37_div1_customdecl_structure" type="section" n="1">
                <head xml:id="customdecl_structure">Structure</head>
                <p xml:id="d37_p4">Toll receipts are structured in the following way: <list type="unordered">
                        <item>The receipts open with one of the following the main verbs:<list type="unordered">
                                <item>
                                    <seg type="syntax" corresp="../authority_lists.xml#main-text" xml:id="d37_s1">[<term xml:lang="grc">τετελώνηται</term>] </seg>
                                    <gloss corresp="#d37_s1">has been paid</gloss> [<ref type="internal" target="15075.xml" corresp="#d37_s1">15075</ref> II CE, Soknopaiou Nesos],</item>
                                <item>
                                    <seg type="syntax" corresp="../authority_lists.xml#main-text" xml:id="d37_s2">[<term xml:lang="grc">πάρες</term>]</seg> or
                                        <seg type="syntax" corresp="../authority_lists.xml#main-text" xml:id="d37_s3">[<term xml:lang="grc">πάρετε</term>]</seg> (<term xml:lang="grc">παρίημι</term>) <gloss corresp="#d37_s2 #d37_s3">let pass</gloss> [<ref type="internal" target="10914.xml" corresp="#d37_s2">10914</ref> I CE, Bakchias], </item>
                                <item>in the case of a pre-payment of the toll before travel <seg type="syntax" corresp="../authority_lists.xml#main-text" xml:id="d37_s4">[<term xml:lang="grc">παρῆξεν</term>]</seg>
                                    <term xml:lang="grc">(παράγειν)</term>
                                    <gloss corresp="#d37_s4">has led</gloss> [<ref type="internal" target="15070.xml" corresp="#d37_s4">15070</ref> II CE,
                                    Soknopaiou Nesos] <bibl>
                                    <author>P.Customs</author>
                                        <ptr target="gramm:sijpesteijn1987"/>
                                        <citedRange>9-11</citedRange>
                                    </bibl>;</item>
                            </list>
                        </item>
                        <item>
                            <seg type="syntax" corresp="../authority_lists.xml#main-text" xml:id="d37_s6">[through <term xml:lang="grc">διὰ (πύλης)</term>
                                <hi rend="italic">place</hi> &lt;gen.&gt;]</seg>
                            <gloss corresp="#d37_s6">through the gate of (place)</gloss> - there was
                            usually a custom station at each gate into the village;</item>
                        <item>The type of tax being paid:<list type="unordered">
                                <item>
                                    <term xml:lang="grc">ἐρημοφυλακία</term> [<ref type="internal" target="12882.xml">12882</ref> I CE, Soknopaiou Nesos; <ref type="internal" target="14360.xml">14360</ref> II CE,
                                    Karanis]</item>
                                <item>
                                    <term xml:lang="grc">λιμὴν Μέμφεως</term> [<ref type="internal" target="15039.xml">15039</ref> II CE,
                                    Philadelphia; <ref type="internal" target="15075.xml">15075</ref>]</item>
                                <item>
                                    <term xml:lang="grc">ρ καὶ ν</term> [<ref type="internal" target="15070.xml">15070</ref>, <ref type="internal" target="15075.xml">11435</ref> II CE, Soknopaiou Nesos];
                                </item>
                            </list>
                        </item>
                        <item>The name of the person transporting the goods;</item>
                        <item>Whether the toll is for goods being exported (<term xml:lang="grc">ἐξάγων</term>) [<ref type="internal" target="15034.xml">15034</ref>
                            I CE, Soknopaiou Nesos; <ref type="internal" target="14354.xml">14354</ref> II CE, Soknopaiou Nesos] or imported (<term xml:lang="grc">εἰσάγων</term>) [<ref type="internal" target="11467.xml">11467</ref>; <ref type="internal" target="14355.xml">14355</ref> both II CE, Philadelphia];</item>
                        <item>The pack animals and the goods being transported are always stated
                            e.g. a donkey carrying a measure of olive oil [<ref type="internal" target="14352.xml">14352</ref> II CE, Soknopaiou Nesos], or a donkey
                            carrying figs [<ref type="internal" target="10611.xml">10611</ref> II
                            CE, Philadelphia];</item>
                        <item>Finally, the <seg type="syntax" corresp="../authority_lists.xml#date" xml:id="d37_s5">[date@end]</seg> on which the toll was paid.</item>
                        <item>Occasionally the name of an official at the toll gate is attached,
                            confirmed by a second hand with <term xml:lang="grc">σεση(μείωμαι)</term>
                            <gloss>I have signed</gloss> [<ref type="internal" target="10914.xml">10914</ref>, <ref type="internal" target="14354.xml">14354</ref>].
                        </item>
                    </list>
                </p>
                <p xml:id="d37_p5">A mid-I CE receipt carries a letter-type opening address, as the
                    taxpayer addresses the official in charge of the toll-gate directly, <term xml:lang="grc">Φανίας ὁ πρὸς τῇ πύλῃ… χαίριν</term> [<ref type="internal" target="12882.xml">12882</ref>] and then continues with the usual content.
                    The receipt for the toll paid for the transportation of alum (<term xml:lang="grc">στυπτηρία</term>) [<ref type="internal" target="9301.xml">9301</ref> II CE, Arsinoite nome] also does not follow the usual structure
                    as it is a refund through a bank of the toll amount paid by the transporter,
                    because this was a commodity exempt from taxes <bibl>
                        <author>P.Customs</author>
                        <ptr target="gramm:sijpesteijn1987"/>
                        <citedRange>83-84</citedRange>
                    </bibl>.</p>
            </div>
            <div xml:id="d37_div2_customdecl_format" type="section" n="2">
                <head xml:id="customdecl_format">Format</head>
                <p xml:id="d37_p6">There is no standard format for toll receipts. Many of the
                    receipts have either a <term xml:lang="lat">pagina</term> format [<ref type="internal" target="15075.xml">15075</ref>; <ref type="internal" target="15072.xml">15072</ref>; <ref type="internal" target="9709.xml">9709</ref>; <ref type="internal" target="15127.xml">15127</ref> all II CE,
                    Soknopaiou Nesos] (horizontal fibres),, [<ref type="internal" target="13742.xml">13742</ref> II CE, Tebtunis; <ref type="internal" target="14360.xml">14360</ref>; <ref type="internal" target="40932.xml">40932</ref> III CE,
                    Soknopaiou Nesos], (vertical fibres); or a squarish format [<ref type="internal" target="14354.xml">14354</ref>; <ref type="internal" target="9708.xml">9708</ref> II CE, Soknopaiou Nesos; <ref type="internal" target="15039.xml">15039</ref>] (horizontal fibres), [<ref type="internal" target="11467.xml">11467</ref>] (vertical fibres). Some are <term xml:lang="lat">transversa
                        charta</term> [<ref type="internal" target="13741.xml">13741</ref> II CE,
                    Tebtunis; <ref type="internal" target="9396.xml">9396</ref> III CE, Soknopaiou
                    Nesos; <ref type="internal" target="29092.xml">29092</ref> and <ref type="internal" target="29093.xml">29093</ref> both II-III CE, Soknopaiou
                    Nesos] or in this orientation with horizontal fibres [<ref type="internal" target="10914">10914</ref>; <ref type="internal" target="15034.xml">15034</ref>; <ref type="internal" target="10099.xml">10099</ref> II CE,
                    Soknopaiou Nesos: <ref type="internal" target="28535.xml">28535</ref> II-III CE,
                    Tebtunis]. At the end of the document there may be an official seal [<ref type="internal" target="14352.xml">14352</ref>; <ref type="internal" target="14355.xml">14355</ref>; <ref type="internal" target="14360.xml">14360</ref>], or an indication that the receipt carried no seal, i.e. <term xml:lang="grc">χωρὶς χαρακ(τῆρος)</term> [<ref type="internal" target="10627.xml">10627</ref> II CE, Philadelphia; <ref type="internal" target="10678.xml">10678</ref> III CE, Dionysias] or <term xml:lang="grc">χωρὶς σφρα(γῖδος)</term> [<ref type="internal" target="31951.xml">31951</ref> III CE, Philadelphia].</p>
            </div>
            <div xml:id="d37_div3_customdecl_layout" type="section" n="3">
                <head xml:id="customdecl_layout">Layout</head>
                <p xml:id="d37_p7">The most common layout is as a single block of text without any
                    differentiation between the elements of information. There is often a line drawn
                    underneath the last line of text presumably to prevent any additions [<ref type="internal" target="15072.xml">15072</ref>; <ref type="internal" target="11520.xml">11520</ref> II CE, Bakchias; <ref type="internal" target="15075.xml">11435</ref>]; in one example the date divides the line
                    drawn into two halves [<ref type="internal" target="11467.xml">11467</ref>].
                    Often the last letter on a line is elongated to fill any remaining space [<ref type="internal" target="29092.xml">29092</ref>; <ref type="internal" target="27652.xml">27652</ref> and <ref type="internal" target="27653.xml">27653</ref> both II CE, Arsinoite nome] and sometimes more elaborate line
                    fillers can be found [<ref type="internal" target="31233.xml">31233</ref> III
                    CE, Philadelphia; <ref type="internal" target="10674.xml">10674</ref> III CE,
                    Soknopaiou Nesos]. There can be a large space between the final line of text and
                    the end of the papyrus [<ref type="internal" target="10611.xml">10611</ref>] –
                    sometimes this space may have carried a seal [<ref type="internal" target="15075.xml">15075</ref>; <ref type="internal" target="14354.xml">14354</ref>; <ref type="internal" target="15072.xml">15072</ref>; <ref type="internal" target="15075.xml">11435</ref>]. There is evidence that some
                    receipts were prepared beforehand, as indicated by a change of hand where the
                    details of the transporter and transportation are entered [<ref type="external" target="https://papyri.info/ddbdp/p.customs;;406">26703</ref> II-III CE,
                    Bakchias; <ref type="external" target="https://papyri.info/ddbdp/stud.pal;22;60">27638</ref> and <ref type="external" target="https://papyri.info/ddbdp/stud.pal;22;10">27629</ref> both II CE,
                    Arsinoite nome].</p>
            </div>
        </body>
    </text>
</TEI>