<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="../schema/gramm.rng"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="d7">
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title>Description of Greek Documentary Papyri: Declaration of Uninundated
                    Land</title>
                <author corresp="../authority-lists.xml#SF">
                    <forename>Susan</forename>
                    <surname>Fogarty</surname>
                </author>
                <author corresp="../authority-lists.xml#PS">
                    <forename>Paul</forename>
                    <surname>Schubert</surname>
                </author>
                <respStmt>
                    <resp>Encoded in TEI P5 by</resp>
                    <name corresp="../authority-lists.xml#EN">
                        <forename>Elisa</forename>
                        <surname>Nury</surname>
                    </name>
                </respStmt>
                <respStmt>
                    <resp>With the collaboration of</resp>
                    <name corresp="../authority-lists.xml#LF">
                        <forename>Lavinia</forename>
                        <surname>Ferretti</surname>
                    </name>
                </respStmt>
                <funder>
                    <orgName key="SNF">Swiss National Science Foundation</orgName> - <ref target="https://data.snf.ch/grants/grant/182205">Project 182205</ref>
                </funder>
            </titleStmt>
            <publicationStmt>
                <authority>Grammateus Project</authority>
                <idno type="DOI">10.26037/yareta:x77o4rblmbc5xazudw2sgi6h3a</idno>
                <availability>
                    <licence target="https://creativecommons.org/licenses/by-nc/4.0/">Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)</licence>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <p>Born digital document. Encoded from a Microsoft Word document.
                    Previous versions on Yareta:
                    <list>
                        <item>
                            Ferretti, L., Fogarty, S., Nury, E., and Schubert, P. 2021. <title>Description of Greek Documentary Papyri: Declaration of Uninundated Land</title>. grammateus project. DOI: 
                            <idno type="DOI">10.26037/yareta:6kakwu3ayvcsxgl6ndf7kxcege</idno>
                        </item>
                    </list>
                </p>
            </sourceDesc>
        </fileDesc>
        <encodingDesc>
            <classDecl>
                <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:tei="http://www.tei-c.org/ns/1.0" href="../grammateus_taxonomy.xml"/>
            </classDecl>
            <p>This file is encoded to comply with TEI Guidelines P5, Version 4.0.0. <ref target="https://tei-c.org/guidelines/p5/">https://tei-c.org/guidelines/p5/</ref>
            </p>
        </encodingDesc>
        <profileDesc>
            <langUsage>
                <language ident="lat">Latin</language>
                <language ident="grc">Greek</language>
            </langUsage>
            <textClass>
                <catRef n="type" scheme="#grammateus_taxonomy" target="#gt_ti"/>
                <catRef n="subtype" scheme="#grammateus_taxonomy" target="#gt_decl"/>
                <catRef n="variation" scheme="#grammateus_taxonomy" target="#gt_decl_abrochos"/>
            </textClass>
        </profileDesc>
        <revisionDesc>
            <change who="../authority_lists.xml#EN" when="2023">Archived on Yareta</change>
            <change who="../authority-lists.xml#EN" when="2021-03-02">TEI file created</change>
            <change who="../authority-lists.xml#SF" when="2020-11-20">Word document</change>
        </revisionDesc>
    </teiHeader>
    <text>
        <body>
            <head>Declaration of Uninundated Land</head>
            <head type="subtitle">
                <term xml:lang="grc">ἄβροχος</term>
            </head>
            <p xml:id="d7_p1">The annual flood of the Nile, backed up by irrigation systems, dykes,
                dams, and essential maintenance (see [<ref type="descr" target="#gt_penthemeros">Penthemeros receipts</ref>]), ensured a fertile soil for the growing of
                essential crops; on the administration of the Nile in the Greco-Roman period see <listBibl>
                    <bibl>
                        <author>Bonneau</author>
                        <ptr target="gramm:bonneau1964"/>
                    </bibl>
                    <bibl>
                        <author>Bonneau</author>
                        <ptr target="gramm:bonneau1971"/>
                    </bibl>
                    <bibl>
                        <author>Bonneau</author>
                        <ptr target="gramm:bonneau1993"/>
                    </bibl>
                </listBibl>. However, despite all these efforts, often the flood water was too low
                to reach some land and it remained infertile and unproductive through no fault of
                the farmer. Land of this kind was referred to as <term xml:lang="lat">abrochos</term> (<term xml:lang="grc">ἄβροχος</term>), uninundated. </p>
            <p xml:id="d7_p2">The first evidence of a process whereby a farmer could declare his
                land to be <term xml:lang="lat">abrochos</term> and claim a reduction in tax
                payments appears during the reign of Antoninus Pius (138-161 CE). The work of
                reference for this type of document is <bibl>
                    <author>Habermann</author>
                    <ptr target="gramm:habermann1997"/>
                </bibl>, see also <bibl>
                    <author>Gonis</author>
                    <ptr target="gramm:gonis2003"/>
                    <citedRange>171</citedRange>
                </bibl> for further references, and especially <bibl>
                    <author>Rowlandson</author>
                    <ptr target="gramm:rowlandson1996"/>
                    <citedRange>16, 77</citedRange>
                </bibl>. The declarations often included references to land that was uninundated and
                artificially irrigated – for a list see <bibl>
                    <author>Habermann</author>
                    <ptr target="gramm:habermann1997"/>
                    <citedRange>223-226</citedRange>
                </bibl> with <bibl>
                    <author>Gonis</author>
                    <ptr target="gramm:gonis1999"/>
                </bibl>. The earliest example dates from 158 CE [<ref type="internal" target="12741.xml">12741</ref>, Philadelphia]; the latest is from 245 CE [<ref type="external" target="http://www.papyri.info/ddbdp/p.oxy;65;4488">78590</ref>,
                Oxyrhynchus] and it seems these declarations disappeared after administrative
                reforms under Philip the Arab (244-249 CE). Most examples have been found in the
                Arsinoite and Oxyrhynchite nomes.</p>
            <div xml:id="d7_div1_structure" n="1" type="section">
                <head xml:id="structure">Structure</head>
                <p xml:id="d7_p3">As a formal declaration the heading is in the form of a <term xml:lang="grc">ὑπόμνημα</term>
                    <seg xml:id="d7_s1" corresp="../authority_lists.xml#introduction" type="syntax">[to <hi rend="italic">name</hi> &lt;dat.&gt;] [from <term xml:lang="grc">παρά</term>
                        <hi rend="italic">name</hi> &lt;gen.&gt;]</seg> and is addressed to the
                    official(s) who had responsibility for the declarations at different times, e.g.
                        <term xml:lang="lat" xml:id="d7_t1" corresp="#d7_s1">komogrammateus</term>
                        [<ref type="internal" target="12741.xml" corresp="#d7_s1 #d7_t1">12741</ref>; <ref type="internal" target="16445.xml" corresp="#d7_s1 #d7_t1">16445</ref> 245 CE, Oxyrhynchus], sometimes together with the <term xml:lang="lat" xml:id="d7_t2" corresp="#d7_s1">basilikogrammateus</term>
                        [<ref type="internal" target="13483.xml" corresp="#d7_t2 #d7_s1">13483</ref>; <ref type="external" target="https://papyri.info/ddbdp/p.fam.tebt;;52" corresp="#d7_t2 #d7_s1">15174</ref>, both 208 CE, Tebtunis], and the <term xml:lang="lat">basilikogrammateus</term> and <term xml:lang="lat" xml:id="d7_t3" corresp="#d7_s1">strategos</term> [<ref type="internal" target="14615.xml" corresp="#d7_t3 #d7_s1">14615</ref> 167-8 CE, Bacchias; <ref type="internal" target="15146.xml" corresp="#d7_t3 #d7_s1">15146</ref> 189-90 CE,
                    Philadelphia] <bibl>
                        <author>Habermann</author>
                        <ptr target="gramm:habermann1997"/>
                        <citedRange>235-236</citedRange>
                    </bibl>. Some with no addressee simply begin <seg xml:id="d7_s2" corresp="../authority_lists.xml#introduction" type="syntax">[from <term xml:lang="grc">παρά</term>
                        <hi rend="italic">name</hi> &lt;gen.&gt;]</seg> [<ref type="internal" target="8959.xml" corresp="#d7_s2">8959</ref> 163 CE, Karanis]. The
                    declarant can be an individual, whose patromymic and place of origin or
                    residence is usually stated, e.g. [<ref type="internal" target="44381.xml">44381</ref> 201 CE, Apias], or a group of individuals making a collective
                    declaration, e.g. [<ref type="internal" target="10221.xml">10221</ref> 164 CE,
                    Lagis]; see <bibl>
                        <author>Habermann</author>
                        <ptr target="gramm:habermann1997"/>
                        <citedRange>239-249</citedRange>
                    </bibl>.</p>
                <p xml:id="d7_p4">The main text follows with a reference to the original order by
                    the prefect, <term xml:lang="grc">κατὰ τὰ κελευσθέντα ὑπὸ τοῦ ἡγεμόνος</term>,
                    e.g. [<ref type="internal" target="12741.xml">12741</ref>; <ref type="internal" target="10221.xml">10221</ref>], and later by the <term xml:lang="grc">κρατίστου ἐπιτρόπου περὶ ἀβρόχου καὶ ἐπηντλημένης</term>, <gloss>the
                        procurator for uninundated and artificially irrigated land</gloss>, e.g.
                        [<ref type="internal" target="22268.xml">22268</ref> 203-4 CE, Oxyrhynchus;
                        <ref type="internal" target="44381.xml">44381</ref>], and details the number
                    of <term xml:lang="lat">arourai</term> and location of the land to be declared
                        <term xml:lang="lat">abrochos</term>. A formal statement, <term xml:lang="grc">διὸ ἐπιδίδωμι τὴν ἀπογραφήν</term>, <gloss>therefore I make
                        the declaration</gloss>, may be added, e.g. [<ref type="internal" target="13753.xml">13753</ref> 169 CE, Arsinoite]. </p>
                <p xml:id="d7_p5">The <seg xml:id="d7_s3" corresp="../authority_lists.xml#date" type="syntax">[date@end]</seg> comes before the subscription where one or
                    more official signed the declaration, see <bibl>
                        <author>Habermann</author>
                        <ptr target="gramm:habermann1997"/>
                        <citedRange>227-234</citedRange>
                    </bibl>. In the collective declaration [<ref type="internal" target="10221.xml">10221</ref>] each of the declarants who could write makes a formal
                    statement in their own hand, <term xml:lang="grc">συνεπιδέδωκα</term>, before
                    the date.</p>
            </div>
            <div xml:id="d7_div2_format" type="section" n="2">
                <head xml:id="format">Format</head>
                <p xml:id="d7_p6">The format of <term xml:lang="lat">abrochia</term> declarations
                    follows the model found in other variations of the sub-type <ref type="descr" target="#gt_decl">Declaration</ref>, i.e. the shape of the document
                    corresponds to the <term xml:lang="lat">pagina</term> standard, with the scribe
                    writing along horizontal fibres. [<ref type="internal" target="10221.xml">10221</ref>] is more squarish in format as it must accommodate two columns
                    of names. An exceptionally long papyrus makes up the declaration made by a
                    wealthy woman from Oxyrhynchus [<ref type="internal" target="16445.xml">16445</ref>] who lists over 1,700 arouras spread over a wide area.</p>
            </div>
            <div xml:id="d7_div3_layout" type="section" n="3">
                <head xml:id="layout">Layout</head>
                <p xml:id="d7_p7">The document often begins with an enlarged first letter of the
                    address to the official, the second line of which can be indented before the
                        <term xml:lang="grc">παρά</term> clause, e.g. [<ref type="external" target="https://papyri.info/ddbdp/sb;16;12561">14614</ref> 163-4 CE,
                    Pelusion; <ref type="internal" target="14615.xml">14615</ref>; <ref type="internal" target="44381.xml">44381</ref>], and the <term xml:lang="grc">παρά</term> itself often distinguished by an enlarged first
                    letter, e.g. [<ref type="internal" target="13483.xml">13483</ref>]; there can
                    also be a space between the two elements of the address, e.g. [<ref type="internal" target="12741.xml">12741</ref>; <ref type="internal" target="10221.xml">10221</ref>]. The body of the declaration is usually a
                    single column of text [<ref type="internal" target="22268.xml">22268</ref>; <ref type="internal" target="44381.xml">44381</ref>]. As can happen in letters
                    with more than one column, the second of the two columns of [<ref type="internal" target="10221.xml">10221</ref>] is narrower than the
                    previous one <bibl>
                        <author>Sarri</author>
                        <ptr target="gramm:sarri2018"/>
                        <citedRange>112</citedRange>
                    </bibl>. After the main text there is often a space between it and the
                    subscription [<ref type="internal" target="8959.xml">8959</ref>] or between the
                    text, and date and subscription [<ref type="internal" target="13483.xml">13483</ref>]; the subscription can also be indented [<ref type="internal" target="44381.xml">44381</ref>].</p>
            </div>
        </body>
    </text>
</TEI>