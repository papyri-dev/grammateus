<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="../schema/gramm.rng"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="d14">
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title>Description of Greek Documentary Papyri: Enteuxis</title>
                <author corresp="../authority-lists.xml#SF">
                    <forename>Susan</forename>
                    <surname>Fogarty</surname>
                </author>
                <author corresp="../authority-lists.xml#PS">
                    <forename>Paul</forename>
                    <surname>Schubert</surname>
                </author>
                <respStmt>
                    <resp>Encoded in TEI P5 by</resp>
                    <name corresp="../authority-lists.xml#EN">
                        <forename>Elisa</forename>
                        <surname>Nury</surname>
                    </name>
                </respStmt>
                <respStmt>
                    <resp>With the collaboration of</resp>
                    <name corresp="../authority-lists.xml#LF">
                        <forename>Lavinia</forename>
                        <surname>Ferretti</surname>
                    </name>
                </respStmt>
                <funder>
                    <orgName key="SNF">Swiss National Science Foundation</orgName> - <ref target="https://data.snf.ch/grants/grant/182205">Project 182205</ref>
                </funder>
            </titleStmt>
            <publicationStmt>
                <authority>Grammateus Project</authority>
                <idno type="DOI">10.26037/yareta:cbwjc3flwvdani3g5bmzhnaxx4</idno>
                <availability>
                    <licence target="https://creativecommons.org/licenses/by-nc/4.0/">Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)</licence>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <p>Born digital document. Encoded from a Microsoft Word document.</p>
            </sourceDesc>
        </fileDesc>
        <encodingDesc>
            <classDecl>
                <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:tei="http://www.tei-c.org/ns/1.0" href="../grammateus_taxonomy.xml"/>
            </classDecl>
            <p>This file is encoded to comply with TEI Guidelines P5, Version 4.0.0. <ref target="https://tei-c.org/guidelines/p5/">https://tei-c.org/guidelines/p5/</ref>
            </p>
        </encodingDesc>
        <profileDesc>
            <langUsage>
                <language ident="lat">Latin</language>
                <language ident="grc">Greek</language>
            </langUsage>
            <textClass>
                <catRef n="type" scheme="#grammateus_taxonomy" target="#gt_ee"/>
                <catRef n="subtype" scheme="#grammateus_taxonomy" target="#gt_enteuxis"/>
            </textClass>
        </profileDesc>
        <revisionDesc>
            <change who="../authority_lists.xml#EN" when="2023">Archived on Yareta</change>
            <change who="../authority-lists.xml#EN" when="2022-03-14">TEI file created</change>
            <change who="../authority-lists.xml#PS ../authority-lists.xml#SF" when="2021-11-04">Word
                document</change>
        </revisionDesc>
    </teiHeader>
    <text>
        <body>
            <head>Enteuxis</head>
            <head type="subtitle">
                <term xml:lang="grc">ἔντευξις</term>
            </head>
            <p xml:id="d14_p1">An <term xml:lang="lat">enteuxis</term> is a petition in an
                epistolary form addressed to the king, or to the king and queen, and as such is
                found only in the Ptolemaic period. There are more than two hundred and fifty
                examples from many nomes; the latest securely dated on papyrus are from the
                Herakleopolite nome [<ref type="external" target="https://papyri.info/ddbdp/bgu;14;2374">3994</ref> 88-81 BCE; <ref type="external" target="https://papyri.info/ddbdp/p.yale;1;57">5541</ref> 93-70
                BCE]; see <bibl>
                    <author>Baetens</author>
                    <ptr target="gramm:baetens2020"/>
                    <citedRange>22-23</citedRange>
                </bibl> for a complete list. </p>
            <p xml:id="d14_p2">In the III BCE the <term xml:lang="lat">enteuxis</term> was also used
                for petitioning other, non-royal, authorities – most of these are from the Zenon archive <bibl>
                    <author>TM Arch 256</author>
                    <ptr target="gramm:vandorpe2013"/>
                </bibl>, see
                <bibl>
                    <author>Baetens</author>
                    <ptr target="gramm:baetens2020"/>
                    <citedRange> 53-54 (list)</citedRange>
                </bibl>. From the II BCE onwards a non-royal petition was drawn up as a <term xml:lang="grc">ὑπόμνημα</term>, see [<ref type="descr" target="#gt_petition">petition</ref>]. Petitions in the form of an <term xml:lang="lat">enteuxis</term> differ typologically from petitions in the form of a <term xml:lang="grc">ὑπόμνημα</term>, and from the II BCE were reserved only for
                petitioning the king. </p>
            <p xml:id="d14_p3">Petitions to the king cover a wide range of issues including, e.g. a
                request to load a ship with grain [<ref type="internal" target="3302.xml">3302</ref>
                222 BCE, Magdola], appeals to force the payment of debts [<ref type="internal" target="3310.xml">3310</ref> 222 BCE; <ref type="internal" target="3311.xml">3311</ref> 221 BCE; <ref type="internal" target="3319.xml">3319</ref> 221 BCE,
                all Magdola], contract disputes [<ref type="internal" target="3334.xml">3334</ref>
                222 BCE, Magdola], land and building issues [<ref type="internal" target="121855.xml">121855</ref> 221 BCE; <ref type="internal" target="121857.xml">121857</ref> 220 BCE, both Muchis; <ref type="internal" target="3401.xml">3401</ref> 160 BCE, Memphis], and reports of violence [<ref type="internal" target="3358.xml">3358</ref> 221 BCE, Magdola; <ref type="internal" target="121859.xml">121859</ref> 219 BCE, Muchis]; <bibl>
                    <author>Baetens</author>
                    <ptr target="gramm:baetens2020"/>
                    <citedRange>43-45</citedRange>
                </bibl>. The petitioner usually requests some form of redress - a punishment,
                request for justice, or other form of support <bibl>
                    <author>Baetens</author>
                    <ptr target="gramm:baetens2020"/>
                    <citedRange>45-50</citedRange>
                </bibl>.</p>
            <p xml:id="d14_p4">Petitions to other authorities are mostly addressed to Zenon, but
                some are addressed to the <term xml:lang="lat">dioiketes</term> [<ref type="internal" target="1517.xml">1517</ref> 257 BCE; <ref type="internal" target="881.xml">881</ref> 253-252 BCE, both Philadelphia], <term xml:lang="lat">oikonomos</term> [<ref type="internal" target="2085.xml">2085</ref> III BCE,
                Philadelphia] or <term xml:lang="lat">architekton</term> [<ref type="external" target="https://papyri.info/ddbdp/sb;18;13881">2611</ref> 256 BCE, Arsinoite
                nome]. They are similarly wide ranging in subject matter, e.g. theft [<ref type="internal" target="793.xml">793</ref> 256 BCE, Philadelphia], and
                mistreatment [<ref type="internal" target="935.xml">935</ref> 251 BCE,
                Philadelphia].</p>
            <p xml:id="d14_p5">As well as royal and non-royal petitioning <term xml:lang="lat">enteuxeis</term>, there are documents which are drawn up as <term xml:lang="lat">enteuxeis</term> but which are not petitions. These documents
                usually make a report or request and are concerned with day-to-day business, e.g. a
                request to buy equipment [<ref type="internal" target="1121.xml">1121</ref> III BCE,
                Philadelphia], or a brief agricultural report [<ref type="internal" target="888.xml">888</ref> 252 BCE, Philadelphia]; they come mainly from the Zenon
                    archive
                <bibl>
                    <author>Baetens</author>
                    <ptr target="gramm:baetens2020"/>
                    <citedRange>60-63 (list)</citedRange>
                </bibl>. These <term xml:lang="lat">enteuxeis</term> do not appear to differ in
                content from ordinary [<ref type="descr" target="#gt_letterBusiness">business
                    letters</ref>] apart from their distinctive opening address. The distinction
                between a non-petitioning <term xml:lang="lat">enteuxis</term> and a business letter
                in III BCE is often blurred – as it is with other document types, such as [<ref type="descr" target="#gt_petition">petition</ref>] and [<ref type="descr" target="#gt_prosangelma">prosangelma</ref>] in the II BCE. On this see <bibl>
                    <author>Bickerman</author>
                    <ptr target="gramm:bickerman1930"/>
                    <citedRange>179</citedRange>
                </bibl> who suggests that the difference lies in the delivery method, and <bibl>
                    <author>Ferretti</author>
                    <ptr target="gramm:ferretti2023"/>
                </bibl>. On the former categorisation of non-royal <term xml:lang="lat">enteuxeis</term> as letters see <bibl>
                    <author>Baetens</author>
                    <ptr target="gramm:baetens2020"/>
                    <citedRange>20-21, and n.63</citedRange>
                </bibl>.</p>
            <div xml:id="d14_div1_structure" type="section" n="1">
                <head xml:id="structure">Structure</head>
                <p xml:id="d14_p6">The opening address, as with all epistolary sub-types, contains
                    the greeting <term xml:lang="grc">χαίρειν</term>, but in an <term xml:lang="lat">enteuxis</term> it is placed between the addressee and addressor:</p>
                <p xml:id="d14_p7">
                    <seg xml:id="d14_s1" type="syntax" corresp="../authority-lists.xml#introduction">[to <hi rend="italic">king</hi> &lt;dat&gt;][<term xml:lang="grc">χαίρειν</term>][from <hi rend="italic">name</hi> &lt;nom&gt;]</seg> [<ref type="internal" target="994.xml" corresp="#d14_s1">994</ref> 243 BCE, Philadelphia]. </p>
                <p xml:id="d14_p8">This simple address is found throughout the III BCE, but by the
                    early II BCE a more elaborate address to both king and queen was established
                    e.g. [<ref type="internal" target="3401.xml">3401</ref>]: <term xml:lang="grc">Βασιλεῖ Πτολεμαίωι καὶ Βασιλίσσηι Κλεοπάτραι τῆι ἀδελφῆι θεοῖς
                        Φιλομήτορσι</term>. On the evolution of the regnal address <bibl>
                        <author>Baetens</author>
                        <ptr target="gramm:baetens2020"/>
                        <citedRange>39-42</citedRange>
                    </bibl>.</p>
                <p xml:id="d14_p9">Non-royal <term xml:lang="lat">enteuxeis</term> have the same
                    simple opening address:</p>
                <p xml:id="d14_p10">
                    <seg xml:id="d14_s2" type="syntax" corresp="../authority-lists.xml#introduction">[to <hi rend="italic">name</hi> &lt;dat&gt;][<term xml:lang="grc">χαίρειν</term>][from <hi rend="italic">name</hi> &lt;nom&gt;]</seg> [<ref type="internal" target="1739.xml" corresp="#d14_s2">1739</ref> 257 BCE, Philadelphia].</p>
                <p xml:id="d14_p11">All <term xml:lang="lat">enteuxeis</term> may have additional
                    information such as the patronymic and origin of the addressor [<ref type="internal" target="3289.xml">3289</ref> 244 BCE, Sebennytos; <ref type="internal" target="3401.xml">3401</ref>] and the addressor can be one
                    person or a group e.g. [<ref type="internal" target="1105.xml">1105</ref> III
                    BCE, Arsinoite nome] where a group of bee-keepers admonish Zenon for keeping
                    their donkeys.</p>
                <p xml:id="d14_p12">Petitions to the king begin the main text with an accusatory <seg xml:id="d14_s3" type="syntax" corresp="../authority-lists.xml#main-text">
                        <term xml:lang="grc">ἀδικοῦμαι ὑπό</term> [<hi rend="italic">name</hi> &lt;gen&gt;]</seg>
                    <gloss corresp="#d14_s3">I am wronged by <hi rend="italic">N</hi>
                    </gloss> [<ref type="internal" target="994.xml" corresp="#d14_s3">994</ref>; <ref type="internal" target="3286.xml" corresp="#d14_s3">3286</ref> 218 BCE, Magdola]. Non-royal petitions may have <seg type="syntax" corresp="../authority_lists.xml#main-text" xml:id="d14_s4">[<term xml:lang="grc">δέομαι</term>]</seg>
                    <gloss corresp="#d14_s4">I ask</gloss>, e.g. the swineherds in [<ref type="internal" corresp="#d14_s4" target="1133.xml">1133</ref> III BCE, Philadelphia] <term xml:lang="grc" xml:id="d14_t1">δεόμεθα οὖν σου, ἐλέησον ἡμᾶς</term>, <gloss corresp="#d14_t1">we ask you, have mercy on us</gloss>; but usually these non-royal <term xml:lang="lat">enteuxeis</term> begin with an immediate explanation of the issue [<ref type="internal" target="935.xml">935</ref>].</p>
                <p xml:id="d14_p13">A request for redress follows, introduced by <term xml:lang="grc">δέομαι</term>, most favoured in the royal <term xml:lang="lat">enteuxeis</term>, [<ref type="internal" target="3401.xml" corresp="#d14_s4">3401</ref>; <ref type="internal" target="3334.xml" corresp="#d14_s4">3334</ref>], or <seg type="syntax" corresp="../authority_lists.xml#main-text" xml:id="d14_s5">[<term xml:lang="grc">ἀξιῶ</term>]</seg> [<ref corresp="#d14_s5" type="external" target="https://papyri.info/ddbdp/upz;1;15">3406</ref> 156 BCE, Memphis], or <seg type="syntax" corresp="../authority_lists.xml#main-text" xml:id="d14_s6">[<term xml:lang="grc">ἀξιῶ δεόμενος</term>]</seg> [<ref type="internal" target="121858.xml" corresp="#d14_s6">121858</ref> 219 BCE, Muchis]; <bibl>
                        <author>Baetens</author>
                        <ptr target="gramm:baetens2020"/>
                        <citedRange>36-37</citedRange>
                    </bibl>. Non-royal petitioning <term xml:lang="lat">enteuxeis</term> also favour <term xml:lang="grc">δέομαι</term> [<ref corresp="#d14_s4" type="internal" target="881.xml">881</ref>; <ref type="internal" corresp="#d14_s4" target="1517.xml">1517</ref>], but some begin the request with <term xml:lang="grc">καλῶς ... ποιήσεις</term> or <term xml:lang="grc">καλῶς ἂν ... ποιήσαις</term>
                    <gloss>please</gloss> (lit. <gloss>you will do well</gloss> / <gloss>you would do well</gloss>) [<ref type="internal" target="935.xml">935</ref>; <ref type="internal" target="2057.xml">2057</ref> 250-249 BCE, Philadelphia].</p>
                <p xml:id="d14_p14">Non-royal <term xml:lang="lat">enteuxeis</term>, if making a
                    request, can have the same verbs and phrases or they may have a simple
                    imperative e.g. <term xml:lang="grc">ἐξελοῦ με</term>
                    <gloss>release me</gloss> [<ref type="internal" target="1130.xml">1130</ref> III
                    BCE, Philadelphia], or <term xml:lang="grc">διδόσθω ἡμῖν ἔλαιον</term>
                    <gloss>give us oil</gloss> [<ref type="internal" target="2153.xml">2153</ref>
                    III BCE, Philadelphia] often introduced by <term xml:lang="grc">εἰ οὖν σοι
                        δοκεῖ</term>
                    <gloss>if it seems good to you</gloss>
                    <bibl>
                        <author>Baetens</author>
                        <ptr target="gramm:baetens2020"/>
                        <citedRange>36-37, 56, 64</citedRange>
                    </bibl>.</p>
                <p xml:id="d14_p15">The closing salutation is usually <seg type="syntax" corresp="../authority_lists.xml#subscription" xml:id="d14_s7">[<term xml:lang="grc">εὐτύχει</term>]</seg> [<ref type="internal" target="1796.xml" corresp="#d14_s7">1796</ref> 245-244BCE, Philadelphia] or <seg type="syntax" corresp="../authority_lists.xml#subscription" xml:id="d14_s8">[<term xml:lang="grc">εὐτυχεῖτε</term>]</seg> [<ref type="internal" target="3401.xml" corresp="#d14_s8">3401</ref>], but non-petitioning <term xml:lang="lat">enteuxeis</term> sometimes close with <term xml:lang="grc">ἔρρωσο</term> [<ref type="internal" target="1051.xml">1051</ref> III BCE; <ref type="internal" target="1767.xml">1767</ref> 251-250 BCE, both Philadelphia], and may add a [date@end] [<ref type="internal" target="1739.xml">1739</ref>]. There is no closing salutation from the <term xml:lang="lat">dekatarchoi</term> in [<ref type="internal" target="7647.xml">7647</ref> 255 BCE Arsinoite nome], only a [date@end]. There may be a subscription from an official appended [<ref type="internal" target="3329.xml">3329</ref> 218 BCE, Magdola; <ref type="internal" target="3679.xml">3679</ref> 117 BCE, Alexandria]; a royal <term xml:lang="lat">enteuxis</term> from a group of farmers carries a reply from the king in the form of a letter [<ref type="internal" target="8669.xml">8669</ref> 157 BCE, Soknopaiou Nesos].</p>
            </div>
            <div xml:id="d14_div2_format" type="section" n="2">
                <head xml:id="format">Format</head>
                <p xml:id="d14_p16">Royal <term xml:lang="lat">enteuxeis</term> are predominantly in
                        <term xml:lang="lat">transversa charta</term> format with the writing
                    against vertical fibres [<ref type="internal" target="3325.xml">3325</ref> 221
                    BCE, Magdola] and many are quite wide and short [<ref type="internal" target="3289.xml">3289</ref> H.11 x W.33.5cm; <ref type="internal" target="121857.xml">121857</ref> H.8.8 x W.33cm; <ref type="internal" target="3311.xml">3311</ref> H.7 x W.33cm]. Some are in <term xml:lang="lat">pagina</term> format with horizontal fibres [<ref type="internal" target="3401.xml">3401</ref> H.32.6 x W.17.5cm; <ref type="internal" target="8669.xml">8669</ref> H.33 x W.19cm]. Non-royal <term xml:lang="lat">enteuxeis</term> are also written in <term xml:lang="lat">transversa
                        charta</term> format and can be wide and short [<ref type="internal" target="881.xml">881</ref> H.10 x W.39cm; <ref type="internal" target="994.xml">994</ref> H.7 x W.35cm; <ref type="internal" target="1517.xml">1517</ref> H.14 x W.42.5cm]; one example exceptionally so
                        [<ref type="internal" target="2102.xml">2102</ref> III BCE, Philadelphia,
                    H.6.5 x W.39.5cm]. Some with this orientation have horizontal fibres [<ref type="internal" target="1601.xml">1601</ref> III BCE, Philadelphia; <ref type="internal" target="2085.xml">2085</ref>]. Others are in <term xml:lang="lat">pagina</term> format with horizontal fibres [<ref type="internal" target="1068.xml">1068</ref> III BCE, Philadelphia] and some
                    in the long and narrow demotic format [<ref type="internal" target="1119.xml">1119</ref> III BCE, Philadelphia, H.37 x W.9cm; <ref type="internal" target="1767.xml">1767</ref> 251-250 BCE Philadelphia, H.33 x W.6.7cm], see <bibl>
                        <author>Sarri</author>
                        <ptr target="gramm:sarri2018"/>
                        <citedRange>95-97</citedRange>
                    </bibl>.</p>
            </div>
            <div xml:id="d14_div3_layout" type="section" n="3">
                <head xml:id="layout">Layout</head>
                <p xml:id="d14_p17">The text is displayed as a single block with only the final
                    salutation separated and centred along the vertical axis of the sheet [<ref type="internal" target="935.xml">935</ref>; <ref type="internal" target="1136.xml">1136</ref> III BCE, Philadelphia] or close to the right
                    hand margin [<ref type="internal" target="770.xml">770</ref> 256 BCE,
                    Alexandria; <ref type="internal" target="3329.xml">3329</ref>]. Some documents
                    have a large space between the end of the main text and the closing [<ref type="internal" target="1136.xml">1136</ref>; <ref type="internal" target="1603.xml">1603</ref> III BCE, Philadelphia]. Royal <term xml:lang="lat">enteuxeis</term> may present the opening address in <term xml:lang="lat">ekthesis</term> to the rest of the writing [<ref type="internal" target="3401.xml">3401</ref>; <ref type="internal" target="8669.xml">8669</ref>].</p>
                <p xml:id="d14_p18">There are examples with more than one column [<ref type="internal" target="681.xml">681</ref> 258 BCE, Alexandria; <ref type="internal" target="1946.xml">1946</ref> 251 BCE, Arsinoite nome; <ref type="internal" target="3679.xml">3679</ref>]. The writing can fill the
                    whole sheet completely [<ref type="internal" target="2101.xml">2101</ref> III
                    BCE, Philadelphia; <ref type="internal" target="121859.xml">121859</ref>],
                    becoming cramped towards the end [<ref type="internal" target="2105.xml">2105</ref> III BCE, Philadelphia] or there can be a large bottom margin
                        [<ref type="internal" target="1136.xml">1136</ref>; <ref type="internal" target="2209.xml">2209</ref> III BCE, Philadelphia]. The scribe can be
                    proficient [<ref type="internal" target="1796.xml">1796</ref>; <ref type="internal" target="1517.xml">1517</ref>], or not as neat and practised
                        [<ref type="internal" target="2220.xml">2220</ref>; <ref type="internal" target="2097.xml">2097</ref> both III BCE, Philadelphia]; [<ref type="internal" target="1090.xml">1090</ref> III BCE, Philadelphia] is
                    clearly a draft, judging from the corrections and supralinear additions.</p>
            </div>
        </body>
    </text>
</TEI>