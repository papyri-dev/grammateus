<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="c3">
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title>Classification of Greek Documentary Papyri: Objective Statement</title>
                <author corresp="../authority-lists.xml#SF">
                    <forename>Susan</forename>
                    <surname>Fogarty</surname>
                </author>
                <author corresp="../authority-lists.xml#PS">
                    <forename>Paul</forename>
                    <surname>Schubert</surname>
                </author>
                <respStmt>
                    <resp>Encoded in TEI P5 by</resp>
                    <name corresp="../authority-lists.xml#EN">
                        <forename>Elisa</forename>
                        <surname>Nury</surname>
                    </name>
                </respStmt>
                <respStmt>
                    <resp>With the collaboration of</resp>
                    <name corresp="../authority-lists.xml#LF">
                        <forename>Lavinia</forename>
                        <surname>Ferretti</surname>
                    </name>
                </respStmt>
                <funder>
                    <orgName key="SNF">Swiss National Science Foundation</orgName> - <ref target="https://data.snf.ch/grants/grant/182205">Project 182205</ref>
                </funder>
            </titleStmt>
            <publicationStmt>
                <authority>Grammateus Project</authority>
                <idno type="DOI">10.26037/yareta:q4es6diymvfl5cqulcr53jz34q</idno>
                <availability>
                    <licence target="https://creativecommons.org/licenses/by-nc/4.0/">Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)</licence>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <p>Born digital document. Encoded from a Microsoft Word document.</p>
            </sourceDesc>
        </fileDesc>
        <encodingDesc>
            <classDecl>
                <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:tei="http://www.tei-c.org/ns/1.0" href="../grammateus_taxonomy.xml"/>
            </classDecl>
            <p>This file is encoded to comply with TEI Guidelines P5, Version 4.0.0.
                    <ref>https://tei-c.org/guidelines/p5/</ref>
            </p>
        </encodingDesc>
        <profileDesc>
            <langUsage>
                <language ident="lat">Latin</language>
                <language ident="grc">Greek</language>
            </langUsage>
            <textClass>
                <catRef n="type" scheme="#grammateus_taxonomy" target="#gt_os"/>
            </textClass>
        </profileDesc>
        <revisionDesc>
            <change who="../authority_lists.xml#EN" when="2023">Archived on Yareta</change>
            <change who="../authority-lists.xml#EN" when="2022-06-08">TEI file created</change>
            <change who="../authority-lists.xml#SF" when="2022-05-20">Word document</change>
        </revisionDesc>
    </teiHeader>
    <text>
        <body>
            <head>Objective Statement</head>
            <p xml:id="c3_p1">The Type Objective Statement is comprised of documents styled as objective statements
                of a procedure or an action. Many of these come from official sources, but there are
                also documents which concern private transactions or the official record of a
                private transaction. The majority are objectively styled. <figure xml:id="c3_f1" n="1">
                    <head>Objective Statement Visualisation.</head>
                    <figDesc>The figure shows all the sub-types and variations in the OS category,
                        displayed in a tree diagram.</figDesc>
                    <graphic url="os.png" rend="inline"/>
                </figure>
                <figure xml:id="c3_f2" n="2">
                    <head>Objective Statement - Structural Elements.</head>
                    <figDesc>The figure shows a table of the general structure for each category of
                        OS documents.</figDesc>
                    <graphic url="os_table.jpg" rend="inline"/>
                </figure>
            </p>
            <div xml:id="c3_div1_synchoresis" type="section" n="1">
                <head xml:id="synchoresis">Synchoresis</head>
                <p xml:id="c3_p2">The <term xml:lang="lat">Synchoresis</term> is a notarial contract produced by
                    the <hi rend="italic">judicial courts</hi> which records a
                    private transaction. Unlike any of the other sub-types under the Type: Objective
                    Statement, this document has the <term xml:lang="lat">hypomnematic</term>
                    opening address more usually found in the Type Transmission of Information, along with a <seg xml:id="c3_s1" type="syntax" corresp="../authority_lists.xml#date">[date]</seg> placed at
                    the end of the document. This can be seen as a hybrid document incorporating
                    elements from two Types. It is placed under Objective Statement because it is
                    constructed with <seg xml:id="c3_s2" type="syntax" corresp="../authority_lists.xml#main-text">[<term xml:lang="grc">συνχωρεῖ</term> + infinitive]</seg>, and comes
                    from an official source.</p>
                <p xml:id="c3_p3">Alexandrian examples are simply addressed to the <term xml:lang="lat">archidikastes</term> [<ref type="internal" target="18561.xml">18561</ref> I
                    BCE, Alexandria], while later Oxyrhynchite examples carry a fuller address to
                    the official [<ref type="internal" target="16537.xml">16537</ref> II CE,
                    Oxyrhynchus]. An official subscription after the <seg xml:id="c3_s3" type="syntax" corresp="../authority_lists.xml#date">[date]</seg> may sometimes be added to
                    indicate the registration of the document [<ref type="internal" target="16537.xml">16537</ref> l. 33 <term xml:lang="grc">κατακεχώρι(σται)</term>].</p>
                <p xml:id="c3_p4">In many examples the name of the <term xml:lang="lat">archidikastes</term> is
                    visually separated from the rest of the text on a single line [<ref type="internal" target="18500.xml">18500</ref>, <ref type="internal" target="18541.xml">18541</ref>] or in <term xml:lang="lat">ekthesis</term>
                    to the rest of the text [<ref type="internal" target="20054.xml">20054</ref>
                    Alexandria; <ref type="internal" target="20057.xml">20057</ref> Arsinoite
                    nome].</p>
                <p xml:id="c3_p5">For other contracts under this type see Objective Statement: sub-types [<ref type="descr" target="#gt_syngr">syngraphe</ref>] and [<ref type="descr" target="#gt_pp">private protocol</ref>]. For contracts with a different
                    formulation see Epistolary Exchange: sub-type [<ref type="descr" target="#gt_cheiro">cheirographon</ref>], Transmission of Information:
                    sub-type [statement]: variations
                        [<ref type="descr" target="#gt_proposalContract">proposal to contract</ref>]
                    and [<ref type="descr" target="#gt_">undertaking</ref>].</p>
            </div>
            <div xml:id="c3_div2_syngraphe" type="section" n="2">
                <head xml:id="syngraphe">Syngraphe</head>
                <p xml:id="c3_p6">Documents constructed in the form of a [<ref type="descr" target="#gt_syngr">syngraphe</ref>] have a broad range of content and are constructed around
                    an objective statement of a lease, or loan <seg xml:id="c3_s4" type="syntax" corresp="../authority_lists.xml#main-text">[<term xml:lang="grc">ἐμίσθωσεν
                            /ἐδάνεισεν</term>
                        <hi rend="italic">name</hi> &lt;nom.&gt;][to <hi rend="italic">name</hi>
                        &lt;dat.&gt;]</seg>, or for other contracts and receipts, a verb of
                    agreement <seg xml:id="c3_s5" type="syntax" corresp="../authority_lists.xml#date">[<term xml:lang="grc">ὁμολογεῖ</term>] + infinitive</seg>.</p>
                <p xml:id="c3_p7">In a simple <term xml:lang="lat">syngraphe</term> the date is placed at the
                    beginning of the document and a statement of validation <seg xml:id="c3_s6" type="syntax" corresp="../authority_lists.xml#subscription">
                        <term xml:lang="grc">ἡ συγγραφὴ κυρία ἔστω</term>
                    </seg>
                        [<ref type="internal" target="13134.xml">13134</ref> l.38] at the end. With
                    the emergence of the <term xml:lang="lat">grapheion</term> in the II BCE, a
                    formal registration docket was placed after the main text <seg xml:id="c3_s7" type="syntax" corresp="../authority_lists.xml#subscription">[<hi rend="italic">name</hi>
                        &lt;nom.&gt; <term xml:lang="grc">κεχρημάτικα</term>]</seg> [<ref type="internal" target="66.xml">66</ref> 105 BCE, Pathyris].</p>
                <p xml:id="c3_p8">Some contracts and receipts in the form of a <term xml:lang="lat">syngraphe</term> have a subjective construction with <seg xml:id="c3_s8" type="syntax" corresp="../authority_lists.xml#main-text">[<term xml:lang="grc">ὁμολογῶ</term> + infinitive]</seg>. Even though they do not fulfill all
                    the criteria for inclusion under this Type (e.g. they are not objectively
                    styled), they are drawn up as <term xml:lang="lat">syngraphai</term> by a public
                    notary and officially registered, and so are placed here.</p>
                <div xml:id="c3_div3_doubleDoc" type="subsection" n="1">
                    <head xml:id="doubleDoc">Double Document</head>
                    <p xml:id="c3_p9">
                        <term xml:lang="lat">Syngraphai</term> drawn up as [<ref type="descr" target="#gt_doubleDoc">double documents</ref>] appear in Greek from the
                        late IV BCE. The scribe wrote the text twice on the same sheet, an inner
                        script rolled up and sealed, and an outer script, open and available for
                        consultation [<ref type="internal" target="5836.xml">5836</ref> 311-310 BCE;
                            <ref type="internal" target="5837.xml">5837</ref> 285-284 BCE (with
                        seals), both Elephantine]. Six witnesses were required to validate the
                        contract, along with a <term xml:lang="grc">συγγραφοφύλαξ</term> (keeper of
                        the <term xml:lang="lat">syngraphe</term>) to whom the document was
                        entrusted.</p>
                    <p xml:id="c3_p10">Double documents registered in the grapheion replaced the necessity for
                        witnesses and a <term xml:lang="grc">συγγραφοφύλαξ</term>, and led to the
                        addition of a registration docket at the end of the outer text, and the
                        eventual shrinking of the inner text to a single line – these are often
                        referred to as agoranomic deeds.</p>
                    <p xml:id="c3_p11">Another variety of double document, the <term xml:lang="lat">symbolon</term>,
                        displays a <seg xml:id="c3_s9" type="syntax" corresp="../authority_lists.xml#main-text">[<term xml:lang="grc">ὁμολογεῖ</term> + infinitive]</seg>
                        construction. Many of these are receipts [<ref type="internal" target="1765.xml">1765</ref> 252 BCE, Philadelphia].</p>
                    <p xml:id="c3_p12">Double documents are found in a
                        <hi rend="italic" xml:space="preserve">transversa charta</hi> format, with
                        the writing against vertical fibres, e.g. [<ref type="internal" target="5838.xml">5838</ref> III BCE, Elephantine], or in this
                        orientation with horizontal fibres. Some of these documents can be quite
                        wide [<ref type="internal" target="84.xml">84</ref> II BCE; <ref type="internal" target="95.xml">95</ref> I BCE both Pathyris]. Some
                            <term xml:lang="lat">symbolon</term> receipts are long and narrow,
                        typical of the demotic format [<ref type="internal" target="762.xml">762</ref> III BCE, Arsinoite nome].</p>
                    <p xml:id="c3_p13">For other contracts under this type see Objective Statement: sub-types [<ref type="descr" target="#gt_synch">synchoresis</ref>] and [<ref type="descr" target="#gt_pp">private protocol</ref>]. For contracts with
                        a different formulation see Epistolary Exchange: sub-type [<ref type="descr" target="#gt_cheiro">cheirographon</ref>], Transmission of Information:
                        sub-type [statement]:
                        variations [<ref type="descr" target="#gt_proposalContract">proposal to
                            contract</ref>] and [<ref type="descr" target="#gt_udertaking">undertaking</ref>].</p>
                </div>
            </div>
            <div xml:id="c3_div4_private_protocol" type="section" n="3">
                <head xml:id="private_protocol">Private Protocol</head>
                <p xml:id="c3_p14">The private protocol is a privately produced contract which follows the
                    formulation used by documents from notarial offices. However, as the document is
                    privately drafted, the scribe places the date at the end of the contract. This
                    means that the document begins directly with the objective statement <seg xml:id="c3_s10" type="syntax" corresp="../authority_lists.xml#main-text">[<term xml:lang="grc">ἐμίσθωσεν</term>
                        <hi rend="italic">name</hi> &lt;nom.&gt;][to <hi rend="italic">name</hi>
                        &lt;dat.&gt;]</seg> [<ref type="internal" target="15404.xml">15404</ref> III
                    CE]. These contracts are attested for the most part in the Oxyrhynchite nome,
                    usually for lease agreements, but also occur in the Arsinoite nome, drawn up
                    there for loan agreements [<ref type="internal" target="14118.xml">14118</ref>
                    223 CE] or the sale of animals
                    [<hi rend="color(FF0000)" xml:space="preserve">13078 </hi>299 CE].</p>
                <p xml:id="c3_p15">A distinctive feature of Oxyrhynchite private protocols is their presentation on long narrow sheets of
                    papyrus, i.e. in the demotic format [<ref type="internal" target="21745.xml">21745</ref> II CE].</p>
                <p xml:id="c3_p16">For other contracts under this type see Objective Statement: sub-types[<ref type="descr" target="#gt_syngr">syngraphe</ref>] and [<ref type="descr" target="#gt_synch">synchoresis</ref>]. For contracts with a different
                    formulation see Epistolary Exchange: sub-type [<ref type="descr" target="#gt_cheiro">cheirographon</ref>], Transmission of Information:
                    sub-type [statement]: variations
                        [<ref type="descr" target="#gt_proposalContract">proposal to contract</ref>]
                    and [<ref type="descr" target="#gt_undertaking">undertaking</ref>].</p>
            </div>
            <div xml:id="c3_div5_announcement" type="section" n="4">
                <head xml:id="announcement">Announcement</head>
                <p xml:id="c3_p17">This sub-type includes documents which are public announcements by the king,
                    prefect, or officials. These are also documents which might be publicly
                    displayed. While there is no single definitive sub-type, the variations are
                    distinguished by their purpose and specific statements.</p>
                <div xml:id="c3_div6_royal_ordinance" type="subsection" n="1">
                    <head xml:id="royal_ordinance">Royal Ordinance</head>
                    <p xml:id="c3_p18">A [<ref type="descr" target="#gt_decree">royal ordinance</ref>] (<term xml:lang="grc">πρόσταγμα</term>) is a decree or command from the
                        Ptolemaic period establishing a law or regulation, or setting a precedent.
                        It opens with the statement <seg xml:id="c3_s11" type="syntax" corresp="../authority_lists.xml#introduction">[<term xml:lang="grc">βασιλέως
                            προστάξαντος</term>]</seg>
                        <gloss>by order of the king</gloss> [<ref target="https://papyri.info/ddbdp/sb;6;9454_2">5775_2</ref>  270-269 BCE, Gurob], sometimes followed by the
                        name of an intermediary. The <seg xml:id="c3_s12" type="syntax" corresp="../authority_lists.xml#date">[date]</seg> follows the main
                        text. These decrees were usually publicly displayed and any that survive are
                        found embedded in other texts [<ref target="https://papyri.info/ddbdp/p.tebt;3.1;700">5311</ref> l.22-55, 124 BCE,
                        Tebtunis].</p>
                </div>
                <div xml:id="c3_div7_edict" type="subsection" n="2">
                    <head xml:id="edict">Edict</head>
                    <p xml:id="c3_p19">An edict (<term xml:lang="grc">διάταγμα</term>) is an instruction or order
                        issued to the population by the emperor or prefect to establish a law, and
                        so covers a wide range of subjects. It opens simply with <seg xml:id="c3_s13" type="syntax" corresp="../authority_lists.xml#introduction">[<hi rend="italic">name</hi> &lt;nom.&gt;] <term xml:lang="grc">λέγει·</term>
                        </seg>
                        <gloss>
                            <hi rend="italic">N</hi> says:</gloss>, e.g. [<ref type="internal" target="10801.xml">10801</ref> l.1-2, 134 CE, Theadelphia] followed by a
                        direct statement in the first person <term xml:lang="grc">κελεύω/ἐκέλευσα</term>
                        <gloss>I order/ordered</gloss> [<ref type="internal" target="10234.xml">10234</ref>
                        136 CE, Arsinoite nome]. An order to publicly post the document may follow
                            <term xml:lang="grc">προτεθήτω/πρόθες</term> [<ref type="internal" target="9034.xml">9034</ref> 138-161 CE, Arsinoite nome]. Those that
                        survive are generally copies or found embedded in other texts.</p>
                </div>
                <div xml:id="c3_div8_proclamation" type="subsection" n="3">
                    <head xml:id="proclamation">Proclamation</head>
                    <p xml:id="c3_p20">A [<ref type="descr" target="#gt_proclamation">proclamation</ref>] is a
                        formal announcement issued by high-ranking officials of the administration
                        and judiciary. They concerned a wide variety of subjects and were publicly
                        posted. Some open with the name and location of the official, sometimes
                        including the statement <seg xml:id="c3_s14" type="syntax" corresp="../authority_lists.xml#main-text">[<term xml:lang="grc">παραγγέλλεται</term>]</seg>, followed
                        by a signature and date. If the verb is not present the announcement begins
                        with <seg xml:id="c3_s15" type="syntax" corresp="../authority_lists.xml#introduction">
                            <term xml:lang="grc">ἐξ αὐθεντείας</term> [<hi rend="italic">name</hi>
                            &lt;gen.&gt;]</seg>
                        <gloss>on the authority of <hi rend="italic">N</hi>
                        </gloss> followed by a title
                            [<ref type="internal" target="16869.xml">16869</ref> 245-249 CE,
                        Oxyrhynchus].</p>
                </div>
                <div xml:id="c3_div9_programma" type="subsection" n="4">
                    <head xml:id="programma">Programma</head>
                    <p xml:id="c3_p21">The approval of a person nominated for a liturgy was made by the <term xml:lang="lat">strategos</term> through a [<ref type="descr" target="#gt_programma">programma</ref>], which was then publicly
                        displayed. Extant examples are mostly copies, indicated by the opening <term xml:lang="grc">ἀντίγραφον προγράμματος</term> [<ref type="internal" target="8942.xml">8942</ref> 169 CE, Arsinoite nome]; or if the copies
                        were pasted together in a <term xml:lang="grc">τόμος συγκολλήσιμος</term>,
                        the number of the sheet or roll [<ref type="external" target="https://papyri.info/ddbdp/p.flor;1;2col1">23526</ref> 165 CE,
                        Hermopolite nome]; or they survive as abstracts from registers indicated by
                        the opening <seg xml:id="c3_s16" type="syntax" corresp="../authority_lists.xml#introduction">
                            <term xml:lang="grc">ἐκ προγραμμάτων</term> + [date]</seg> [<ref type="external" target="https://papyri.info/ddbdp/p.sijp;;21b">110156</ref> 185 CE, Kynopolite nome]. The objective
                        statement is constructed with <term xml:lang="grc">παραγγέλλεται</term>
                        <gloss>it is proclaimed/announced</gloss> [<ref type="internal" target="8942.xml">8942</ref> l.13], but this may not always be present as the fact of
                        publication was in itself the approval [<ref type="internal" target="21792.xml">21792</ref> l.1-13, 260 CE, Oxyrhynchus]. The
                        signature of the <term xml:lang="lat">strategos</term> was added, followed
                        by the <seg xml:id="c3_s17" type="syntax" corresp="../authority_lists.xml#date">[date]</seg>.</p>
                    <p xml:id="c3_p22">As there were stages to the whole procedure, windows for later additions to
                        the document were left by the scribe [<ref type="internal" target="21792.xml">21792</ref>]. The latter example is long and narrow,
                        with the fibres horizontal, typical of the demotic format, but another
                        example suggests that these <term xml:lang="lat">programmata</term> may also
                        have been written <hi rend="italic">transversa charta</hi> [<ref type="descr" target="#gt_programma">programma<ptr target="#d35_p8"/></ref>].</p>
                </div>
            </div>
            <div xml:id="c3_div10_diagraphe" type="section" n="5">
                <head xml:id="diagraphe">Bank diagraphe</head>
                <p xml:id="c3_p23">A [<ref type="descr" target="#gt_diagraphe">bank digraphe</ref>] or bank receipt
                    records a payment made through a bank, and can be the final stage in a contract,
                    to which the receipt was added at the end. I CE examples survive as abstracts
                    from bank registers and so open with the heading <seg xml:id="c3_s18" type="syntax" corresp="../authority_lists.xml#introduction">
                        <term xml:lang="grc">ἀντίγραφον διαγραφῆς</term> [through <term xml:lang="grc">διά</term>
                        <hi rend="italic">bank</hi>]</seg> followed by the <seg xml:id="c3_s19" type="syntax" corresp="../authority_lists.xml#date">[date]</seg>, after which the names of
                    the parties are recorded. The verb <seg xml:id="c3_s20" type="syntax" corresp="../authority_lists.xml#main-text">[<term xml:lang="grc">διέγραψε</term>]</seg> is not explicitly stated, though implied.</p>
                <p xml:id="c3_p24">II-III CE bank receipts are self-contained documents, no longer appended to a
                    contract, but as contracts in their own right with the <seg xml:id="c3_s21" type="syntax" corresp="../authority_lists.xml#date">[date@start]</seg>,
                    followed by the name of the bank <seg xml:id="c3_s22" type="syntax" corresp="../authority_lists.xml#main-text">[through <hi rend="italic">bank</hi>
                        <term xml:lang="grc">διά</term> &lt;gen.&gt;]</seg>, and continuing with the
                    names of the parties [<ref type="internal" target="9302.xml">9302</ref> 151 CE
                    Arsinoite nome]. There is no conjugated verb to introduce the infinitive which
                    may be <term xml:lang="grc">ἔχειν / ἀπέχειν / πεπρακέναι</term>.</p>
                <p xml:id="c3_p25">First century CE registers of bank receipts from the Hermopolite nome also
                    include copies of the relevant contract, written in wide sheets in more than one
                    column [<ref target="https://papyri.info/ddbdp/p.lond;3;1168">22825</ref> 44 CE,
                    with bank receipt in col.(iii)]. This format continued into the following
                    centuries. Those from the Arsinoite nome are in <term xml:lang="lat">pagina</term> format.</p>
            </div>
            <div xml:id="c3_div11_receipt" type="section" n="6">
                <head xml:id="receipt">Receipt</head>
                <p xml:id="c3_p26">The majority of receipts are framed as objective statements with the content
                    being the variable element.</p>
                <div xml:id="c3_div12_toll" type="subsection" n="1">
                    <head xml:id="toll">Toll Receipt</head>
                    <p xml:id="c3_p27">A [<ref type="descr" target="#gt_os_toll">toll</ref>] or customshouse receipt
                        is a document that attests the payment of a tax levied on goods transiting
                        through a toll, or checkpoint. The receipt opens directly with the objective
                        statement recording which toll has been paid, <term xml:lang="grc">τετελώνηται διὰ πύλης Σοκνοπαίου Νήσου λιμένος Μέμφεως</term>
                        <gloss>the tax for the harbour of Memphis has been paid (for passage) through
                            the gate of Soknopaiou Nesos</gloss> [<ref type="internal" target="15075.xml">15075</ref> 146 CE, Soknopaiu Nesos]. Others may
                        simply state <term xml:lang="grc">πάρες διὰ πύλης</term>
                        <gloss>let pass through the gates...</gloss> and record the number of donkeys or
                        camels and the goods they carry [<ref type="internal" target="10914.xml">10914</ref> 80 CE, Arsinoite nome]. The <seg xml:id="c3_s23" type="syntax" corresp="../authority_lists.xml#date">[date]</seg> of payment is placed
                        at the end followed by the signature of an official.</p>
                    <p xml:id="c3_p28">A line may be drawn at the end of the text to prevent any further additions
                            [<ref type="internal" target="15072.xml">15072</ref> II CE, Soknopaiou
                        Nesos] and there may be a seal [<ref type="internal" target="14355.xml">14355</ref> 133 CE, Philadelphia]. Some receipts were prepared
                        beforehand, as evidenced by a change of hand where the details of the
                        transporter and transportation are entered [<ref target="https://papyri.info/ddbdp/p.customs;;406">26703</ref> II-III CE,
                        Arsinoite nome].</p>
                </div>
                <div xml:id="c3_div13_penthemeros" type="subsection" n="2">
                    <head xml:id="penthemeros">Penthemeros receipt</head>
                    <p xml:id="c3_p29">Upon completion of the annual obligation to work five days on dyke and canal
                        maintenance, a [<ref type="descr" target="#gt_penthemeros">penthemeros</ref>] receipt was issued. These receipts were produced in a
                        few stages: the scribe in the office of the presiding official (the <term xml:lang="grc">κατασπορεύς</term>) wrote the date (year only), and the
                        objective statement <term xml:lang="grc">εἴργασται ὑπὲρ χωματικῶν</term>
                        <gloss>work has been done on the dykes</gloss> [<ref type="internal" target="14301.xml">14301</ref> II CE, Tebtunis]. At the work site the
                        remaining details were completed by the overseer, i.e. the date (month,
                        days), the location, and the name of the labourer [<ref type="internal" target="9452.xml">9452</ref> II CE, Theadelphia]. An official signature
                        may follow [<ref type="internal" target="14581.xml">14581</ref> 146 CE,
                        Theadelphia].</p>
                    <p xml:id="c3_p30">Many of these receipts are squarish in format [<ref type="internal" target="13877.xml">13877</ref> III CE, Philadelphia], or are
                        horizontally oriented with the writing along the fibres [<ref type="internal" target="9393.xml">9393</ref> II CE, Karanis].</p>
                </div>
                <div xml:id="c3_div14_sitologos" type="subsection" n="4">
                    <head xml:id="sitologos">Sitologos receipt</head>
                    <p xml:id="c3_p31">The <term xml:lang="lat">sitologos</term> issued receipts for the payment of
                        land taxes and also for transactions concerning private stocks held in the
                        local granary. The former often involve a subjective statement; the latter
                        an objective one. Receipts for private transactions do not usually include a
                        date of issue, but will refer to the year of harvest from which the amount
                        is to be taken.</p>
                    <p xml:id="c3_p32">Sitologos receipts are also composed as objective statements in the form of a
                            [<ref type="descr" target="#gt_syngr">syngraphe</ref>], with an
                        infinitive construction <seg xml:id="c3_s24" type="syntax" corresp="../authority_lists.xml#main-text">[<term xml:lang="grc">ὁμολογεῖ...μεμετρῆ(σθαι)</term>]</seg> [<ref type="internal" target="3124.xml">3124</ref> II BCE Hermopolite nome].</p>
                    <p xml:id="c3_p33">There are a variety of formats, the writing predominantly along horizontal
                        fibres. The text is written as a single block, perhaps with an enlarged
                        first letter [<ref type="internal" target="11732.xml">11732</ref> III CE,
                        Nilopolis], and little use of <term xml:lang="lat">ekthesis</term> or <term xml:lang="lat">eisthesis</term>.</p>
                </div>
                <div xml:id="c3_div15_tax" type="subsection" n="5">
                    <head xml:id="tax">Poll-Tax receipt</head>
                    <p xml:id="c3_p34">A [poll-tax] (<term xml:lang="grc">λαογραφία</term>) was imposed on all male citizens from their 14th year. Most poll-tax receipts were drawn up by the scribe as an objective statement [<ref type="internal" target="10901.xml">10901</ref> 194 CE, Euhemeria; <ref target="14374.xml" type="internal">14374</ref> 181 CE, Tebtunis]. These open with a <seg xml:id="c3_s25" type="syntax" corresp="./authority_lists.xml#date">[date@start]</seg> written out in full [<ref type="internal" target="10435.xml">10435</ref> 166 CE, Ptolemais Euergetis]. The term <seg xml:id="c3_s26" type="syntax" corresp="./authority_lists.xml#date">[<term xml:lang="grc">ἀριθμήσεως / εἰς ἀρίθμησιν</term>]</seg> may be added just after the date, e.g. [<ref type="internal" target="12837.xml">12837</ref> 174 CE, Ptolemais Euergetis] indicating that the payment is to be attributed to the previous month.</p>
                    <p xml:id="c3_p35">They are drawn up in a variety of formats. There can be more than one receipt written out on the same sheet, uniformly displaying the same distinctive features [<ref target="12835.xml" type="internal">12835</ref> 141 CE, Ptolemais Euergetis; <ref type="internal" target="13848.xml">13848</ref> 107 CE, Tebtunis].</p>
                    <p xml:id="c3_p36">For receipts with a different typology, see Epistolary Exchange: sub-type:
                            [<ref type="descr" target="#gt_ee_receipt">receipt</ref>] and Objective
                            Statement [<ref type="descr" target="#gt_syngr">syngraphe<ptr target="#homologia_obj"/></ref>] and
                        variation [<ref type="descr" target="#gt_doubleDoc">double
                        document<ptr target="#symbolon"/></ref>].</p>
                </div>
            </div>
            <div xml:id="c3_div16_certificate" type="section" n="7">
                <head xml:id="certificate">Certificate of Calf Sacrifice</head>
                <p xml:id="c3_p37">A certificate of purity was issued on examination by the priest of any calf
                    offered for sacrifice in a temple. The receipt opens with a <seg xml:id="c3_s27" type="syntax" corresp="../authority_lists.xml#date">[date@start]</seg>, the main
                    text acknowledges the examination <term xml:lang="grc">ἐπεθεώρησα</term>
                    <gloss>I inspected</gloss>, describes the animal, and states that a seal of purity has
                    been affixed to the animal’s horn [<ref type="internal" target="13878.xml">13878</ref> II CE, Soknopaiou Nesos].</p>
                <p xml:id="c3_p38">These receipts are usually written on small squarish pieces of papyri, with the
                    writing along horizontal or vertical fibres.</p>
            </div>
            <div xml:id="c3_div17_invitation" type="section" n="8">
                <head xml:id="invitation">Invitation</head>
                <p xml:id="c3_p39">An invitation is a privately produced document asking the recipient to attend a
                    celebration. There is no date, no opening or closing salutation, and it usually
                    comprises a single objective request beginning <seg xml:id="c3_s28" type="syntax" corresp="../authority_lists.xml#main-text">
                        <term xml:lang="grc">ἐρωτᾷ /
                            καλεῖ σε</term> [<hi rend="italic">name</hi> &lt;nom.&gt;]</seg>
                    <gloss>
                        <hi rend="italic">N</hi> requests/asks you</gloss> with only the host’s name
                    specified.</p>
                <p xml:id="c3_p40">Invitations are written on small slips of papyrus, many of which are <hi rend="italic">transversa charta</hi> [<ref type="internal" target="30389.xml">30389</ref> III CE, Oxyrhynchus], or horizontally
                    oriented with the writing along the fibres [<ref type="internal" target="28950.xml">28950</ref> II-III CE, Oxyrhynchus].</p>
            </div>
        </body>
    </text>
</TEI>