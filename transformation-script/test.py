#!/usr/bin/env python

from lxml import etree

# TEI namespace
ns = {'tei': 'http://www.tei-c.org/ns/1.0'}

# etree parser
parser = etree.XMLParser(remove_blank_text=True)

# remove namespace prefixes from xml elements
def strip_ns_prefix(tree):
    #iterate through only element nodes (skip comment node, text node, etc) :
    for element in tree.xpath('descendant-or-self::*'):
        #if element has prefix...
        if element.prefix:
            print(element.prefix)
            #replace element name with its local name
            element.tag = etree.QName(element).localname
    return tree

tree = etree.parse('grammateus-template.xml', parser)
root = tree.getroot()

strip_ns_prefix(tree)

tree.write("test.xml", pretty_print=True)
