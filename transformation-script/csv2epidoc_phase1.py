#!/usr/bin/env python
# coding: utf-8

# import modules
import csv # work with tab separated text matrices
from lxml import etree # work with XML structure
import re # regular expressions
from datetime import date
import sys # get arguments
import os # manipulate file paths
import glob

# ## Usage
# with the command line:
#
# 1. with argument
# > python csv2epidoc.py path/to/output/for/TEI/files
#
# 2. without arguments (default path for output set in variables below)
# > python csv2peidoc.py

# ## Variables
nb_record_created = 0

# TEI namespace
ns = {'tei': 'http://www.tei-c.org/ns/1.0'}

# etree parser
parser = etree.XMLParser(remove_blank_text=True)

# dict of links between epidoc files and external links files
links_history = {}
links_texts = {}
# for images
links_images = {}
links_iiif = {}
links_canvas = {}
links_iframe = {}

# file paths
defaultfolder = '../matrices/textfiles'
datapath = '../data/'
papyripath = datapath+'papyri/'
historypath = datapath+'external_links_history.xml'
textpath = datapath+'external_links_texts.xml'
imagepath = datapath+'external_links_images.xml'
matrices_txt_from = "G:/Mon Drive/Grammateus/Matrices/textfiles/"
matrices_txt_to = defaultfolder


# ## Functions

# take as argument a path to a folder with matrices
def get_folder_path():
    try:
        folder_path = os.path.normpath(sys.argv[1])
        return folder_path
    except (TypeError, IndexError):
        print("No folder path has been included. Using the default folder: "+defaultfolder)
        return defaultfolder

# get list of files in the folder as absolute paths
def get_files_path(folder_path):
    try:
        files_path = [os.path.join(folder_path, x) for x in os.listdir(folder_path)]
        # check if the folder is empty
        if len(files_path) == 0:
            sys.exit("There are no matrices to convert to epidoc.")
        return files_path
    except FileNotFoundError:
        sys.exit("The specified folder does not exist.")

# get papyri list from matrix
def create_papyri_list(file):
    # empty papyri list
    list_papyri = []
    # read file
    with open(file, newline = '', encoding="utf-8") as matrix:
        matrix_reader = csv.DictReader(matrix, delimiter='\t')
        for papyrus in matrix_reader:
            # create a dictionary from zipped header-cell value
            list_papyri.append(dict(papyrus))
    # returns list of papyri
    return list_papyri


# transforms one papyrus into an epidoc etree from template
def create_epidoc(rowdata, list_tm):
    # create new xml tree from template
    tree = etree.parse('grammateus-template.xml', parser)
    root = tree.getroot()

    # ----- debug -----
    #if rowdata['TM'] == '685':
    #    print(rowdata['format-subtype'])

    # ----- title and IDs -----

    # title - from series-[volume]-numbers-[generic]-[page]-[number]
    title = root.find('.//tei:titleStmt/tei:title', ns)
    bibl = etree.SubElement(title, 'bibl')
    bibl.tail = None
    # add each element of the title into <bibl> with <biblScope>
    # series
    series = etree.SubElement(bibl, 'title')
    series.set('level', 's')
    series.set('type', 'abbreviated')
    series.text = rowdata['series']
    # volume
    if rowdata['volume'] != '':
        volume = etree.SubElement(bibl, 'biblScope')
        volume.set('unit', 'volume')
        volume.text = rowdata['volume']
    # fascicle
    if rowdata['fascicle'] != '':
        fascicle = etree.SubElement(bibl, 'biblScope')
        fascicle.set('unit', 'fascicle')
        fascicle.text = rowdata['fascicle']
    # numbers
    numbers = etree.SubElement(bibl, 'biblScope')
    numbers.set('unit', 'numbers')
    numbers.text = rowdata['numbers']
    # side (r / v). It is present in one matrix only
    if 'side' in rowdata and rowdata['side'] != '':
        side = etree.SubElement(bibl, 'biblScope')
        side.set('unit', 'side')
        side.text = rowdata['side']
    # generic (a / b / c...)
    if rowdata['generic'] != '':
        generic = etree.SubElement(bibl, 'biblScope')
        generic.set('unit', 'generic')
        generic.text = rowdata['generic']
    if rowdata['page'] != '':
        page = etree.SubElement(bibl, 'biblScope')
        page.set('unit', 'page')
        page.text = rowdata['page']
    if rowdata['number'] != '':
        nb = etree.SubElement(bibl, 'biblScope')
        nb.set('unit', 'number')
        nb.text = rowdata['number']

    # TM number
    tm = root.find('.//tei:idno[@type="TM"]', ns)
    tm.text = rowdata['TM']

    # HGV number(s)
    hgv = root.find('.//tei:idno[@type="hgv"]', ns)
    hgv.text = rowdata['HGV']
    # save as a list
    hgv_id = rowdata['HGV'].split(' ')

    # ddb-hybrid
    ddbhybrid = root.find('.//tei:idno[@type="ddb-hybrid"]', ns)
    ddbhybrid.text = rowdata['DDB']

    # ddb-filename
    ddb = rowdata['DDB'].replace(';', '.')
    ddb = ddb.replace('..', '.')
    ddbfilename = root.find('.//tei:idno[@type="ddb-filename"]', ns)
    ddbfilename.text = ddb

    # unique filename
    # if TM number exists more than once,
    if list_tm.count(rowdata['TM']) > 1:
        # use HGV instead (e.g. 28932)
        file_id = hgv_id[0]
    else:
        file_id = rowdata['TM']

    # ----- support -----
    # fibres
    support = root.find('.//tei:support', ns)
    support.set('ana', '../authority-lists.xml#'+rowdata['fibres'])

    # dimensions
    height = support.find('.//tei:height', ns)
    height.text = rowdata['height']
    width = support.find('.//tei:width', ns)
    width.text = rowdata['width']

    # ----- layout -----
    # columns
    layout = root.find('.//tei:layout', ns)
    layout.set('columns', rowdata['columns'])

    # margins
    top = layout.find('.//tei:dim[@n="top"]', ns)
    top.text = rowdata['margin-top']
    bottom = layout.find('.//tei:dim[@n="bottom"]', ns)
    bottom.text = rowdata['margin-bottom']
    right = layout.find('.//tei:dim[@n="right"]', ns)
    right.text = rowdata['margin-right']
    left = layout.find('.//tei:dim[@n="left"]', ns)
    left.text = rowdata['margin-left']

    # blank spaces
    if rowdata['blank-after'] != '':
        lines = rowdata['blank-after'].split(', ')
        #blank_height = rowdata['blank hgt.'].split(', ')
        blank = layout.find('.//tei:ab[@type="blank"]', ns)
        for l in lines:
            locus = etree.SubElement(blank, 'locus')
            try:
                locus.set('from', l)
                locus.set('to', str(int(l)+1))
            except ValueError:
                print("Error in blank-after column for papyrus TM"+rowdata['TM'])


    # text sections
    sections = root.find('.//tei:ab[@type="section"]', ns)
    try:
        int(rowdata['section-nb'])
    except ValueError:
        sys.exit("Error in section-nb column for papyrus TM"+rowdata['TM'])
    # for each section according to section-nb
    for s in range(1, int(rowdata['section-nb'])+1):
        # create element with xml ID
        sec = etree.SubElement(sections, 'locus')
        sec.set("{http://www.w3.org/XML/1998/namespace}id", 'section'+str(s))
        if rowdata['section'+str(s)] == 'missing':
            # if missing section
            sec.text = 'missing'
        else:
            # else add attributes
            try:
                lines, hand, sectionName = rowdata['section'+str(s)].split('/')
            except ValueError:
                print("not enough value to upack for papyrus: "+rowdata['TM']+", at column:"+'section'+str(s))
            sec.set('from', lines.split('-')[0])
            sec.set('to', lines.split('-')[1])
            sec.set('corresp', hand)
            sec.set('ana', '../authority-lists.xml#'+sectionName)

    # ----- handDesc -----
    handDesc = root.find('.//tei:handDesc', ns)
    handDesc.tail = None
    # for each hand according to section-nb
    for h in range(1, int(rowdata['hand-nb'])+1):
        # create element with xml ID
        handNote = etree.SubElement(handDesc, 'handNote')
        handNote.set("{http://www.w3.org/XML/1998/namespace}id", 'm'+str(h))
        # add attributes
        if "/" in rowdata['m'+str(h)]:
            section, scribe = rowdata['m'+str(h)].split('/')
        else:
            section, scribe = (rowdata['m'+str(h)], "")
        handNote.set('corresp', section)
        if scribe:
            handNote.set('scribeRef', '../authority-lists.xml#'+scribe)

    # ----- sealDesc -----
    # TODO: use papyri.info encoding?
    physDesc = root.find('.//tei:physDesc', ns)
    sealDesc = etree.SubElement(physDesc, 'sealDesc')
    seal = etree.SubElement(sealDesc, 'seal')
    p = etree.SubElement(seal, 'p')
    if rowdata['seal'].lower() == 'yes' or rowdata['seal'].lower() == 'y':
        p.text = 'Yes'
    else:
        p.text = 'No'

    # ----- history -----
    history = root.find('.//tei:history', ns)
    history_corresp = '#link_history_'+file_id
    history.set('corresp', history_corresp)

    # ----- profileDesc -----
    # format type/subtype/variation
    # this is assuming only 1 type and 1 subtype, and 1 optional variation
    # get IDs
    f_type_id = '#'+rowdata['format-type']
    f_subtype_id = '#'+rowdata['format-subtype']
    f_variation_id = '#'+rowdata['format-variation']
    # set catRef
    f_type = root.find('.//tei:catRef[@n="format-type"]', ns)
    f_type.set('target', f_type_id)
    f_subtype = root.find('.//tei:catRef[@n="format-subtype"]', ns)
    f_subtype.set('target', f_subtype_id)
    if 'format-variation' in rowdata and rowdata['format-variation']!= '':
        f_variation = root.find('.//tei:catRef[@n="format-variation"]', ns)
        f_variation.set('target', f_variation_id)

    # Grammateus keywords
    kw = root.find('.//tei:keywords[@scheme="grammateus"]', ns)
    kw_gramm = rowdata['hgv-en'].split(',')
    kw.tail = None
    for term in kw_gramm:
        t = etree.SubElement(kw, 'term')
        t.set('{http://www.w3.org/XML/1998/namespace}lang', 'en')
        t.text = term

    # HGV Subjects
    # removing HGV subjects because inconsistencies in matrices, not used by project team
    # hgv_de = rowdata['hgv-de'].split(',')
    # kw = root.find('.//tei:keywords[@scheme="hgv"]', ns)
    # kw.tail = None
    # for term in hgv_de:
    #     t = etree.SubElement(kw, 'term')
    #     t.set('{http://www.w3.org/XML/1998/namespace}lang', 'de')
    #     t.text = term

    # ----- facsimile -----
    # image
    image = rowdata['image'].split(' and ') # in rare cases when there are more than one link
    manifest = list(filter(None, rowdata['IIIF'].split(' and '))) # filters out empty IIIF
    canvasID = rowdata['canvasID']
    iframe = rowdata['iframe']
    facs = root.find('.//tei:facsimile/tei:graphic', ns)
    facs_corresp = '#link_facs_'+file_id
    facs.set('corresp', facs_corresp)

    # ----- text -----
    textdiv = root.find('.//tei:body/tei:div', ns)
    text_corresp = '#link_text_'+file_id
    textdiv.set('corresp', text_corresp)

    # ----- lists for external files -----
    # get link to history
    links_history[history_corresp] = ['https://papyri.info/hgv/'+n+'/source' for n in hgv_id]
    # get link to images
    links_images[facs_corresp] = image
    links_iiif[facs_corresp] = manifest
    links_canvas[facs_corresp] = canvasID
    links_iframe[facs_corresp] = iframe
    # get link to papyri.info
    links_texts[text_corresp] = ['https://papyri.info/ddbdp/'+rowdata['DDB']+'/source']

    # ----- cleaning and saving -----
    # date of record created
    recordcreated = root.find('.//tei:revisionDesc/tei:change', ns)
    recordcreated.set('when', date.today().isoformat())

    # strip namespace prefixes from results
    strip_ns_prefix(tree)

    # save
    save_epidoc_file(file_id, tree)

# external links functions

def create_new_items(links, tree, manifest={}, canvas={}, iframe={}):
    root = tree.getroot()
    # for each papyrus in list of links
    for key, value in links.items():
        # create item element
        item = etree.SubElement(root, 'item')
        # set @xml:id as corresp link without the 1st character '#'
        item.set('{http://www.w3.org/XML/1998/namespace}id', key[1:])
        # for each link attached to a papyrus
        for v in value:
            # create ref subelement(s)
            ref = etree.SubElement(item, 'ref')
            # set @target attribute as link
            ref.set('target', v)
        # if iiif exists
        if key in manifest and manifest[key]:
            # for each manifests
            for index, m in enumerate(manifest[key], start=1):
                # create a <ref> @type iiif with  the link
                ref = etree.SubElement(item, 'ref')
                ref.set('target', m)
                ref.set('type', 'iiif')
                ref.set('n', str(index))
                # for manifests of a whole book (e.g. nyu, TM 22674)
                if key in canvas and canvas[key]:
                    # add a subelement ref @type canvasID
                    canvaselmt = etree.SubElement(ref, 'ref')
                    canvaselmt.set('target', canvas[key])
                    canvaselmt.set('type', 'canvasID')
        # if iframe exists
        if key in iframe and iframe[key]:
            ref = etree.SubElement(item, 'ref')
            ref.set('target', iframe[key])
            ref.set('type', 'iframe')
    return tree

def add_new_items(links, tree, manifest={}, canvas={}, iframe={}):
    root = tree.getroot()
    # for each papyrus in matrix, add or replace links
    for key, value in links.items():
        # check if record already exists
        item = root.find('.//tei:item[@{http://www.w3.org/XML/1998/namespace}id="'+key[1:]+'"]', ns)
        # yes - replace with new
        if item is not None:
            # remove existing ref(s)
            for ref in list(item):
                item.remove(ref)
            # replace with matrix ref(s)
            for v in value:
                ref = etree.SubElement(item, 'ref')
                ref.set('target', v)
                # if iiif exists
                if key in manifest and manifest[key]:
                    # for each manifests
                    for index, m in enumerate(manifest[key], start=1):
                        # create a <ref> @type iiif with  the link
                        ref = etree.SubElement(item, 'ref')
                        ref.set('target', m)
                        ref.set('type', 'iiif')
                        ref.set('n', str(index))
                        # for manifests of a whole book (e.g. nyu, TM 22674)
                        if key in canvas and canvas[key]:
                            # add a subelement ref @type canvasID
                            canvaselmt = etree.SubElement(ref, 'ref')
                            canvaselmt.set('target', canvas[key])
                            canvaselmt.set('type', 'canvasID')
                # if iframe exists
                if key in iframe and iframe[key]:
                    ref = etree.SubElement(item, 'ref')
                    ref.set('target', iframe[key])
                    ref.set('type', 'iframe')
        # no - create new one
        else:
            # create item element
            item = etree.SubElement(root, 'item')
            item.set('{http://www.w3.org/XML/1998/namespace}id', key[1:])
            # for each link
            for v in value:
                # create ref subelement(s)
                ref = etree.SubElement(item, 'ref')
                # set @target attribute as actual link
                ref.set('target', v)
                # if iiif exists
                if key in manifest and manifest[key]:
                    # for each manifests
                    for index, m in enumerate(manifest[key], start=1):
                        # create a <ref> @type iiif with  the link
                        ref = etree.SubElement(item, 'ref')
                        ref.set('target', m)
                        ref.set('type', 'iiif')
                        ref.set('n', str(index))
                        # for manifests of a whole book (e.g. nyu, TM 22674)
                        if key in canvas and canvas[key]:
                            # add a subelement ref @type canvasID
                            canvaselmt = etree.SubElement(ref, 'ref')
                            canvaselmt.set('target', canvas[key])
                            canvaselmt.set('type', 'canvasID')
                # if iframe exists
                if key in iframe and iframe[key]:
                    ref = etree.SubElement(item, 'ref')
                    ref.set('target', iframe[key])
                    ref.set('type', 'iframe')
    return tree

def remove_deleted_items(links, tree):
    root = tree.getroot()
    # for each item already in the tree
    for item in root.iter('{http://www.tei-c.org/ns/1.0}item'):
        # we can check the first attribute because we know there is only one (xml:id)
        # if the first attribute value is not in the list of links
        if '#'+item.values()[0] not in links:
            # remove <item> element
            root.remove(item)
    return tree


# save epidoc etree
def save_epidoc_file(TMnumber, tree):
    # use the TM number as filename
    outpath = papyripath+TMnumber+'.xml'
    # write tree in file
    tree.write(outpath, pretty_print=True)

# remove namespace prefixes from xml elements
def strip_ns_prefix(tree):
    #iterate through only element nodes (skip comment node, text node, etc) :
    for element in tree.xpath('descendant-or-self::*'):
        #if element has prefix...
        if element.prefix:
            #replace element name with its local name
            element.tag = etree.QName(element).localname
    return tree

# get list of all tm numbers to find doublons
def list_tm_nb(files):
    list_tm = []
    for file in files:
        with open(file, encoding="utf-8") as csv_file:
            csv_reader = csv.reader(csv_file, delimiter='\t')
            header = next(csv_reader) # ignore 1st header row
            for lines in csv_reader:
                list_tm.append(lines[0]) # use TM identifier
    return list_tm

# ----------------------
# ## Main Transformation

# empty the data folder
files = glob.glob(papyripath+'*')
for f in files:
    os.remove(f)

# remove external links files
try:
    os.remove(historypath)
    os.remove(textpath)
    os.remove(imagepath)
except:
    print("Did not delete external links files.")

# update textfiles matrices from synced google drive
for file  in os.listdir(matrices_txt_from):
    # only matrices (not desktop.ini)
    if file.endswith(".txt"):
        # read file from synced folder
        with open(matrices_txt_from+file, "r", encoding="utf-8") as f:
            content = f.read()
        # write to local folder
        with open(matrices_txt_to+"/"+file, "w", encoding="utf-8") as f:
            f.write(content)

# get path to matrices folder
folder_path = get_folder_path()

# get list of paths to the matrices files in folder
files = get_files_path(folder_path)

print('===============')

# get list of tm numbers
list_tm = list_tm_nb(files)

# find doublons
set_tm = set(list_tm)
print('Doublons:'+str(len(list_tm)-len(set_tm))+'\n---')
for tm in set_tm:
    if list_tm.count(tm) > 1:
        print('There are '+str(list_tm.count(tm))+' papyri with ID '+tm)
print('===============')

# for each matrix
for file in files:

    # create a list of papyri from each row of the matrix
    paplist = create_papyri_list(file)

    # for each papyrus
    for pap in paplist:

        # generate an epidoc file
        create_epidoc(pap, list_tm)

    # print number of records created
    x = sum(1 for pap in paplist)
    filename = os.path.basename(file)
    print(str(x)+' records created for '+filename)

    # add them to the total
    nb_record_created = nb_record_created + x

# print total nb of records created
print('Total created: ', nb_record_created)


# ## Update Links Files

# ----- history -----

# if file already exists
if os.path.exists(historypath):

    # load existing file
    tree = etree.parse(historypath, parser)

    # add new links/update existing links
    tree = add_new_items(links_history, tree)

    # remove links for papyri that were not in the matrix
    tree = remove_deleted_items(links_history, tree)

else:
    # load empty file for history links
    tree = etree.parse('empty_external_links_history.xml', parser)

    # create new items for all links
    tree = create_new_items(links_history, tree)

# strip namespace prefixes from results => we keep the xinclude prefix
#strip_ns_prefix(tree)

# save
tree.write(historypath, pretty_print=True)

# ----- images -----

# if file already exists
if os.path.exists(imagepath):

    # load existing file
    tree = etree.parse(imagepath, parser)

    # add new links/update existing links
    tree = add_new_items(links_images, tree, manifest=links_iiif, canvas=links_canvas, iframe=links_iframe)

    # remove links for papyri that were not in the matrix
    tree = remove_deleted_items(links_images, tree)

else:
    # load empty file for image links
    tree = etree.parse('empty_external_links_images.xml', parser)

    # create new items for all links
    tree = create_new_items(links_images, tree, manifest=links_iiif, canvas=links_canvas, iframe=links_iframe)

# strip namespace prefixes from results
strip_ns_prefix(tree)

# save
tree.write(imagepath, pretty_print=True)

# ----- texts -----

# if file already exists
if os.path.exists(textpath):

    # load existing file
    tree = etree.parse(textpath, parser)

    # add new links/update existing links
    tree = add_new_items(links_texts, tree)

    # remove links for papyri that were not in the matrix
    tree = remove_deleted_items(links_texts, tree)

else:
    # load empty file for texts links
    tree = etree.parse('empty_external_links_texts.xml', parser)

    # create new items for all links
    tree = create_new_items(links_texts, tree)

# strip namespace prefixes from results
strip_ns_prefix(tree)

# save
tree.write(textpath, pretty_print=True)


# TODO: use XML validation? https://lxml.de/validation.html
