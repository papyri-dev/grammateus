<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:exist="http://exist-db.org/xquery/kwic" xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs tei" version="2.0">

    <xsl:param name="id"/>
    
    <xsl:variable name="taxonomy">../../data/grammateus_taxonomy.xml</xsl:variable>
    <xsl:variable name="descriptions">../../data/descriptions/</xsl:variable>
    <xsl:variable name="root">https://grammateus.unige.ch/</xsl:variable>
    
    <xsl:variable name="category" select="doc($taxonomy)//id($id)"/>
    
    <xsl:template match="/">
        <div class="text">
            <!-- title -->
            <h1 class="h1">
                <xsl:value-of select="$category/tei:catDesc/string()"/>
            </h1>
            <!-- for each sub-category (subtype and/or variation) -->
            <xsl:for-each select="$category//tei:category[tei:catDesc/tei:ptr[@type eq 'descr']]">
                <!-- get description file -->
                <xsl:variable name="descr-path" select="./tei:catDesc/tei:ptr[@type eq 'descr']/@target"/>
                <xsl:variable name="descr-link" select="substring-before($descr-path, '.xml')"/>
                <xsl:variable name="descr-file" select="doc(concat($descriptions, $descr-path))"/>
                <!-- subtitle -->
                <h3 class="h3">
                    <a href="{$root}descriptions/{$descr-link}">
                        <xsl:value-of select="./tei:catDesc/string()"/>
                    </a>
                </h3>
                <!-- contents -->
                <ol>
                    <xsl:for-each select="$descr-file//tei:body/tei:div[@type eq 'section']">
                        <li>
                            <a href="{$root}descriptions/{$descr-link}#{./tei:head/@xml:id}">
                                <!-- tokenize the text to include only the first part of the heading and no greek text in parenthesis-->
                                <xsl:choose>
                                    <xsl:when test="./tei:head[1]/tei:term">
                                        <xsl:value-of select="./tei:head[1]/tokenize(string(), ' \(')[1]"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="./tei:head[1]/string()"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </a>
                            <xsl:if test="./tei:div[@type eq 'subsection']">
                                <ul>
                                    <xsl:for-each select="./tei:div[@type eq 'subsection']">
                                        <li>
                                            <a href="{$root}descriptions/{$descr-path}#{./tei:head[1]/@xml:id}">
                                                <!-- tokenize the text to include only the first part of the heading and no greek text in parenthesis-->
                                <xsl:choose>
                                    <xsl:when test="./tei:head[1]/tei:term">
                                        <xsl:value-of select="./tei:head[1]/tokenize(string(), ' \(')[1]"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="./tei:head[1]/string()"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                                            </a>
                                        </li>
                                    </xsl:for-each>
                                </ul>
                            </xsl:if>
                        </li>
                    </xsl:for-each>
                </ol>
            </xsl:for-each>
        </div>
    </xsl:template>
    


</xsl:stylesheet>