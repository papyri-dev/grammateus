<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:exist="http://exist-db.org/xquery/kwic" xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs tei" version="2.0">

    <xsl:variable name="root">https://grammateus.unige.ch/</xsl:variable>
    
    <!-- links to papyri not in our corpus / in papyri.info -->
    <xsl:template match="tei:ref[@type = 'external']">
        <a class="out-link" href="{@target}" target="_blank">
            <xsl:apply-templates/>
        </a>
    </xsl:template>
    
    <!-- links to papyri available in our corpus -->
    <xsl:template match="tei:ref[@type = 'internal']">
        <a class="db-link" href="{$root}document/{substring-before(@target, '.xml')}" target="_blank">
            <xsl:apply-templates/>
        </a>
    </xsl:template>
    
</xsl:stylesheet>