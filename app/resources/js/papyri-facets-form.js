$('.facetForm').submit(function(e){
            
            // disable empty hidden fields (date)
            $('input[type=hidden]').each(function(i) {
                var $input = $(this);
                if ($input.val() === ''){
                    $input.attr('disabled', 'disabled');
                }
            });
            // disable empty keyword field
            $(':input', this).filter(function() {
                return this.value.length === 0;
            }).prop('disabled', true);

            e.preventDefault();
            var formData=$(this).serialize();
            var fullUrl = window.location.href;
            console.log(fullUrl);
            
            // if page, keyword, min-date or max-date in form data, remove from previous url
            if (fullUrl.includes('page')) {
                console.log('page');
                fullUrl = fullUrl.replace(/page=[\w\W\d\s]*?&/, '');
            }
            if (fullUrl.includes('keyword')) {
                console.log('keyword');
                fullUrl = fullUrl.replace(/keyword=[\w\W\d\s\*]*?&/, '');
            }
            if ((fullUrl.includes('max-date')) && ($('#max-date').val() !== '')){
                console.log('max-date');
                fullUrl = fullUrl.replace(/max-date=[-]?\d+&/, '');
            }
            if ((fullUrl.includes('min-date')) && ($('#min-date').val() !== '')){
                console.log('min-date');
                fullUrl = fullUrl.replace(/min-date=[-]?\d+&/, '');
            }
            
            if (fullUrl.endsWith('.html')){
                fullUrl = fullUrl+"?" // case when there is no parameters in the query string yet
            }
            var finalUrl = fullUrl+formData+"&";// final parameter is also followed by amp
            var encoded = finalUrl.replace(/%3A/g, ':') // simplify url by writing : instead of %3A

            window.location.href = encoded;
        })