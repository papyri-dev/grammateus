var range = document.getElementById('date-range');
        
        // use the search parameter to set the slider's handle positions
        var params = new URLSearchParams(window.location.search)
        if (params.has('min-date')) {
            var rangeMin = parseInt(params.get('min-date'), 10);
        }else {
            var rangeMin = -300;
        }
        if (params.has('max-date')) {
            var rangeMax = parseInt(params.get('max-date'), 10);
        }else {
            var rangeMax = 700;
        }

noUiSlider.create(range, {

    range: {
        'min': -300,
        'max': 700
    },

    step: 100,

    // Handles start at ...
    start: [rangeMin, rangeMax],

    // ... must be at least 100 apart
    margin: 100,

    // Display colored bars between handles
    connect: true,

    // Move handle on tap, bars are draggable
    behaviour: 'tap',
    format: wNumb({
        decimals: 0
    }),

    // Show a scale with the slider
    pips: {
        mode: 'steps',
        stepped: true,
        density: 4
    }
});
    
    var minDate = document.getElementById('min-date');
    var maxDate = document.getElementById('max-date');
    
    //update parameters
    range.noUiSlider.on('change', function (values, handle) {
        // set new values 
        var value = values[handle];
        if (handle) { //second handle
            maxDate.value = value;
        } else { //first handle
            minDate.value = value;
        }
    
        // remove previous parameter value from url
    });