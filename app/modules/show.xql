xquery version "3.1";

module namespace show="http://grammateus.ch/show";

import module namespace gramm="http://grammateus.ch/gramm" at "gramm.xql";
import module namespace g="http://grammateus.ch/global" at "global.xql";
import module namespace functx="http://www.functx.com";
import module namespace console="http://exist-db.org/xquery/console";

declare namespace tei = "http://www.tei-c.org/ns/1.0";


(:: DATE AND PLACE ::)

(:~ Get the century from a date.
 : 
 : We look only at century 1 to 8, since we do not have papyri from later than the eighth century.
 : There is probably a better way to do that, automatically.
 :
 : @param $date - a date object.
 : @return string - century in Roman numerals.
 :
:)
declare function show:get-century-from-date($date) {
    let $year := year-from-date(xs:date($date))
    let $century :=
        if((0001 <= $year and $year <= 0100) or (-0100 <= $year and $year <= -0000)) then
            'I'
        else if((0101 <= $year and $year <= 0200) or (-0200 <= $year and $year <= -0101)) then
            'II'
        else if((0201 <= $year and $year <= 0300) or (-0300 <= $year and $year <= -0201)) then
            'III'
        else if((0301 <= $year and $year <= 0400) or (-0400 <= $year and $year <= -0301)) then
            'IV'
        else if((0401 <= $year and $year <= 0500) or (-0500 <= $year and $year <= -0401)) then
            'V'
        else if((0501 <= $year and $year <= 0600) or (-0600 <= $year and $year <= -0501)) then
            'VI'
        else if((0601 <= $year and $year <= 0700) or (-0700 <= $year and $year <= -0601)) then
            'VII'
        else if((0701 <= $year and $year <= 0800) or (-0800 <= $year and $year <= -0701)) then
            'VIII'
        else('error!')
    return $century
};

(:~ Get the era from a date.
 :
 : @param $date - a date object.
 : @return string - era (BCE/CE).
 :
:)
declare function show:get-era-from-date($date) {
    let $era := if ($date < xs:date("0001-01-01")) then 'BCE' else 'CE'
    return $era
};

(:~
 : Format a date from yyyy-mm-dd to century and era.
 :
 : @param $date - a date object.
 : @return string - century (roman number) followed by era (BCE/CE).
 :
:)
declare function show:format-date($date) {
    let $century := show:get-century-from-date($date)
    let $era := show:get-era-from-date($date)
    return
        concat($century, ' ', $era)
};

(:~
 : Assess whether a date is precise or uncertain/approximate.
 : 
 : See: http://www.stoa.org/epidoc/gl/latest/supp-historigdate.html
 : We could also add conditions for only @notBefore or only @notAfter
 :
 : @param $history - the HGV <history> fragment.
 : @return boolean - century (roman number) followed by era (BCE/CE).
 :
:)
declare function show:is-date-precise($history) {
    let $precision :=
        if ($history//tei:origDate[2]) then "false"
        else if ($history//tei:origDate[@cert eq "low"]) then "false"
        else if ($history//tei:origDate[@precision eq "low"]) then "false"
(:        else if ($history//tei:origDate[@precision eq "medium"]) then "false":)(:ca. date. Not sure it counts as imprecise/uncertain:)
        else if ($history//tei:origDate/tei:precision or $history//tei:origDate/tei:certainty) then "false"
        else "true"
    return
        $precision
};

(:~
 : Format a papyrus date from external HGV data, for display on show page and facet search results.
 :
 : @param $doc - document node
 : @return formatted date as string.
 : 
:)
declare function show:date($doc as node()){

    (: select the first of possible origDate elements :)
    let $origDate := $doc//tei:origDate[1]
    
    (: date is formatted as century in Roman numeral + BCE/CE Era :)
    let $formatted_date :=
    
        (: @when attribute :)
        if ($origDate[@when]) then
            gramm:string-to-date($origDate/@when/string()) => show:format-date()
        
        (: @notBefore and @notAfter :)
        else if ($origDate[@notBefore] and $origDate[@notAfter]) then
            (: @notBefore :)
            let $nb_date := gramm:string-to-date($origDate/@notBefore/string()) => show:format-date()
            (: @notAfter :)
            let $na_date := gramm:string-to-date($origDate/@notAfter/string()) => show:format-date()
            (: if nA and nB are different, combine the centuries:)
            return if ($nb_date != $na_date)
                then concat($nb_date, " - ", $na_date)
                else $nb_date
        
        (: when only @notBefore or @notAfter, they are given a 50-year range :)
        else if ($origDate[@notBefore]) then
            let $tpq := gramm:string-to-date($origDate/@notBefore/string()) => show:format-date()
            let $taq := (gramm:string-to-date($origDate/@notBefore/string())+xs:yearMonthDuration("P50Y"))=>show:format-date()
            return
                if($tpq eq $taq) then $tpq else $tpq || "-" || $taq
        else if ($origDate[@notAfter]) then
            let $tpq := (gramm:string-to-date($origDate/@notAfter/string())-xs:yearMonthDuration("P50Y"))=>show:format-date()
            let $taq := gramm:string-to-date($origDate/@notAfter/string()) => show:format-date()
            return
                if($tpq eq $taq) then $taq else $tpq || "-" || $taq
        else ()
    
    return 
            $formatted_date
};

(:~
 : Get the place of origin from external HGV data.
 :
 : @param $doc - document node
 : @return origPlace text from <history> fragment.
 :
:)
declare function show:place($doc as node()){
    let $hgvFragment := gramm:doc-expand-history($doc)
    let $hgvPlace := $hgvFragment//tei:origPlace
    let $origPlace := replace($hgvPlace, 'unbekannt', 'unknown')
                    => functx:if-empty('unknown')
    return $origPlace
};


(:: TEXT ::)

(:~
 : Transforms a papyri.info XML transcription to HTML
 : with epidoc stylesheets.
 :
 : @param $xml - document node
 : @return HTML.
 :
:)
declare function show:epidoc($xml as node()){
    let $xsl := doc(concat($g:epidoc, "start-edition.xsl"))
    let $params :=
        <parameters>
            <param name="edn-structure" value="ddbdp"/>
            <param name="leiden-style" value="sammelbuch"/>

        </parameters>
    return
        if ($xml//tei:TEI) then transform:transform($xml, $xsl, $params) else $xml
};

(:~
 : Transforms the epidoc HTML text to new HTML, with added <span> for each line
 : and class for each section.
 :
 : @param $sections - node from the XML <ab type="section">
 : @param $html - epidoc html text
 : @return HTML.
 :
:)
declare function show:text-content($sections as node(), $html as node()){
    let $xml := <root>{$sections, $html}</root>
    let $xsl := doc(concat($g:xslt, "txt-sect.xslt"))
    return
        transform:transform($xml, $xsl, ())
};

declare function show:reprint-alert($tm) {
    <div class="alert alert-danger" id="alert-home" role="alert">
        <i class="fas fa-exclamation-circle"/>
        The document "{$tm}" has been reprinted.<br/>
        Please be aware that the text sections may need to be revised.
        </div>
};


(:: SVG CONTAINER ::)

(:~
 : Creates an svg element with the correct aspect ratio and text inside.
 : PROBLEM: some papyri have custom rules for choosing recto/verso, or for columns
 : cf. gramateus.js => we cannot be automatically precise for those nb_lines and nb_col
 :
 : @param $w - width in cm
 : @param $h - height in cm
 : @param $txt - string with text of the papyrus
 : @param $fibres - fibres direction (fv/fh/fm)
 : @param $nb_lines - list of integers, maximum number of lines in each textpart
 : @param $nb_col - integer, number of columns
 : @return SVG.
 :
:)
declare function show:svg($w, $h, $txt, $fibres, $nb_lines, $nb_col) {
    let $factor := number("20")
    let $factor_font :=  number("38")
    let $factor_line_height := number("15")(:15:)
    let $factor_space_dim := number("30")
    let $max_str_length := 
        (:columns:)
        let $columns_total := 
            (: int/ext script in double docs. ATTENTION: what if int/ext is actually in columns on papyri?? :)
            if($txt//span[@id eq 'abr,int' or @id eq 'abintdupl' or @id eq 'abint'])
            (:count all lines as one column:)
            then $txt//div[@id eq "edition"]
            (:margin text must be counted as column:)
            (::otherwise:)
            else $txt//div[@class eq 'textpart']
        for $column in $columns_total
        return
            (:count only the lines with a class section, which we are displaying. 
            it will ignore verso lines for instance:)
            max($column//span[matches(@class, "section")]/string-length(.))
(:max($txt//div[@class eq 'textpart']//span[contains(@class, "section")]/string-length(.))*number($nb_col):)
    let $ratio_width := $w div sum($max_str_length)
    (:ratio_height not working when textparts in one column, e.g. TM 1837:)
    let $ratio_height := $h div max($nb_lines)
    let $font_size := $ratio_width*$factor_font
    (:adjust font size for case like TM7787, many short lines :)
    let $font_size_adjusted :=
        if ($font_size*max($nb_lines) gt $h*$factor)then (($h*$factor div max($nb_lines))*0.75)
        else $font_size
    let $line_height := 
        (:normal case - too big for TM1837:)
        if ($font_size_adjusted eq $font_size) then ($ratio_height div $font_size)*$factor_line_height
        (:adjusted font:)
        else ($ratio_height div $font_size_adjusted)*($factor_line_height+3)
    let $debug_msg :=
        <msg>
            <max_str_length>{$max_str_length}</max_str_length>
            <lines>{$nb_lines}</lines>
            <ratio_width>{$ratio_width}</ratio_width>
            <ratio_height>{$ratio_height}</ratio_height>
            <font_size>{$font_size}</font_size>
            <font_size_adjusted>{$font_size_adjusted}</font_size_adjusted>
            <line_height>{$line_height}</line_height>
        </msg>
    return
    <svg id="svg" width="{($w*$factor)+$factor_space_dim}" height="{($h*$factor)+$factor_space_dim}" viewBox="0 0 {($w*$factor)+$factor_space_dim} {($h*$factor)+$factor_space_dim}" style="display:none;">
        <defs>
            <!-- vertical fibres pattern -->
            <pattern id="fv" patternUnits="userSpaceOnUse" width="10" height="10" patternTransform="rotate(0)">
			    <line x1="0" y1="0" x2="0" y2="10" stroke="rgba(225, 207, 183, 0.3)" stroke-width="10" />
		    </pattern>
		    <!-- horizontal fibres pattern -->
		    <pattern id="fh" patternUnits="userSpaceOnUse" width="10" height="10" patternTransform="rotate(90)">
                <line x1="0" y1="0" x2="0" y2="10" stroke="rgba(225, 207, 183, 0.3)" stroke-width="10" />
            </pattern>
            <!-- mixed fibres -->
            <!-- mask for vertical separation - left/right -->
            <mask id="mixed-v">
                <rect x="{$factor_space_dim}" y="{$factor_space_dim}" width="{($w*$factor) div 2}" height="{$h*$factor}" fill="url(#fh)"/>
                <rect x="{$factor_space_dim + (($w*$factor) div 2)}" y="{$factor_space_dim}" width="{($w*$factor) div 2}" height="{$h*$factor}" fill="url(#fv)"/>
            </mask>
            <!-- pattern for both half -->
            <pattern id="fm" patternUnits="userSpaceOnUse" width="{$w*$factor + $factor_space_dim}" height="{$h*$factor + $factor_space_dim}">
                <rect x="{$factor_space_dim}" y="{$factor_space_dim}" width="{$w*$factor}" height="{$h*$factor}" fill="rgba(225, 207, 183)" mask="url(#mixed-v)"/>
            </pattern>
        </defs>
        <g>
            <!-- dimensions labels -->
            <text x="{(($w*$factor) div 2)+$factor_space_dim}" y="{$factor_space_dim div 2}" text-anchor="middle">--- {$w/text()} cm ---</text>
            <text x="{-((($h*$factor) div 2)+$factor_space_dim)}" y="{$factor_space_dim div 2}" text-anchor="middle" style="font-size:initial;display:flex;align-items:center;transform: rotate(-90deg);">--- {$h/text()} cm ---</text>
            <!-- background rectangle and fibres rectangle -->
            <rect x="{$factor_space_dim}" y="{$factor_space_dim}" width="{$w*$factor}" height="{$h*$factor}" fill="rgba(234, 222, 204, 0.3)"></rect>
            <rect x="{$factor_space_dim}" y="{$factor_space_dim}" width="{$w*$factor}" height="{$h*$factor}" fill="url(#{$fibres})"></rect>
            <!-- text -->
            <foreignObject x="{$factor_space_dim}" y="{$factor_space_dim}" width="{$w*$factor}" height="{$h*$factor}" style="line-height:{$line_height};" font-size="{$font_size_adjusted}">{($txt)}</foreignObject>
        </g>
    </svg>
};


(:: IMAGE ::)

(:~
 : Creates a iiif viewer only for papyri with manifest.
 : Examples with viewer TM:12911, 12913, 12914, 12915, 12916 (17 petitions, 3 libelli, 2 notif. of death)
 : Examples without TM: 755, 756, 757 (all business letters from Zenon archive)
 :
 : @param $doc - document node 
 : @return HTML <viewer>.
 :
:)
declare function show:iiif-viewer($doc as node()) {
    let $has_manifest := gramm:get-iiif-manifest($doc)
    return
        if (count($has_manifest) >= 1) then
            <div id='viewer'>
                <div id='manifest' style='display:none;'>
                    {for $m in $has_manifest
                    return
                        <a href="{$m/@target/string()}">
                            {
                                (: here assuming only one canvas but possible to loop over a list instead :)
                                for $c in $m/tei:ref
                                return
                                <span class="canvasID" data-url="{$c/@target/string()}"></span>
                            }
                        </a>    
                    }
                </div>
                <div id="mirador"></div>
            </div>
        else ()
};

(:~
 : Embeds an iframe only for papyri with links (Oxyrynchus papyri hosted on figshare).
 :
 : @param $doc - document node 
 : @return HTML <iframe>.
 :
:)
declare function show:iframe($doc) {
    let $src := gramm:get-iframe($doc)
    return
        if ($src) then
            <iframe src="{$src/@target/string()}" allowfullscreen="1" frameborder="0"
                style="width:inherit;height:500px;"></iframe>
        else ()
};



(: ---------- TEMPLATE FUNCTION ---------- :)

(: create show page HTML structure :)
declare function show:page($resource as xs:string?){
    (: get document ID :)
    let $document := $resource

    (: get xml file as doc :)
    let $uri_1 := replace(concat($g:phase1,$document,".xml"), '/exist/', '/db/')
    let $uri_2 := replace(concat($g:byzantine,$document,".xml"), '/exist/', '/db/')
    let $doc := if (doc-available($uri_1)) then doc($uri_1) else doc($uri_2)

    (: TM number :)
    let $tm := $doc//tei:idno[@type eq 'TM']
    
    (: taxonomy/typology :)
    let $f_type := gramm:get-catDesc($doc//tei:catRef[@n eq 'format-type'])/normalize-space()
    let $f_subtype := gramm:get-catDesc($doc//tei:catRef[@n eq 'format-subtype'])/normalize-space()
    let $f_variation := gramm:get-catDesc($doc//tei:catRef[@n eq 'format-variation'])/normalize-space()
    let $f_type_link := gramm:get-descr-link($doc//tei:catRef[@n eq 'format-type'])
    let $f_subtype_link := gramm:get-descr-link($doc//tei:catRef[@n eq 'format-subtype'])
    let $f_variation_link := gramm:get-descr-link($doc//tei:catRef[@n eq 'format-variation'])
    let $type_class := gramm:get-class-link($doc//tei:catRef[@n eq 'format-type'])
    let $subtype_class := gramm:get-class-link($doc//tei:catRef[@n eq 'format-subtype'])
    let $variation_class := gramm:get-class-link($doc//tei:catRef[@n eq 'format-variation'])

    (: fibres :)
    let $fibres := functx:substring-after-last($doc//tei:support/@ana/string(), '#')

    (: shape :)
    let $w := $doc//tei:width
    let $h := $doc//tei:height
    let $ratio := $w div $h
    let $shape := gramm:shape($ratio)

    (: link to image :)
    let $img_link := gramm:get-img-link($doc)

    (:: text-container and text content ::)

    (: text content :)
    (: XML :)
    let $text_1 := gramm:doc-expand-text($doc) (: papyri.info xml :)
    (: check if reprinted in new document :)
    let $text_xml := 
        if ($text_1//tei:body//tei:ref[@type eq 'reprint-in'])
        then gramm:reprint-update($doc) => gramm:doc-expand-text() 
        else $text_1
    
    (: html :)
    let $text_html := show:epidoc($text_xml) (: epidoc html :)
    let $sections := $doc//tei:ab[@type eq 'section']
    let $text_sections := show:text-content($sections, $text_html) (: text sections bg color :)

    (: text container :)
    (: column number problem: texpart is not always a column, e.g. double documents
        But column nb is also not always exact, e.g. margin text has to be counted as a column for the display :)
    let $columns_real := number($doc//tei:layout/@columns/string())
    let $columns := count($text_sections//div[@id eq 'edition']/div[@class eq "textpart"][not(preceding-sibling::span[matches(@id, 'v')])])
    (: +1 => 1st line is <a> instead of <br>
        we do not count lines on the verso 
        original=> count($text_sections//div[@id eq 'edition']//br[not(parent::div[preceding-sibling::span[contains(@id, 'v')]])])+1 
        now trying to count only the text that is displayed and has a text section:)
    let $lines := count($text_sections//span[@class eq "section"])
    
    (: max nb of lines for each column, from html:)
    (:TODO: use column_real, and a new column variable to add in matrices, to know how many actual text column need to be dislpayed, that include possible margin text etc. problem = if some textpart are part of same column?:)
    let $max_lines_textpart :=
        for $textpart in $text_sections//div[@id eq 'edition']//div[@class eq 'textpart']
        let $l := count($textpart//br[not(parent::div[preceding-sibling::span[matches(@id, 'v')]])])+1
(:        let $l := count($textpart//span[contains(@class, "section")]):)
        order by $l descending
        return $l
    
    (:ADD MARGIN AS COLUMNS:)
    let $max_lines := 
        if($columns_real eq 1) 
        then sum($max_lines_textpart) 
(:        else if (count($max_lines_textpart) ne $columns_real):)
(:        then console:log("problem with column number and textpart"):)
        else $max_lines_textpart


    return
    <div>
        <div class="page-header">
            <h1 class="h1">
                {gramm:title($doc)}
            </h1>
            <hr/>
        </div>
        <!-- metadata -->
        <div class="row">
        <!-- Identification aspects -->
        <div class="col-auto">
            <p>
                <span class="show-metadata">TM: </span>
                {$tm}
            </p>
            <p>
                <span class="show-metadata">Type: </span>
                {$f_type}
                <!-- link to type classification -->
                {if($type_class) then (
                    <a class="badge badge-secondary badge-link" href="{$g:root}classification/{$type_class}" target="_blank">C</a>) else ()}
                <!-- link to type description -->
                {if($f_type_link) then (
                    <a class="badge badge-secondary badge-link" href="{$g:root}descriptions/{$f_type_link}" target="_blank">D</a>) else ()}
                <!-- link to type papyri -->
                <a class="badge badge-secondary badge-link" href="{$g:root}papyri.html?facet:f-type={$f_type}&amp;" target="_blank">P</a>
            </p>
            <p>
                <span class="show-metadata">Subtype: </span>
                {$f_subtype}
                <!-- link to type classification -->
                {if($subtype_class) then (
                    <a class="badge badge-secondary badge-link" href="{$g:root}classification/{$subtype_class}" target="_blank">C</a>) else ()}
                <!-- link to subtype description -->
                {if($f_subtype_link) then (
                    <a class="badge badge-secondary badge-link" href="{$g:root}descriptions/{$f_subtype_link}" target="_blank">D</a>) else ()}
                <!-- link to subtype papyri -->
                <a class="badge badge-secondary badge-link" href="{$g:root}papyri.html?facet:f-subtype={$f_subtype}&amp;" target="_blank">P</a>
            </p>
            <!-- if there is a variation, include it in metadata -->
            {if ($f_variation)
            then (
                <p>
                <span class="show-metadata">Variation: </span>
                {$f_variation}
                <!-- link to type classification -->
                {if($variation_class) then (
                    <a class="badge badge-secondary badge-link" href="{$g:root}classification/{$variation_class}" target="_blank">C</a>) else ()}
                <!-- link to variation description -->
                {if($f_variation_link) then (
                    <a class="badge badge-secondary badge-link" href="{$g:root}descriptions/{$f_variation_link}" target="_blank">D</a>) else ()}
                <!-- link to variation papyri -->
                <a class="badge badge-secondary badge-link" href="{$g:root}papyri.html?facet:f-variation={$f_variation}&amp;" target="_blank">P</a>
            </p>) 
            else ()}
            <p>
                <span class="show-metadata">Provenance: </span>
                {show:place($doc)}
            </p>
            <p>
                <span class="show-metadata">Century: </span>
                {show:date($doc)}
            </p>
        </div>
        <!-- Physical aspects -->
        <div class="col">
            <p>
                <span class="show-metadata">Width: </span>
                {$w} cm
            </p>
            <p>
                <span class="show-metadata">Height: </span>
                {$h} cm
            </p>
            <p>
                <span class="show-metadata">Shape: </span>
                {$shape, ' '}
                {if ($shape eq 'Vertical') then
                    <a href="{$g:root}papyri.html?facet:shape=Vertical&amp;" class="text-dark">{gramm:fa-shape-vertical()}</a>
                else if ($shape eq 'Horizontal') then
                    <a href="{$g:root}{$g:root}papyri.html?facet:shape=Horizontal&amp;" class="text-dark">{gramm:fa-shape-horizontal()}</a>
                else
                    <a href="{$g:root}papyri.html?facet:shape=Squarish&amp;" class="text-dark">{gramm:fa-shape-square()}</a>
                }
            </p>
            <p>
                <span class="show-metadata">Fibres: </span>
                {if ($fibres eq 'fv') then
                    <span>Vertical <a href="{$g:root}papyri.html?facet:fibres=Vertical&amp;" class="text-dark">{gramm:fibres-symbol('Vertical')}</a></span>
                else if ($fibres eq 'fh') then
                    <span>Horizontal <a href="{$g:root}papyri.html?facet:fibres=Horizontal&amp;" class="text-dark">{gramm:fibres-symbol('Horizontal')}</a></span>
                else
                    <span>Mixed <a href="{$g:root}papyri.html?facet:fibres=Mixed&amp;" class="text-dark">{gramm:fibres-symbol('Mixed')}</a></span>
                }
            </p>
            <p>
                <span class="show-metadata">Seal: </span>
                {$doc//tei:seal/tei:p/normalize-space()}
            </p>
        </div>
        <!-- external links -->
        <div class="col">
            <p class="btn-toolbar">
                <a id="ddb-hybrid" class="btn btn-info mx-1 my-1" role="button" target="_blank" href="{concat($g:papinfoddb, $doc//tei:idno[@type='ddb-hybrid'])}">
                    papyri.info
                </a>
                <a class="btn btn-info mx-1 my-1" role="button" target="_blank" href="{concat($g:hgv, $doc//tei:idno[@type='hgv'])}">
                    HGV
                </a>
                <a class="btn btn-info mx-1 my-1" role="button" target="_blank" href="{concat($g:tm, $tm)}">
                    TM
                </a>
                <!-- display button only if the link exists -->
                {if (not($img_link eq '')) then
                    <a class="btn btn-info mx-1 my-1" role="button" target="_blank" href="{$img_link}">
                        Image
                    </a>
                else()
                }
            </p>
            <p><!-- link for the online version, not local -->
                <span class="show-metadata">XML: </span>
                <a id="filename" href="{$document||'/source'}">
                    {$document}
                </a>
            </p>
        </div>
        </div>
        <hr/>
        <!-- notice for reprinted papyri: line nb may be out of date -->
        {if ($doc//tei:div[@type = 'edition' and @subtype = 'lb-review-needed']) then show:reprint-alert($tm) else ()}
        <!-- papyrus schema -->
        <div class="row">
            <div class="col">
                <ul class="legend">
                <!-- list of text sections -->
                {
                    let $auth := doc($g:authority)
                    for $section in distinct-values($sections/tei:locus/@ana/string())
                    let $id := functx:substring-after-last($section, '#')
                    let $name := $auth//id($id)/tei:label/string()
                    return
                        <li><span class="{$id}"></span> {$name}</li>
                }
                </ul>
            </div>
        </div>
        {if ($shape ne "Horizontal" or number($w) lt 15) then (
            <div class="row" id="papyrus-viz">
                <div class="col-6" id="text-viz">
                    <div id="text-container">
                        <div id="text-content">
                            {show:svg($w, $h, $text_sections/div, $fibres, $max_lines, $columns)}
                        </div>
                    </div>
                </div>
                <div class="col-6" id="image-viz">
                    {show:iiif-viewer($doc)}
                    {show:iframe($doc)}
                </div>
            </div>
        ) else (
            <div class="row" id="papyrus-viz">
                <div class="col">
                <div class="row" id="text-viz">
                    <!--<div id="text-container"> not necessary with SVG -->
                        <div id="text-content">
                            {show:svg($w, $h, $text_sections/div, $fibres, $max_lines, $columns)}
                        </div>
                    <!--</div>-->
                </div>
                <div class="row" id="image-viz">
                    <div class="col"><!-- mirador viewer must be in a div class col -->
                        {show:iiif-viewer($doc)}
                        {show:iframe($doc)}
                    </div>
                </div>
                </div>
            </div>
                )}
    </div>
};
