xquery version "3.1";



import module namespace g="http://grammateus.ch/global" at "global.xql";
import module namespace gramm="http://grammateus.ch/gramm" at "gramm.xql";

declare namespace tei = "http://www.tei-c.org/ns/1.0";

declare function local:xml2csv($results) {
    for $r in $results
    return
        gramm:tm($r) || "," ||
        gramm:title($r) || "," ||
        $g:papinfoddb || $r//tei:idno[@type eq 'ddb-hybrid'] || "&#10;" 
};


declare function local:download($node, $filename) {
    response:set-header("Content-Disposition", concat("attachment;
filename=", $filename))
    ,
    response:stream(
        $node,
        'indent=yes' (: serialization :)
        )
};

let $csv := local:xml2csv(collection($g:papyri))

let $filename := fn:current-dateTime() => fn:adjust-date-to-timezone(())

return local:download($csv, $filename || "-results.csv")

