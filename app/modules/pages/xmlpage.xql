xquery version "3.1";

module namespace x="http://grammateus.ch/pages/xmlpage";

import module namespace g="http://grammateus.ch/global" at "../global.xql";
import module namespace gramm="http://grammateus.ch/gramm" at "../gramm.xql";

declare function x:page($pagetype, $resource) {
    let $test_path := x:path($pagetype, $resource)
    let $doc_path := if(doc-available($test_path)) then $test_path else $resource
    return
    <div class="row">
        {
            gramm:sidebar-container($pagetype, $doc_path),
            gramm:main($doc_path),
            gramm:back-to-top()
        }
    </div>
};

declare function x:path($pagetype, $resource) {
    (: path to resource :)
    let $path := switch ($pagetype)
        case "introduction" return $g:intro
        case "classification" return $g:class
        case "descriptions" return $g:descr
        default return ()
    (: the main XML page for descr/class is in typology.xml, 
    else it has the name of the resource variable :)
    let $page := if ((substring-before($resource, ".html") eq $pagetype) or ($resource eq $pagetype))
        then "typology" 
        else $resource
    (: document node :)
    let $fullpath := $path||$page||".xml"
    
    return $fullpath
};