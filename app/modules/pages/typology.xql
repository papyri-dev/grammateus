xquery version "3.1";

module namespace t="http://grammateus.ch/pages/typology";

import module namespace g="http://grammateus.ch/global" at "../global.xql";

declare function t:page() {
    <div class="row">
        <div class="col-md-12 paragraph">
            <h1 class="h1">Typology</h1>
            <h3 class="h3">Overview</h3>
            <p>The typology presented here is an attempt to identify the main types of document among the wide variety of Greek documentary papyri.</p>
            <p>The typology is accompanied by:
                <ul class="nobullet">
                    <li>
                        <span class="badge badge-secondary">C</span> an explanation of the <a href="classification" target="_blank">classification</a> methodology.</li>
                    <li>
                        <span class="badge badge-secondary">D</span> a <a href="descriptions" target="_blank">description</a> of the different types of papyri.</li>
                    <li>
                        <span class="badge badge-secondary">P</span> A list of papyri that belong to each type.</li>
                </ul>
            </p>
            <p>You can use the <a href="compare">comparison tool</a> to see two categories side-by-side with information about their general dimensions and their specific formulas.</p>
            {t:show()}
        </div>
    </div>
};

(:~ show the <taxonomy> classification
 : 
 : the visualisation is currently a list, displayed with nested cards that can be toggled
 : 
 : @return html <main> content
 :)
declare function t:show() {
    transform:transform(doc($g:taxonomy), doc($g:taxonomyToList), ())
};