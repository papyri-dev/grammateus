xquery version "3.1";

module namespace biblio="http://grammateus.ch/pages/bibliography";

import module namespace g="http://grammateus.ch/global" at "../global.xql";
import module namespace functx="http://www.functx.com";

declare namespace tei="http://www.tei-c.org/ns/1.0";

declare function biblio:page() {
    <div class="paragraph">
        <h1 class="h1">Bibliography</h1>
        <p>Our bibliography is available on Zotero: <a href="https://www.zotero.org/groups/2288077/grammateus">https://www.zotero.org/groups/2288077/grammateus</a></p>
        <hr/>
        <div class="col" id="biblio-list">
            {let $references := (collection($g:intro)//tei:bibl/tei:ptr/@target/string(),
                        collection($g:descr)//tei:bibl/tei:ptr/@target/string(),
                        collection($g:class)//tei:bibl/tei:ptr/@target/string()
                )
            let $ordered_references :=
            (: find distinct bibliographic references :)
            for $key in distinct-values($references)
        
            (: get the key :)
            let $k := $key => substring-after("gramm:")
    
            (: get the reference document from key :)
            let $doc := doc($g:bibl||$k||".xml")
    
            (: count authors/editors for a proper display :)
            let $author := count($doc//tei:text//tei:author)
            let $editor := count($doc//tei:text//tei:editor)
    
            (: date for sorting :)
            let $date := $doc//tei:imprint/tei:date
    
            (: sorting variable :)
            (: order by first author, if not by first editor, or by key :)
            let $sort := 
                if($doc//tei:text//tei:author) then ($doc//tei:text//tei:author[1]/tei:surname)
                else if($doc//tei:text//tei:editor) then ($doc//tei:text//tei:editor[1]/tei:surname)
                else $key
    
            (: first author/editor, and if several items by the same, then use the date - not working with TM archives... :)
(:            order by $sort, $key, $date:)
            order by $key
            return 
                <x>
                    <i>{$k}</i>
                    <i>{$doc}</i>
                    <i>{$author}</i>
                    <i>{$editor}</i>
                    <i>{$date}</i>
                </x>
        for $x at $pos in $ordered_references
            let $k := $x/i[1]
            let $doc := $x/i[2]
            let $author := number($x/i[3])
            let $editor := number($x/i[4])
            let $date := $x/i[5]
        return
        <div class="row biblio-ref">
            <a id="{$k}" class="biblio-anchor"></a>
            <div class="col-1 biblio-bullet">
                <!--<img src="{$g:img}roll-horizontal-2.png"/>-->
                <!--<span class="paranumber">{$pos}</span>-->
                <!-- use col-1
                <img src="{$g:img}bullet-indianred.png"/>
                <img src="{$g:img}roll.png"/>-->
                <!--<i class="fa fa-bookmark"></i>-->
                
                
                {(: reference with link :)
                if ($doc//tei:body//tei:ref/@target/string()) then
                    <a href="{$doc//tei:body//tei:ref/@target/string()}">
                        <i class="fas fa-external-link-alt biblio-title-link"></i>
                    </a>
                else <i class="far fa-square"/>
                }
            </div>
            <div class="col">
            
           {
            
            biblio:set-author($doc, $author, $editor)
            }
            
            
            <!-- date -->
            {if ($date/string()) then <span>. {$date/string()}{substring-after($k, $date/string())}.</span> else ()}
            
            <!-- Publication short title (nickname), e.g. P.Yale I (oates1967) -->
            {if ($doc//tei:title[@level = 'm' and @type eq 'short']) 
            then <span class="biblio-title">
                    {if (contains($k, 'tmarchive') or matches($k, '^tmper\d+')) then () else " = "}
                    {
                    (:remove last dot after short title (e.g. P.Dion.), which is a very rare ocurrence:)
                    $doc//tei:title[@level = 'm' and @type eq 'short']/string() => replace("\.$", "")
                        
                    }.
                </span> else ()}
            
            <!-- title -->
            {   (: journal article :)
                if ($doc//tei:title[@level eq "j"])
                then biblio:journal-title($doc)
                (: book chapter :)
                else if ($doc//tei:analytic/tei:title[@level eq "a"] and $doc//tei:monogr/tei:title[@level eq "m"])
                then biblio:bookchapter-title($doc)
                (: monograph :)
                else biblio:book-title($doc)
            }
            
            <!-- links to online versions -->
            {if ($doc//tei:idno[@subtype eq "OA"]) then <span> Available Open Access: <a class="biblio-link" href="{$doc//tei:idno[@subtype eq "OA"]}">{$doc//tei:idno[@subtype eq "OA"]}</a></span>
            else if ($doc//tei:idno[@subtype eq "doc"]) then <span> Available Online: <a class="biblio-link" href="{$doc//tei:idno[@subtype eq "doc"]}">{$doc//tei:idno[@subtype eq "doc"]}</a></span>
            else ()}
            </div>
        </div> 
            }
        </div>
    </div>
};

declare function biblio:set-author($doc, $author, $editor) {
    (:author:)
    if ($author = 1) then biblio:one-author($doc//tei:text//tei:author)
    else if ($author = 2) then biblio:two-author($doc//tei:text//tei:author)
    else if ($author gt 2) then biblio:multi-author($doc//tei:text//tei:author)
    
    (: no author/editor only :)
    else if ($editor = 1) then biblio:one-author($doc//tei:text//tei:editor)
    else if ($editor = 2) then biblio:two-author($doc//tei:text//tei:editor)
    else if ($editor gt 2) then biblio:multi-author($doc//tei:text//tei:editor)
    
    (:no author / editor (TM Archive):)
    else ()
};

declare function biblio:one-author($author) {
    <span class="biblio-title">{$author[1]/tei:surname}, {substring($author[1]/tei:forename, 1, 1)}</span>
};

declare function biblio:two-author($author) {
    <span class="biblio-title">{$author[1]/tei:surname}, {substring($author[1]/tei:forename, 1, 1)}. &amp; {$author[2]/tei:surname}, {substring($author[2]/tei:forename, 1, 1)}</span>
};

declare function biblio:multi-author($author) {
    <span class="biblio-title">{$author[1]/tei:surname}, {substring($author[1]/tei:forename, 1, 1)}.&#32;<i>et al</i></span>
};

declare function biblio:journal-title($doc) {
    <span>&#32;
           <!-- article title-->
            <span>"{$doc//tei:biblStruct//tei:title[@level eq "a"]/string()}"</span>
            <!-- journal title, short if possible -->
            {
                if ($doc//tei:biblStruct//tei:title[@level eq "j" and @type eq "short"]) then 
                    <span>,<i>&#32;{$doc//tei:biblStruct//tei:title[@level eq "j" and @type eq "short"]/string()}</i></span>
                else if ($doc//tei:biblStruct//tei:title[@level eq "j"]) 
                then <span>,&#32;<i>{$doc//tei:biblStruct//tei:title[@level eq "j"]/string()}</i></span>
                else ()
            }
            <!-- volume -->
            {if ($doc//tei:biblScope[@unit eq "vol"]) 
            then <span>&#32;({$doc//tei:biblScope[@unit eq "vol"]})</span> 
            else ()}
            <!-- pages -->
            {if ($doc//tei:biblScope[@unit eq "pp"]) 
            then <span>,&#32;{$doc//tei:biblScope[@unit eq "pp"]}</span> 
            else ()}
            <span>.</span>
    </span>
};

declare function biblio:book-title($doc) {
    <span>&#32;<i>
        {$doc//tei:biblStruct//tei:title[@level eq "m"][@type eq "main"]/string()}
    </i>.</span>
};

declare function biblio:bookchapter-title($doc) {
    <span>&#32;
        "{$doc//tei:biblStruct//tei:title[@level eq "a"]/string()}".&#32;In&#32;
        <i>{$doc//tei:biblStruct//tei:title[@level eq "m" and not(@type eq 'short')]/string()}</i>,&#32;
        edited by {biblio:book-editor($doc//tei:biblStruct//tei:monogr)},&#32;
        {$doc//tei:biblScope[@unit eq "pp"]}.
    </span>
};

declare function biblio:book-editor($monogr) {
    (: 1 editor :)
    if (count($monogr//tei:editor) = 1) 
    then substring($monogr//tei:editor/tei:forename, 1, 1) || ". " || $monogr//tei:editor/tei:surname
    (: 2 editors :)
    else if (count($monogr//tei:editor) = 2)
    then substring($monogr//tei:editor[1]/tei:forename,1,1) || ". " || $monogr//tei:editor[1]/tei:surname || " and " ||
    substring($monogr//tei:editor[2]/tei:forename,1,1) || ". " || $monogr//tei:editor[2]/tei:surname
    (: 3 and more editors :)
    else
        <span>{substring($monogr//tei:editor[1]/tei:forename,1,1) || ". " || $monogr//tei:editor[1]/tei:surname}<i> et al.</i></span>
};

