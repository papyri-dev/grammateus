xquery version "3.1";

module namespace search-facets="http://grammateus.ch/search-facets";

import module namespace gramm="http://grammateus.ch/gramm" at "gramm.xql";
import module namespace show="http://grammateus.ch/show" at "show.xql";
import module namespace g="http://grammateus.ch/global" at "global.xql";

import module namespace console="http://exist-db.org/xquery/console";

declare namespace tei = "http://www.tei-c.org/ns/1.0";

(: Remark: the keyword and date filters are not facets. :)
declare variable $search-facets:list :=
    <list>
        <item id="f-type">Type</item>
        <item id="f-subtype">Subtype</item>
        <item id="f-variation">Variation</item>
        <item id="section">Text Sections</item>
        <item id="shape">Shape of Papyrus</item>
        <item id="fibres">Direction of Fibres</item>
        <item id="origPlace">Provenance</item>
    </list>;
    
declare variable $search-facets:group := 
    <group id="section" name="Text Sections">
        {for $s at $pos in doc($g:authority)//tei:list[@type eq 'layout']/tei:item
        return
            <item id="s{$pos}">{$s/tei:label/string()}</item>
        }
    </group>;
 
 
(: sorting a map - from vangogh letters. 
 : https://gitlab.existsolutions.com/tei-publisher/vangogh/blob/master/modules/facets.xql#L24-35 :)   
declare function local:sort($facets as map(*)?) {
    array {
        if (exists($facets)) then
            for $key in map:keys($facets)
            let $value := map:get($facets, $key)
            order by $key ascending
            return
                map { $key: $value }
        else
            ()
    }
};


(:~ returns an export button to save results as csv.
 : 
 : @return <a> link to export page
 :)
declare function search-facets:export() {
    let $params := request:get-query-string()
    return
        <div class="order-by">
            <button class="btn export">
                <a class="sort-papyri export-link" 
                    href="{$g:root}export-search-csv.xq?{$params}" 
                    type="button" 
                    value="download">
                    Export
                </a>
            </button>
        </div>
};

(:~ calculates the range of 5 page numbers to be displayed for pagination.
 : 
 : @param page - current page nb
 : @param pagelength - total nb of pages
 :)
declare function local:page-range($page as xs:integer, $page_total as xs:integer) {
    
    let $range :=
        if ($page_total <= 5) then (1 to $page_total)
        else if ($page < 4 and $page + 2 <= $page_total ) then (1 to 5)
        else if (($page + 2) > $page_total) then ($page_total - 4 to $page_total)
        else ($page - 2 to $page + 2)
    
    return $range
};

(:~ returns pagination links.
 : 
 : @param results - query results
 : @param pagelength - number of results per page
 :)
declare function local:pagination($results, $pagelength as xs:integer) {
    
    let $page_current := number(request:get-parameter("page", "1"))
    
    let $page_total := if (count($results) mod $pagelength eq 0) then count($results) idiv $pagelength else (count($results) idiv $pagelength)+1
    
    let $query := request:get-query-string() => replace("page=.+?&amp;", "")
    
    let $log1 := console:log($pagelength)
    let $log2 := console:log(count($results) idiv $pagelength)
    let $log3 := console:log($page_total)
    
    let $pagination := if (count($results) <= $pagelength) then () 
    else
    <div class="papyri-pagination-container">
        <nav aria-label="Page navigation">
            <ul class="pagination papyri-pagination">
                <li class="page-item papyri-page-item {if($page_current = 1) then "disabled" else ()}">
                    <a class="page-link papyri-page-link" href="?{$query}page={$page_current - 1}&amp;" aria-label="Previous">
                        <span aria-hidden="true">{gramm:fa-previous()}</span>
                        <span class="sr-only">Previous</span>
                    </a>
                </li>
                
                { for $i in local:page-range($page_current, $page_total)
                    return
                        <li  class="page-item papyri-page-item {if ($i = $page_current) then "active" else ()}">
                            <a class="page-link papyri-page-link" href="?{$query}page={$i}&amp;">{$i}</a>
                        </li>
                }
                   
                <li class="page-item papyri-page-item {if($page_current = $page_total) then "disabled" else ()}">
                    <a class="page-link papyri-page-link" href="?{$query}page={$page_current + 1}&amp;" aria-label="Next">
                        <span aria-hidden="true">{gramm:fa-next()}</span>
                        <span class="sr-only">Next</span>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
    
    return $pagination
};

(:~ returns message with number of results for the query.
 : 
 : @param results - query results
 : @param page - current result page
 : @param pagelength - number of results per page
 :)
declare function local:results-nb($results, $pagelength as xs:integer, $page as xs:integer) {
    let $nb := count($results)
    let $message := 
        if ($nb <= $pagelength) 
        then let $pap := if ($nb > 1) then " Papyri" else " Papyrus"
        return $nb || $pap
        else 
            let $start := ($pagelength*($page -1))+1
            let $end := $start -1 + count(subsequence($results, ($pagelength*($page -1))+1, $pagelength))
            return $start || "-" || $end || " of " || $nb || " Papyri"
    
    return $message
};

(:~ returns formated search result for the papyri page.
 : 
 : @param r - an XML EpiDoc document from the database
 :)
declare function search-facets:format-results($r) {
    <div class="result row border-bottom">
        <div class="col result-tm"><!-- col-md-2 -->
            <span class="badge badge-secondary">TM {gramm:tm($r)}</span>
        </div>
        <div class="col result-info"><!-- col-md-10 -->
            <!-- Link -->
            {(gramm:get-doc-link($r), 
            (: logo when image available :)
            if (gramm:has-iiif($r)) then gramm:iiif-logo() 
            else if (gramm:has-iframe($r)) then gramm:figshare-logo() 
            else ())}
            
            <!-- Typology -->
            <div>Type: {gramm:get-catDesc($r//tei:catRef[@n eq 'format-type'])}</div>
            <div>Subtype: {gramm:get-catDesc($r//tei:catRef[@n eq 'format-subtype'])}</div>
            {
                let $variation := gramm:get-catDesc($r//tei:catRef[@n eq 'format-variation'])
                return
                    if ($variation) 
                    then <div>Variation: {$variation}</div> 
                    else()
            }
            
            <!-- Date/Place -->
            <div>
                <!--<span>{gramm:fa-calendar()}&#160;</span>-->
                <span>{show:date($r)},&#160;</span>
                <!--<span>{gramm:fa-map()}&#160;</span>-->
                <span>{gramm:origPlace($r)}</span>
            </div>
            
            <!-- Shape/Fibres -->
            <span>Shape&#160;&#160;</span><span>{
                ($r//tei:width div $r//tei:height) => gramm:shape() => gramm:shape-symbol()
            }&#160;&#160;</span>

            <span>Fibres {
                gramm:fibres($r) => gramm:fibres-symbol()
            }</span>
            
        </div>
    </div>
};

(:~ returns the html (bootstrap card) for each dimension in the variable $search-facet:list
 : 
 : @param results - a sequence of XML documents
 :)
declare function search-facets:get-facets($results){

(: for each facet in the list :)
for $dimension in $search-facets:list/item
return
    (: if it is the sepcial text section facet:)
    if ($dimension eq "Text Sections")
    then 
        (: return all facets grouped in a single card :)
        <div class="card">
            <div class="card-header facet-header" id="heading-{$dimension/@id}">
                <button class="btn btn-link mr-auto" type="button" data-toggle="collapse" data-target="#{$dimension/@id}" aria-expanded="true" aria-controls="{$dimension/@id}">
                    <span class="mr-auto">{$dimension/string()}</span>
                        {gramm:fa-down()}
                </button>
            </div>
            <div id="{$dimension/@id}" class="collapse" aria-labelledby="heading-{$dimension/@id}">
                <div class="card-body">
                {
                    for $dimension2 in $search-facets:group/item
                    let $facets := ft:facets($results, string($dimension2/@id), ())
                    return
                    array:for-each(local:sort($facets), function($entry) {
                    map:for-each($entry, function($label, $count) {
                        <div class="facet-content">
                            <input class="form-check-input" type="checkbox" name="facet:{$dimension2/@id}" value="{$label}"/>
                            <label>{$label} <span style="font-style:italic;">({$count})</span></label>
                        </div>
                        })
                    })
                }
                </div>
            </div>
    </div>
    (: or return normal card facet :)
    else
        let $facets := ft:facets($results, string($dimension/@id), ())
        return
        <div class="card">
            <div class="card-header facet-header" id="heading-{$dimension/@id}">
                <button class="btn btn-link mr-auto" type="button" data-toggle="collapse" data-target="#{$dimension/@id}" aria-expanded="true" aria-controls="{$dimension/@id}">
                    <span class="mr-auto">{$dimension/string()}</span>
                        {gramm:fa-down()}
                </button>
            </div>
            <div id="{$dimension/@id}" class="collapse" aria-labelledby="heading-{$dimension/@id}">
                <div class="card-body">
                {array:for-each(local:sort($facets), function($entry) {
                    map:for-each($entry, function($label, $count) {
                        <div class="facet-content">
                            <input class="form-check-input" type="checkbox" name="facet:{$dimension/@id}" value="{$label}"/>
                            <label>{$label} <span style="font-style:italic;">({$count})</span></label>
                        </div>
                        })
                    })
                }
                </div>
            </div>
    </div>
};


(: ---------- TEMPLATE FUNCTION (search entry point) ---------- :)


declare function search-facets:advanced-search ($keyword as xs:string, $pagelength as xs:integer){
    
    (: current page :)
    let $page := number(request:get-parameter("page", "1"))
    
    (: get list of all existing parameters:)
    let $params := request:get-parameter-names()
    
    (: create the facet options from existing parameters :)
    let $options := map{
        "facets" : map:merge(
            (:add only facet parameters to the map:)
            for $p in $params
            where matches($p, "facet:")
            return map { substring-after($p, "facet:"): request:get-parameter($p, "")}
        ),
        "leading-wildcard": "yes"
    }
    
    let $results_kw := 
        (: filter docs by keyword :)
        if ($keyword != '') then collection($g:papyri)//tei:TEI[ft:query(., $keyword, $options)]
        (: or else search all documents :)
        else collection($g:papyri)//tei:TEI[ft:query(., (), $options)]
    
    (: filter papyri collection for the date :)
    let $results_date :=
        if (["min-date", "max-date"] = $params) 
        then 
            let $min := if ("min-date" = $params) then request:get-parameter("min-date", ()) else "-300"
            let $max := if ("max-date" = $params) then request:get-parameter("max-date", ()) else "700"
            return gramm:date-filter($min, $max, $results_kw)
        else $results_kw
    
    (: filter results for the keyword :)
(:    let $results_kw := :)
(:        (: filter docs by keyword :):)
(:        if ($keyword != '') then $results_date[ft:query(., $keyword, $options)]:)
(:        (: or else search all documents :):)
(:        else $results_date[ft:query(., (), $options)]:)
        
    (: sort result :)
    let $order-by := request:get-parameter("sort-by", "")
    let $sort := 
            if ($order-by eq 'type') then 'for $r in $results_date
            order by gramm:get-catDesc($r//tei:catRef[@n eq "format-type"]),
            gramm:get-catDesc($r//tei:catRef[@n eq "format-subtype"]),
            gramm:get-catDesc($r//tei:catRef[@n eq "format-variation"])
            return $r'
            else if ($order-by eq 'place') then 'for $r in $results_date 
            order by gramm:origPlace($r//tei:history) return $r'
            else if ($order-by eq 'TM') then 'for $r in $results_date order by gramm:tm($r) => number() return $r'
            else 'for $r in $results_date
            let $vol := if ($r//tei:biblScope[@type eq "volume"]) then number($r//tei:biblScope[@type eq "volume"]) else number("0")
            order by $r//tei:title[@level eq "s"], $vol, number($r//tei:biblScope[@type eq "numbers"]) ascending, $r//tei:biblScope[@type eq "generic"] 
            return $r'

    let $results := util:eval($sort)
    
    (: get labels/count for each facet dimension :)
    let $facets := search-facets:get-facets($results)

    return 
    <div class="col search">
        <div class="row">
            <div class="col">
                <ul class="legend">
                    {   (: button to remove all paramters from query string :)
                        (: the pagetype parameter is always present, we do not want to count it :)
                        if (count($params) > 1) 
                        then 
                            <li>
                                <a class="badge badge-danger remove-filters" href="papyri.html">
                                    Remove all filters {gramm:fa-times()}
                                </a>
                            </li>
                        else ()}
                    {   (: parameters to remove from current query :)
                        let $current := request:get-query-string() => replace("%20", " ")
                        for $p in $params
                        (: ignore the pagetype parameter from controller :)
                        where $p ne 'pagetype'
                            (: parameter and value as a string :)
                            let $value := request:get-parameter($p, ())
                            (: there can be several values for the same parameter name :)
                            for $v in $value
                                (: last & is to mark the next param, so that we do not replace a similar one
                                    => esp. for origPlace
                                    => for that we also add a final &, see papyri-facet-form.js l.41:)
                                let $string := $p || "=" || $v || "&amp;"
                                let $regex_1 := replace($string, "\(", "\\(")
                                let $regex_2 := replace($regex_1, "\)", "\\)")
                                let $regex_3 := replace($regex_2, '\*', '\\*')
                                let $regex := replace($regex_3, "\?", "%3F")
                                (: new query without the parameter :)
                                let $new_query_1 := replace($current, $regex, "")
                                (: we remove the page parameter from the new query :)
                                let $new_query := replace($new_query_1, "page=.+?&amp;", "")
                                (: parameter name for display :)
                                let $param_name := 
                                    if (matches($p, 'facet:'))
                                    (: facet parameters, both from list and group of text sections :)
                                    then $search-facets:list/item[@id eq substring-after($p, "facet:")]/string() || $search-facets:group/item[@id eq substring-after($p, "facet:")]/parent::group/@id
                                    (: "keyword" parameter :)
                                    else $p
                                return
                                    (: ignore the page parameter :)
                                    if ($p = "page") then () else
                                    <li>
                                        <a class="badge badge-secondary" href="papyri.html?{$new_query}">
                                            {$param_name || ": " || $v || " "}{gramm:fa-times()}
                                        </a>
                                    </li>
                    }
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 papyri-col">
                <h3 class="h3">Search Filters</h3>
                <form action="{$g:root}papyri.html?" method="get" class="facetForm">
                    <!-- keyword search -->
                    <div class="card">
                        <div class="card-header facet-header" id="heading-keyword">
                            <button class="btn btn-link mr-auto" type="button" data-toggle="collapse" data-target="#keyword" aria-expanded="true" aria-controls="keyword">
                                <span class="mr-auto">Keyword</span>
                                {gramm:fa-up()}
                            </button>
                        </div>
                        <div id="keyword" class="collapse show" aria-labelledby="heading-keyword">
                            <div class="card-body" style="min-height:100px;">
                                <input id="doc-kw" name="keyword" type="text" class="form-control"
                               placeholder="e.g. 'Camel', or a TM number" value="{$keyword}" aria-label="Search"/>
                               <p>{gramm:fa-info()} Search for identifiers, subjects, and places, or get <a type="collapseThree" class="collapsed go-to-help" data-toggle="collapse" href="#collapseThree" >help</a>.</p>
                            </div>
                        </div>
                    </div>
                    <!-- facets from index -->
                    {$facets}
                    <!-- date range slider -->
                    <div class="card">
                        <div class="card-header facet-header" id="heading-century">
                            <button class="btn btn-link mr-auto" type="button" data-toggle="collapse" data-target="#century" aria-expanded="true" aria-controls="century">
                                <span class="mr-auto">Century</span>
                                {gramm:fa-down()}
                            </button>
                        </div>
                        <div id="century" class="collapse" aria-labelledby="heading-century">
                            <div class="card-body" style="min-height:100px;">
                                <div id="date-range" class="noUiSlider"></div>
                                <input type="hidden" id="min-date" name="min-date"/>
                                <input type="hidden" id="max-date" name="max-date"/>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-secondary" type="submit">Search</button>
                    <p><a type="collapseTwo" class="collapsed go-to-help search-info-btn" data-toggle="collapse" href="#collapseTwo" >Help</a></p>
                </form>
            </div>
            <div class="col-md-8 papyri-col">
                <h3 class="h3 row">
                    <div class="col">
                        {
                            local:results-nb($results, $pagelength, $page)
                        } 
                    </div>
                    <div class="d-flex justify-content-end flex-wrap">
                        <div class="dropdown show order-by">
                            <a class="dropdown-toggle sort-papyri" data-toggle="dropdown" href="#" role="button" id="dropdownSortPapyri">Sort by
                                <span class="caret"></span>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="dropdownSortPapyri">
                            {
                                let $params := request:get-query-string() => replace("sort-by=.+?&amp;", "")
                                return 
                                <div>
                                    <a class="dropdown-item" href="{concat('papyri.html?',$params,"sort-by=publication&amp;")}">Publication</a>
                                    <a class="dropdown-item" href="{concat('papyri.html?',$params,'sort-by=type&amp;')}">Type</a>
                                    <a class="dropdown-item" href="{concat('papyri.html?',$params,'sort-by=place&amp;')}">Place</a>
                                    <a class="dropdown-item" href="{concat('papyri.html?',$params,'sort-by=TM&amp;')}">TM</a>
                                </div>
                            }
                            </div>
                        </div>
                        <!-- pagination -->
                        {local:pagination($results, $pagelength)}
                        <!-- export (experimental) -->
                        {search-facets:export()}
                    </div>
                </h3>
                <div id="search-results">
                    {
                        (: subsequence: results, starting position, number of items:)
                        for $r in subsequence($results, ($pagelength*($page -1))+1, $pagelength)
                        return
                            search-facets:format-results($r)
                    }
                </div>
                <!-- pagination -->
                <div class="row bottom-pagination d-flex justify-content-end">
                    {local:pagination($results, $pagelength)}
                </div>
                
                {gramm:back-to-top()}
            </div>
        </div>
    </div>
};