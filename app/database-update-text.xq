xquery version "3.1";

import module namespace config="http://grammateus.ch/config" at "modules/config.xqm";
import module namespace g="http://grammateus.ch/global" at "modules/global.xql";

(: update database with latest texts :)

(: Update authority lists + external links + taxonomy :)
let $updated_files_1 := xmldb:store-files-from-pattern($config:data-root, "C:\Users\nury\Documents\grammateus\data", "*.xml", "text/xml", true(), "list_*")

(: Update descriptions :)
let $updated_files_3 := xmldb:store-files-from-pattern($config:data-root || "/descriptions", "C:\Users\nury\Documents\grammateus\data\descriptions", "*.xml")

(: Update classification :)
let $updated_files_4 := xmldb:store-files-from-pattern($config:data-root || "/classification", "C:\Users\nury\Documents\grammateus\data\classification", "*.xml")

(: Update introduction :)
let $updated_files_5 := xmldb:store-files-from-pattern($config:data-root || "/introduction", "C:\Users\nury\Documents\grammateus\data\introduction", "*.xml")

(: Update editorial :)
let $updated_files_5 := xmldb:store-files-from-pattern($config:data-root || "/editorial", "C:\Users\nury\Documents\grammateus\data\editorial", "*.html")

return $updated_files_3

