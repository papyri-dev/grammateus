xquery version "3.1";

import module namespace gramm="http://grammateus.ch/gramm" at "modules/gramm.xql";
import module namespace g="http://grammateus.ch/global" at "modules/global.xql";
import module namespace response="http://exist-db.org/xquery/response";

declare namespace tei = "http://www.tei-c.org/ns/1.0";


(:~
 : Transforms search results into a csv string with
 : - TM number
 : - Papyrus title
 : - Link to papyri.info
 :
 : @param $results - document node from a search in the database
 : @return String.
 :
:)
declare function local:xml2csv($results) {
    for $r in $results
    return
        gramm:tm($r) || "," ||
        gramm:title($r) || "," ||
        $g:papinfoddb || $r//tei:idno[@type eq 'ddb-hybrid'] || "," ||
        normalize-space(gramm:get-catDesc($r//tei:catRef[@n eq 'format-type'])) || "," ||
        normalize-space(gramm:get-catDesc($r//tei:catRef[@n eq 'format-subtype'])) || "," ||
        normalize-space(gramm:get-catDesc($r//tei:catRef[@n eq 'format-variation'])) || "&#10;"
};

(:~
 : Creates download header.
 :
 : @param $node - response content
 : @param $filname - string
 : @return response stream.
 :
:)
declare function local:download($node, $filename) {
    response:set-header("Content-Disposition", concat("attachment;
filename=", $filename))
    ,
    response:stream(
        $node,
        'indent=yes' (: serialization :)
        )
};

(:: EXPORT ::)
(: Searching the database with query parameters and creates download file :)

(: get list of all existing parameters:)
let $params := request:get-parameter-names()
    
(: create the facet options from existing parameters :)
let $options := map{
    "facets" : map:merge(
        (:add only facet parameters to the map:)
        for $p in $params
        where contains($p, "facet:")
        return map { substring-after($p, "facet:"): request:get-parameter($p, "")}
    )
}
    
(: filter papyri collection for the date :)
let $results_date :=
    if (["min-date", "max-date"] = $params) 
    then 
        let $min := if ("min-date" = $params) then request:get-parameter("min-date", ()) else "-300"
        let $max := if ("max-date" = $params) then request:get-parameter("max-date", ()) else "300"
            return gramm:date-filter($min, $max, collection($g:papyri)//tei:TEI)
    else collection($g:papyri)//tei:TEI
    
(: filter results for the keyword :)
let $keyword := request:get-parameter("keyword", "")
    
let $results_kw := 
    (: filter docs by keyword :)
    if ($keyword != '') then $results_date[ft:query(., $keyword, $options)]
    (: or else search all documents :)
    else $results_date[ft:query(., (), $options)]
        
(: sort result :)
let $order-by := request:get-parameter("order-by", "")
let $sort := 
    if ($order-by eq 'type') then 'for $r in $results_kw
        order by gramm:get-catDesc($r//tei:catRef[@n eq "format-type"]),
        gramm:get-catDesc($r//tei:catRef[@n eq "format-subtype"]),
        gramm:get-catDesc($r//tei:catRef[@n eq "format-variation"])
        return $r'
    else if ($order-by eq 'place') then 'for $r in $results_kw 
        order by gramm:origPlace($r//tei:history) return $r'
    else if ($order-by eq 'TM') then 'for $r in $results_unsorted order by gramm:tm($r) => number() return $r'
    else 'for $r in $results_kw
        order by $r//tei:title[@level eq "s"], number($r//tei:biblScope[@type eq "volume"]), number($r//tei:biblScope[@type eq "numbers"]), $r//tei:biblScope[@type eq "generic"] return $r'

let $results := util:eval($sort)

(: CSV header :)
let $header := "TM, Title, papyri.info, Type, Subtype, Variation" || "&#10;"
    
let $csv_results := local:xml2csv($results)

(: CSV content = header + results :)
let $csv := ($header, $csv_results)
    
return
    local:download($csv, fn:current-date()=>fn:adjust-date-to-timezone(()) || "-results.csv")