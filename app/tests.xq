xquery version "3.1";

import module namespace config="http://grammateus.ch/config" at "modules/config.xqm";
import module namespace g="http://grammateus.ch/global" at "modules/global.xql";
import module namespace gramm="http://grammateus.ch/gramm" at "modules/gramm.xql";
import module namespace show="http://grammateus.ch/show" at "modules/show.xql";

declare namespace tei = "http://www.tei-c.org/ns/1.0";
declare namespace functx = 'http://www.functx.com';
declare namespace json="http://www.json.org";

declare option exist:serialize "method=html media-type=text/html";


declare function functx:substring-after-last
  ( $arg as xs:string? ,
    $delim as xs:string )  as xs:string {
    replace ($arg,concat('^.*',$delim),'')
 };
 
declare function functx:add-attributes
  ( $elements as element()* ,
    $attrNames as xs:QName* ,
    $attrValues as xs:anyAtomicType* )  as element()? {

   for $element in $elements
   return element { node-name($element)}
                  { for $attrName at $seq in $attrNames
                    return if ($element/@*[node-name(.) = $attrName])
                           then ()
                           else attribute {$attrName}
                                          {$attrValues[$seq]},
                    $element/@*,
                    $element/node() }
} ;

let $doc := doc($g:phase1||'13001.xml')

return gramm:title($doc)


