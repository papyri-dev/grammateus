xquery version "3.1";

import module namespace config="http://grammateus.ch/config" at "modules/config.xqm";
import module namespace URI="http://grammateus.ch/URI" at "modules/URI.xqm";
import module namespace gramm="http://grammateus.ch/gramm" at "modules/gramm.xqm";
import module namespace show="http://grammateus.ch/show" at "modules/show.xqm";

declare namespace tei = "http://www.tei-c.org/ns/1.0";
declare namespace functx = 'http://www.functx.com';
declare namespace json="http://www.json.org";

declare option exist:serialize "method=html media-type=text/html";

let $doc := doc("/db/apps/grammateus/data/papyri/19654.xml")//tei:TEI

(: w, h, txt, fibres, nb_lines nb_col :)

return $doc

