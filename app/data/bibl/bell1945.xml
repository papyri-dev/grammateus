<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title level="a">The Arabic Bilingual Entagion</title>
                <title level="j">Proceedings of the American Philosophical Society</title>
                <funder>
                    <orgName key="SNF">Swiss National Science Foundation</orgName> - <ref target="https://p3.snf.ch/Project-182205">Project 182205</ref>
                </funder>
                <editor role="creator" ref="https://www.zotero.org/enury">enury</editor>
                <respStmt>
                    <resp>Record added to Zotero by</resp>
                    <name ref="https://www.zotero.org/enury">enury</name>
                </respStmt>
            </titleStmt>
            <publicationStmt>
                <authority>Grammateus Project</authority>
                <idno type="URI">https://grammateus.unige.ch/data/bibl/bell1945.xml</idno>
                <availability>
                    <licence target="https://creativecommons.org/licenses/by-nc/4.0/">Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)</licence>
                </availability>
                <date>2024-10-09+02:00</date>
            </publicationStmt>
            <sourceDesc>
                <p>Born digital document.</p>
            </sourceDesc>
        </fileDesc>
        <revisionDesc>
            <change when="2024-10-09+02:00">CREATED: This bibl record was autogenerated from a Zotero record.</change>
        </revisionDesc>
    </teiHeader>
    <text>
        <body>
            <biblStruct>
                <analytic>
                    <author>
                        <forename>Harold Idris</forename>
                        <surname>Bell</surname>
                    </author>
                    <title level="a">The Arabic Bilingual Entagion</title>
                    <idno type="URI">QBPCJZIW</idno>
                    <idno type="URI">https://www.zotero.org/groups/grammateus/items/QBPCJZIW</idno>
                    <idno type="URI">https://www.zotero.org/groups/2288077/items/QBPCJZIW</idno>
                    <ref target="https://papyri.info/biblio/38019"/>
                    <idno type="URI" subtype="doc">https://www.jstor.org/stable/985433</idno>
                </analytic>
                <monogr>
                    <title level="j">Proceedings of the American Philosophical Society</title>
                    <title level="j" type="short">ProcPhilSoc</title>
                    <imprint>
                        <date>1945</date>
                    </imprint>
                    <biblScope unit="pp">531-542</biblScope>
                    <biblScope unit="vol">89</biblScope>
                </monogr>
                <note type="abstract">The word έντάγιον is regularly used in Byzantine papyri in the sense of "receipt." This cannot have been the original meaning, for έντάσσω, from which it is derived, could not yield such a sense and actually occurs as "to requisition." In fact there are some instances in Byzantine papyri of the word έντάγιοψ itself meaning an order for payment, which was clearly its original sense. The evolution must have been "order for payment," "receipted order," "receipt." The original sense is that in which the word occurs in the letters of the Arab Governor of Egypt, Kurrah b. Sharīk from Aphrodito. An entagion is there an order for the payment of taxes, the delivery of supplies, or the execution of work, addressed to the people of a village or other locality and enclosed with the Governor's letter (to the pagarch) in which the service was ordered. The orders to the individual taxpayer were issued by the pagarch. There was a difference of form between the entagion and the letter to a pagarch: the latter was sent in duplicate, Arabic and Greek in separate rolls, whereas in the entagion Arabic and Greek were in the same roll. Moreover the letters to pagarchs were written in a sloping cursive hand with hardly any abbreviations, the entagia in minuscules, freely abbreviated. At 'Auja Ḥafīr in Palestine the Colt Expedition discovered a quantity of Greek and Arabic papyri which included seven entagia, here published in full, which, despite certain differences in detail, show a remarkable resemblance in form and arrangement to the somewhat later Egyptian examples. Clearly the same general scheme was employed in various provinces of the Arab Empire. This fact is most plausibly explained as due to borrowing from the Byzantine practice, and it is likely that the form of the entagion was evolved within the Byzantine Empire before the Arab conquest. As there appear to be no actual examples of Byzantine entagia of a form closely similar (except for the use of Arabic) to those of Arab times, it may be that the form was evolved not in Egypt but in some other province, e. g. Syria, was there borrowed by the Arabs and by them introduced into Egypt.</note>
            </biblStruct>
        </body>
    </text>
</TEI>