<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="c1">
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title>Classification of Greek Documentary Papyri: Epistolary Exchange</title>
                <author corresp="../authority-lists.xml#SF">
                    <forename>Susan</forename>
                    <surname>Fogarty</surname>
                </author>
                <author corresp="../authority-lists.xml#PS">
                    <forename>Paul</forename>
                    <surname>Schubert</surname>
                </author>
                <respStmt>
                    <resp>Encoded in TEI P5 by</resp>
                    <name corresp="../authority-lists.xml#EN">
                        <forename>Elisa</forename>
                        <surname>Nury</surname>
                    </name>
                </respStmt>
                <respStmt>
                    <resp>With the collaboration of</resp>
                    <name corresp="../authority-lists.xml#LF">
                        <forename>Lavinia</forename>
                        <surname>Ferretti</surname>
                    </name>
                </respStmt>
                <funder>
                    <orgName key="SNF">Swiss National Science Foundation</orgName> - <ref target="https://data.snf.ch/grants/grant/182205">Project 182205</ref>
                </funder>
            </titleStmt>
            <publicationStmt>
                <authority>Grammateus Project</authority>
                <idno type="DOI">10.26037/yareta:knltgzaynzdbfdfa2wguun22fu</idno>
                <availability>
                    <licence target="https://creativecommons.org/licenses/by-nc/4.0/">Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)</licence>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <p>Born digital document. Encoded from a Microsoft Word document.</p>
            </sourceDesc>
        </fileDesc>
        <encodingDesc>
            <classDecl>
                <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:tei="http://www.tei-c.org/ns/1.0" href="../grammateus_taxonomy.xml"/>
            </classDecl>
            <p>This file is encoded to comply with TEI Guidelines P5, Version 4.0.0.
                    <ref>https://tei-c.org/guidelines/p5/</ref>
            </p>
        </encodingDesc>
        <profileDesc>
            <langUsage>
                <language ident="lat">Latin</language>
                <language ident="grc">Greek</language>
            </langUsage>
            <textClass>
                <catRef n="type" scheme="#grammateus_taxonomy" target="#gt_ee"/>
            </textClass>
        </profileDesc>
        <revisionDesc>
            <change who="../authority_lists.xml#EN" when="2023">Archived on Yareta</change>
            <change who="../authority-lists.xml#EN" when="2022-06-02">TEI file created</change>
            <change who="../authority-lists.xml#SF" when="2022-04-27">Word document</change>
        </revisionDesc>
    </teiHeader>
    <text>
        <body>
            <head>Epistolary Exchange</head>
            <p xml:id="c1_p1">The Type Epistolary Exchange encompasses all documents which are part of a
                multidirectional communication and are composed in the style of a letter. The
                formulation of the content is always subjective: the authors speak in their own
                name.</p>
            <p xml:id="c1_p2">A spectrum of Sub-types and Variations can be mapped where the scribe adapts the
                epistolary format to suit a number of purposes, as follows: <figure xml:id="c1_f1" n="1">
                    <head>Epistolary Exchange Visualisation.</head>
                    <figDesc>The figure shows all the sub-types and variations in the EE category,
                        displayed in a tree diagram.</figDesc>
                    <graphic url="ee.png" rend="inline"/>
                </figure>
            </p>
            <p xml:id="c1_p3">The structural elements are arranged so as to establish a model document for each
                sub-type and variation : <figure xml:id="c1_f2" n="2">
                    <head>Epistolary Exchange - Structural Elements.</head>
                    <figDesc>The figure shows a table of the general structure for each category of
                        EE documents.</figDesc>
                    <graphic url="ee_table.jpg" rend="inline"/>
                </figure>
            </p>
            <div xml:id="c1_div1_private" n="1" type="section">
                <head xml:id="private">Private Letter</head>
                <p xml:id="c1_p4">The [<ref type="descr" target="#gt_letterPrivate">private letter</ref>] can be
                    interpreted as a basic scribal model, easily adapted to facilitate the
                    production of numerous sub-types of document with a variety of content.</p>
                <div xml:id="c1_div2_condolence">
                    <head xml:id="condolence">Condolence</head>
                    <p xml:id="c1_p5">A letter of [<ref target="#gt_condolence" type="descr">condolence</ref>] may
                        have some expressions added to the opening address and contain typical
                        phrases of consolation.</p>
                </div>
                <div xml:id="c1_div3_recommendation">
                    <head xml:id="recommendation">Recommendation</head>
                    <p xml:id="c1_p6">A letter of [<ref type="descr" target="#gt_rec">recommendation</ref>] has
                        distinct sections and is constructed around the verb <term xml:lang="grc">συνίστημι</term>
                        <gloss>I introduce</gloss>.</p>
                    <p xml:id="c1_p7">There are generally no changes to the format and layout in both variations of
                        the private letter.</p>
                </div>
            </div>
            <div xml:id="c1_div4_business" type="section" n="2">
                <head xml:id="business">Business Letter</head>
                <p xml:id="c1_p8">The [<ref type="descr" target="#gt_letterBusiness">business letter</ref>] is
                    differentiated by its content. A <seg xml:id="c1_s1" type="syntax" corresp="../authority_lists.xml#date">[date]</seg> is not routinely
                    included; however a distinguishing feature of the business letters in the Zenon
                    archive (III BCE) is the inclusion of a <seg xml:id="c1_s2" type="syntax" corresp="../authority_lists.xml#date">[date@end]</seg>, 
                    usually after the closing formula [<ref type="internal" target="755.xml">755</ref> 257 BCE, Alexandria].</p>
                <p xml:id="c1_p9">The scribe may also leave a large lower margin or space after the main text to
                    facilitate the addition of other signatories as a method of authentication [<ref type="internal" target="19606.xml">19606</ref> 113-120 CE, Hermopolis
                    Magna], see <bibl>
                        <author>Sarri</author>
                        <ptr target="gramm:sarri2018"/>
                        <citedRange>146-192</citedRange>
                    </bibl>
                </p>
                <p xml:id="c1_p10">Other documents with business content are based on a different model, that of the
                        <term xml:lang="grc">ὑπόμνημα</term>, (<term xml:lang="lat">hypomnema</term>) and are treated under the type Transmission of Information:
                    sub-type [<ref type="descr" target="#gt_businessNote">business note</ref>].</p>
                <div xml:id="c1_div5_order">
                    <head xml:id="order">Order to Pay</head>
                    <p xml:id="c1_p11">For an [<ref type="descr" target="#gt_order">order to pay</ref>] the scribe
                        adapts the basic business letter model by omitting the closing salutation,
                        but a <seg xml:id="c1_s3" type="syntax" corresp="../authority_lists.xml#date">[date@end]</seg> is always added. The formula remains fairly consistent throughout
                        the Ptolemaic, Roman, and Byzantine periods.</p>
                    <p xml:id="c1_p12">For this variation, more examples in squarish format survive [<ref target="114264.xml" type="internal">114264</ref> 157 CE, Oxyrhynchite
                        nome, with vertical fibres; <ref type="internal" target="20124.xml">20124</ref> 237 CE, Oxyrhynchite nome, with horizontal fibres].</p>
                    <p xml:id="c1_p13">A space may be left after the main text for the insertion of a validation
                        signature or greeting [<ref target="20647.xml" type="internal">20647</ref>
                        160 CE, Oxyrhynchus].</p>
                    <p xml:id="c1_p14">Some orders to pay from III CE are based on a different model, the <term xml:lang="grc">ὑπόμνημα</term>, and are to be found under the type
                        Transmission of Information: sub-type [<ref type="descr" target="#gt_businessNote">business note<ptr target="#d2_p19"/>
                        </ref>]. There are some of these
                        different models with the opening address of a business note, but where the
                        scribe has also added the epistolary greeting [<term xml:lang="grc">χαίρειν</term>]. This must be categorised as a <hi rend="bold">hybrid</hi> variation with both letter and business note
                        characteristics, e.g. [<ref target="16336.xml" type="internal">16336</ref>
                        261/312 CE].</p>
                </div>
            </div>
            <div xml:id="c1_div6_official" type="section" n="3">
                <head xml:id="official">Official Letter</head>
                <p xml:id="c1_p15">In the introductory formula of an [<ref type="descr" target="#gt_letterOfficial">official letter</ref>] the scribe adds the official status of one or both
                    of the parties [<ref type="internal" target="13452.xml">13452</ref> 23 CE; <ref target="8811.xml" type="internal">8811</ref> 185 CE, both Arsinoite
                    nome].</p>
                <p xml:id="c1_p16">A reverse form of the standard opening address is also found emphasising a
                    hierarchical relationship, e.g. [<ref type="internal" target="12603.xml">12603</ref> 214-215 CE, Oxyrhynchus, addressed to the prefect; <ref target="https://papyri.info/ddbdp/sb;14;11647" type="external">18168</ref>
                    280 CE, Memphis, from a strategos to an epistrategos]; both are very formally
                    written in a chancery hand.</p>
                <p xml:id="c1_p17">While the closing salutation is usually <seg xml:id="c1_s4" type="syntax" corresp="../authority_lists.xml#subscription"/> or <seg xml:id="c1_s5" corresp="../authority_lists.xml#subscription" type="syntax">[<term xml:lang="grc">ἐρρῶσθαί σε εὔχομαι</term>]</seg>, letters from the
                    higher levels of Roman administration close with <seg xml:id="c1_s6" corresp="../authority_lists.xml#subscription" type="syntax">[<term xml:lang="grc">ἐρρῶσθαί σε βούλομαι</term>]</seg>, e.g. [<ref type="external" target="https://papyri.info/ddbdp/p.oxy;20;2265">17201</ref>
                    120-123 CE, Alexandria]. The closing salutation is usually followed by a <seg xml:id="c1_s7" corresp="../authority_lists.xml#date" type="syntax">[date@end]</seg>.</p>
                <p xml:id="c1_p18">There may be a number of hands involved in the production of an official letter,
                    which can point to a process - blank spaces may be left by the scribe for the
                    addition of other hands [<ref target="17913.xml" type="internal">17913</ref> 290
                    CE, Oxyrhynchus].</p>
                <div xml:id="c1_div7_circular">
                    <head xml:id="circular">Circular</head>
                    <p xml:id="c1_p19">A [<ref type="descr" target="#gt_circular">circular</ref>] letter is
                        differentiated from a standard official letter in its purpose and structure.
                        It is a letter sent to multiple recipients, usually containing some
                        instructions, e.g. [<ref type="internal" target="20720.xml">20720</ref> 288
                        CE, Oxyrhynchus]. Circulars may also include some previous
                        correspondence.</p>
                    <p xml:id="c1_p20">The opening formula is as for any official letter, but the shorter <seg xml:id="c1_s8" corresp="../authority_lists.xml#introduction" type="syntax">[to <hi rend="italic">name</hi> &lt;dat.&gt;][<term xml:lang="grc">χαίρειν</term>]</seg> is also found, with the recipients addressed
                        by title only [<ref type="internal" target="41725.xml">41725</ref> 223/181
                        BCE, Arsinoite nome].</p>
                    <p xml:id="c1_p21">Circulars from higher authorities to other officials usually close <seg xml:id="c1_s9" corresp="../authority_lists.xml#subscription" type="syntax">[<term xml:lang="grc">ἐρρῶσθαι ὑμᾶς εὔχομαι</term>]</seg> [<ref type="internal" target="21819.xml">21819</ref> 278 CE, Oxyrhynchus]. The
                        more formal <seg xml:id="c1_s10" corresp="../authority_lists.xml#subscription" type="syntax">[<term xml:lang="grc">ἐρρῶσθαι ὑμᾶς βούλομαι</term>]</seg> is used
                        when an edict or decree is being circulated [<ref type="external" target="https://papyri.info/ddbdp/chr.wilck;;490">9286</ref> l.7].</p>
                    <p xml:id="c1_p22">The scribe may use some visual devices to separate the embedded letter from
                        the surrounding text, e.g. <term xml:lang="lat">paragraphoi</term> in [<ref type="internal" target="3663.xml">3663</ref>; <ref type="internal" target="78769.xml">78769</ref>], the indentation of the date between
                        letters in [<ref type="internal" target="21819.xml">21819</ref>], or the use
                        of spaces [<ref type="external" target="https://papyri.info/ddbdp/p.tebt;3.1;707">5319</ref>; <ref type="external" target="https://papyri.info/ddbdp/p.iand;7;140">11490</ref>].</p>
                </div>
                <div xml:id="c1_div8_warrant">
                    <head xml:id="warrant">Warrant</head>
                    <p xml:id="c1_p23">A [<ref type="descr" target="#gt_ee_warrant">warrant</ref>], or order to
                        arrest, has the specific purpose of ordering a person be brought before an
                        authority. In the Ptolemaic period a warrant could be drawn up by the scribe
                        as an official letter [<ref type="internal" target="1939.xml">1939</ref> 254
                        BCE, Philadelphia], or added as an apostil to the original petition [<ref target="12922.xml" type="internal">12922</ref> 34 CE Euhemeria]. Some
                        warrants presented as apostils appear in an adapted letter form at the end
                        of the original petition, with the epistolary opening followed by a brief
                        order to deliver the accused person, and the date; the closing farewell is
                        omitted [<ref type="external" target="http://www.papyri.info/ddbdp/p.tebt;3.2;961">5475</ref> 150/139
                        BCE, Tebtunis].</p>
                    <p xml:id="c1_p24">Other apostils are written as a short statement: <seg xml:id="c1_s11" corresp="../authority_lists.xml#introduction" type="syntax">[to <hi rend="italic">name</hi> &lt;dat.&gt;]</seg>, a brief order, no
                        farewell, and ending with the <seg xml:id="c1_s12" corresp="../authority_lists.xml#date" type="syntax">[date]</seg> [<ref target="https://papyri.info/ddbdp/sb;24;16295" type="external">8810</ref> l.40-42, 199 BCE], often in another hand. In the Roman
                        period this form of apostil evolved into documents with a different typology
                        and are categorised under Transmission of Information: sub-type [<ref type="descr" target="#gt_ti_warrant">warrant</ref>].</p>
                </div>
            </div>
            <div xml:id="c1_div9_cheirographon" type="section" n="4">
                <head xml:id="cheirographon">Cheirographon</head>
                <p xml:id="c1_p25">Throughout the Ptolemaic and Roman periods the standard letter format was adapted
                    to facilitate the drafting of private contracts in the form of a [<ref type="descr" target="#gt_cheiro">cheirographon</ref>]. They differ from the
                    standard letter in that there is no closing salutation, but like the official
                    letter there is invariably a [date@end] [<ref type="internal" target="20987.xml">20987</ref> 48 CE, Oxyrhynchus]. The text is displayed in
                    a single block with few features to distinguish sections [<ref type="internal" target="9159.xml">9159</ref> 276-277 CE, Philadelphia].</p>
                <p xml:id="c1_p26">For contracts with typologies other than that of a cheirographon, see Transmission
                    of Information: sub-type [<ref type="descr" target="#gt_statement">statement</ref>]: variations [<ref type="descr" target="#gt_proposalContract">proposal to contract</ref>] and [<ref type="descr" target="#gt_undertaking">undertaking</ref>], and Objective
                    Statement: sub-types: [<ref type="descr" target="#gt_syngr">syngraphe</ref>],
                        [<ref type="descr" target="#gt_synch">synchoresis</ref>], [<ref type="descr" target="#gt_pp">private protocol</ref>].</p>
                <div xml:id="c1_div10_homologia" type="subsection" n="1">
                    <head xml:id="homologia">Homologia</head>
                    <p xml:id="c1_p27">The subjective [<ref type="descr" target="#gt_ee_homologia">homologia</ref>]
                        is the same sub-type of document as a <term xml:lang="lat">cheirographon</term> but the infinitive <term xml:lang="grc">χαίρειν</term> is dropped [<ref type="internal" target="20176.xml">20176</ref> 44 CE, Arsinoite nome; <ref type="internal" target="10875.xml">10875</ref> 161 CE, Theadelphia].</p>
                    <p xml:id="c1_p28">Another opening is of a kind more associated with an [<ref type="descr" target="#gt_letterOfficial">official letter<ptr target="#d21_p9"/>
                        </ref>], with the addressee
                        placed first <seg xml:id="c1_s13" type="syntax" corresp="../authority_lists.xml#introduction">[to <hi rend="italic">name</hi> &lt;dat.&gt;][from <hi rend="italic">name</hi>
                            &lt;nom.&gt;]</seg> but again without <term xml:lang="grc">χαίρειν</term> [<ref type="internal" target="10556.xml">10556</ref> 131
                        CE, Theadelphia; <ref type="external" target="https://papyri.info/ddbdp/p.oxy;45;3252">15915</ref> 257-258 CE,
                        Aphroditopolite nome].</p>
                    <p xml:id="c1_p29">Other varieties of <term xml:lang="lat">homologiai</term> can be found under
                        Transmission of Information: sub-type [<ref type="descr" target="#gt_statement">statement</ref>]: variation [<ref type="descr" target="#gt_ee_homologia">homologia</ref>]. Objectively styled <term xml:lang="lat">homologiai</term> may be found under Objective Statement:
                        sub-type [<ref type="descr" target="#gt_syngr">syngraphe<ptr target="#homologia_obj"/>
                        </ref>].</p>
                </div>
            </div>
            <div xml:id="c1_div11_enteuxis" type="section" n="5">
                <head xml:id="enteuxis">Enteuxis</head>
                <p xml:id="c1_p30">For the [<ref type="descr" target="#gt_enteuxis">enteuxis</ref>], the scribe
                    adapts the standard letter opening by placing the addressee first and distancing
                    the petitioner by placing the greeting between both names: <seg xml:id="c1_s14" type="syntax" corresp="../authority_lists.xml#introduction">[to <hi rend="italic">name</hi> &lt;dat.&gt;][<term xml:lang="grc">χαίρειν</term>] [from <hi rend="italic">name</hi> &lt;nom.&gt;]</seg> [<ref type="internal" target="3302.xml">3302</ref> 222 BCE, Arsinoite nome]; this is the inverted
                    formula sometimes found in [<ref type="descr" target="#gt_letterOfficial">official letters</ref>], but the unusual position of <seg xml:id="c1_s15" type="syntax" corresp="../authority_lists.xml#introduction">[<term xml:lang="grc">χαίρειν</term>]</seg> is found only in <term xml:lang="lat">enteuxeis</term>.</p>
                <p xml:id="c1_p31">The closing salutation is usually <term xml:lang="grc">εὐτύχει</term> [<ref type="internal" target="1796.xml">1796</ref> 245-244 BCE, Philadelphia] and
                    there is generally no <seg xml:id="c1_s16" type="syntax" corresp="../authority_lists.xml#date">[date]</seg>. Documents from the Zenon archive may close with <seg xml:id="c1_s17" type="syntax" corresp="../authority_lists.xml#subscription">[<term xml:lang="grc">ἔρρωσο</term>]</seg> and may add a <seg xml:id="c1_s18" type="syntax" corresp="../authority_lists.xml#introduction">[date]</seg> [<ref type="internal" target="1739.xml">1739</ref> 257 BCE, Philadelphia].</p>
                <p xml:id="c1_p32">From the II BCE onwards a non-royal petition was drawn up with a different
                    typology and is categorised under Transmission of Information: sub-type [<ref type="descr" target="#gt_petition">petition</ref>].</p>
            </div>
            <div xml:id="c1_div12_receipt" type="section" n="6">
                <head xml:id="receipt">Receipt</head>
                <p xml:id="c1_p33">After the standard letter opening, a receipt is constructed around the phrase <seg xml:id="c1_s19" type="syntax" corresp="../authority_lists#main-text">[<term xml:lang="grc">ἔχω / ἔσχον
                            παρὰ σοῦ</term>]</seg>
                    <gloss>I receive/ have received from you</gloss>; the <seg xml:id="c1_s20" type="syntax" corresp="../authority_lists#date">[date]</seg> is invariably written after
                    the main text, and there is rarely a closing salutation [<ref type="external" target="https://papyri.info/ddbdp/p.mich;6;371">12175</ref> 201/230 CE,
                    Karanis; <ref type="external" target="https://papyri.info/ddbdp/p.oxy;50;3572">30083</ref> III CE, Oxyrhynchus].</p>
                <p xml:id="c1_p34">The text is often laid out as a single block [<ref type="external" target="https://papyri.info/ddbdp/p.oxy;50;3572">30083</ref>; <ref type="external" target="https://papyri.info/ddbdp/p.mich;6;371">12175</ref>]
                    with few distinguishing features. The date can be separated from the main text
                        [<ref type="external" target="https://papyri.info/ddbdp/p.mich;3;193">5244</ref>; <ref type="external" target="https://papyri.info/ddbdp/p.theon;;9">40820</ref> 195 CE].</p>
                <p xml:id="c1_p35">For receipts with a different typology, see Objective Statement: sub-types: [<ref type="descr" target="#gt_ee_receipt">receipt</ref>], [<ref type="descr" target="#gt_syngr">syngraphe</ref>], and variation [<ref type="descr" target="#gt_doubleDoc">double document</ref>].</p>
                <div xml:id="c1_div13_rent" type="subsection" n="1">
                    <head xml:id="rent">Rent</head>
                    <p xml:id="c1_p36">The surviving evidence for receipts for the payment of [<ref type="descr" target="#gt_rent">rent</ref>] indicate that they were often written
                        in the form of a letter and so constitute a variation with a specific
                        purpose.</p>
                    <p xml:id="c1_p37">The layout is as for receipts in general [<ref type="external" target="https://papyri.info/ddbdp/p.warr;;12">13700</ref>; <ref type="external" target="https://papyri.info/ddbdp/p.fay;;88">10931</ref>
                        204 CE, Theadelphia]. There are examples of rent receipts as double
                        documents [<ref type="external" target="https://papyri.info/ddbdp/p.cair.zen;3;59362v">1005</ref>];
                        sometimes there is more than one receipt on the same sheet [<ref type="external" target="https://papyri.info/ddbdp/p.oxy;14;1646">21955</ref> 268-269 CE, Oxyrhynchus; <ref type="external" target="https://papyri.info/ddbdp/p.princ;2;37">12832</ref> 255 CE,
                        Tanis].</p>
                    <p xml:id="c1_p38">Rent receipts were also drawn up with a different as objective statements, see [rent receipt_16]
                        </p>
                </div>
                <div xml:id="c1_div14_telos" type="subsection" n="2">
                    <head xml:id="telos">Tax on catoecic land (<term xml:lang="grc">τέλος
                            καταλοχισμῶν</term>)</head>
                    <p xml:id="c1_p39">Receipts for the payment of taxes on the transmission of catoecic land [<ref type="descr" target="#gt_telos">
                            <term xml:lang="grc">τέλος
                                καταλοχισμῶν</term>
                        </ref>] constitute a variation by virtue of their
                        specific purpose and uniform format.</p>
                    <p xml:id="c1_p40">The format and layout of the [<ref type="descr" target="#gt_letterBusiness">business letter</ref>] of the Ptolemaic period has been carried into
                        the Roman period for this specific type of receipt [<ref type="internal" target="11419.xml">11419</ref> 182-192 CE, Arsinoite nome; <ref type="internal" target="11271.xml">11271</ref> 206 CE, Arsinoite nome;
                            <ref target="15120.xml" type="internal">15120</ref> 204 CE, Arsinoite
                        nome].</p>
                    <p xml:id="c1_p41">These receipts are unusual for the Roman period in that they are oriented
                        transversa charta, the writing against the fibres.</p>
                    <p xml:id="c1_p42">There is often a closing salutation, followed by the date, and a signature by
                        the official.</p>
                    <p xml:id="c1_p43">On these documents see <bibl>
                            <author>Schubert</author>
                            <ptr target="gramm:schubert2019"/>
                        </bibl>.</p>
                </div>
            </div>
        </body>
    </text>
</TEI>