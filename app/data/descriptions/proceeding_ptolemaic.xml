<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="../schema/gramm.rng"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="d32">
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title>Description of Greek Documentary Papyri: Report of Official Proceedings
                    (Ptolemaic)</title>
                <author corresp="../authority-lists.xml#PS">
                    <forename>Paul</forename>
                    <surname>Schubert</surname>
                </author>
                <author corresp="../authority-lists.xml#SF">
                    <forename>Susan</forename>
                    <surname>Fogarty</surname>
                </author>
                <respStmt>
                    <resp>Encoded in TEI P5 by</resp>
                    <name corresp="../authority-lists.xml#EN">
                        <forename>Elisa</forename>
                        <surname>Nury</surname>
                    </name>
                </respStmt>
                <respStmt>
                    <resp>With the collaboration of</resp>
                    <name corresp="../authority-lists.xml#LF">
                        <forename>Lavinia</forename>
                        <surname>Ferretti</surname>
                    </name>
                </respStmt>
                <funder>
                    <orgName key="SNF">Swiss National Science Foundation</orgName> - <ref target="https://data.snf.ch/grants/grant/182205">Project 182205</ref>
                </funder>
            </titleStmt>
            <publicationStmt>
                <authority>Grammateus Project</authority>
                <idno type="DOI">10.26037/yareta:32euyqzp3vbc5iafvnzypygr4i</idno>
                <availability>
                    <licence target="https://creativecommons.org/licenses/by-nc/4.0/">Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)</licence>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <p>Born digital document. Encoded from a Microsoft Word document.</p>
            </sourceDesc>
        </fileDesc>
        <encodingDesc>
            <classDecl>
                <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:tei="http://www.tei-c.org/ns/1.0" href="../grammateus_taxonomy.xml"/>
            </classDecl>
            <p>This file is encoded to comply with TEI Guidelines P5, Version 4.0.0.
                    <ref target="https://tei-c.org/guidelines/p5/">https://tei-c.org/guidelines/p5/</ref>
            </p>
        </encodingDesc>
        <profileDesc>
            <langUsage>
                <language ident="lat">Latin</language>
                <language ident="grc">Greek</language>
            </langUsage>
            <textClass>
                <catRef n="type" scheme="#grammateus_taxonomy" target="#gt_ri"/>
                <catRef n="subtype" scheme="#grammateus_taxonomy" target="#gt_report"/>
                <catRef n="variation" scheme="#grammateus_taxonomy" target="#gt_officialProcP"/>
            </textClass>
        </profileDesc>
        <revisionDesc>
            <change who="../authority_lists.xml#EN" when="2023">Archived on Yareta</change>
            <change who="../authority-lists.xml#EN" when="2022-05-02">TEI file created</change>
            <change who="../authority-lists.xml#PS ../authority-lists.xml#SF" when="2021-09-28">Word
                document</change>
        </revisionDesc>
    </teiHeader>
    <text>
        <body>
            <head>Report of official proceedings</head>
            <head type="subtitle">Ptolemaic</head>
                <p xml:id="d32_p1">In Ptolemaic Egypt, reports of official proceedings may
                    originate from either judicial courts or administrative instances. These reports
                    are the result of an action brought by one party against another for, e.g.,
                    criminal acts or breaches of contract; or in an administrative setting,
                    malpractice, and include the decision of the court or tribunal on the matter. A
                    general survey is available in <bibl>
                        <author>Wolff</author>
                        <ptr target="gramm:wolff1962"/>
                    </bibl>. The judicial system is divided among several courts, with some degree
                    of overlap between them (see below). Criminal offences committed by officials
                    are handled by the local governors and their subordinates <bibl>
                        <author>Wolff</author>
                        <ptr target="gramm:wolff1962"/>
                        <citedRange>118</citedRange>
                    </bibl>. The general typology (structure, format and layout) of reports of
                    proceedings from either judicial or administrative instances being quite
                    similar, the following description covers both.</p>
                <p xml:id="d32_p2">In the Roman period, reports from officials cover military,
                    administrative, judicial and religious matters; see [<ref type="descr" target="#gt_officialProcR">report of official proceedings (Roman)</ref>], a
                    type of document that differs markedly from Ptolemaic reports.</p>
                <p xml:id="d32_p3">The three main judicial courts in Ptolemaic Egypt are: <list type="unordered">
                        <item>The <term xml:lang="grc">δικαστήριον</term> (<term xml:lang="lat">dikasterion</term>, <gloss>justice court</gloss>) <bibl>
                                <author>Wolff</author>
                                <ptr target="gramm:wolff1962"/>
                                <citedRange>37-48</citedRange>
                            </bibl>, an autonomous court (by permission of the king) open to
                            non-Egyptians.</item>
                        <item>The <term xml:lang="grc">λαοκρίται</term> (<term xml:lang="lat">laokritai</term>, <gloss>those in charge of deciding for the
                            people</gloss>) <bibl>
                                <author>Wolff</author>
                                <ptr target="gramm:wolff1962"/>
                                <citedRange>48-53</citedRange>
                            </bibl>, open to Egyptians.</item>
                        <item>The <term xml:lang="grc">χρηματισταί</term> (<term xml:lang="lat">chrematistai</term>
                            <gloss>those in charge of handling transactions</gloss>) <bibl>
                                <author>Wolff</author>
                                <ptr target="gramm:wolff1962"/>
                                <citedRange>64-89</citedRange>
                            </bibl>, who deal with judicial matters by delegation of the
                            king.</item>
                    </list>
                </p>
                <p xml:id="d32_p4">Consensus is lacking about the precise distribution of tasks
                    between these three courts. <bibl>
                        <author>Wolff</author>
                        <ptr target="gramm:wolff1962"/>
                        <citedRange>83</citedRange>
                    </bibl> opposes the <term xml:lang="lat">laokritai</term> to the <term xml:lang="lat">dikasterion</term>, the former being in charge of cases
                    brought by the Egyptians, and the latter, of those brought by the Greeks and
                    other non-Egyptians. With the gradual blurring of the distinction between ethnic
                    groups, the <term xml:lang="lat">dikasterion</term> disappears from the
                    available evidence in the late III BCE. According to this view, the court of
                        <term xml:lang="lat">chrematistai</term>, acting by delegation of the king,
                    was open to all and thus gradually overlapped with that of the <term xml:lang="lat">laokritai</term> to the point of causing the latter’s near
                    disappearance in the late II BCE.</p>
                <p xml:id="d32_p5">A royal ordinance of 118 BCE states that the jurisdiction of the
                court of <term xml:lang="lat">chrematistai</term> or <term xml:lang="lat">laokritai</term> is determined by the written language used in the contract
                passed by the opposing parties [<ref type="external" target="https://papyri.info/trismegistos/2938">2938</ref> l.207-220, ca. 118
                BCE, Kerkeosiris (?)]. In Wolff’s view, this is to be considered alongside amnesty
                decrees found in the same document: both measures – jurisdiction according to use of
                language, and amnesty – seem to point in the same direction: the king was apparently
                trying to restore institutional order in the kingdom, in the case of distinction of
                language by introducing an artificial limit between the jurisdiction of the courts
                of <term xml:lang="lat">laokritai</term> and <term xml:lang="lat">chrematistai</term>. He was thus protecting the <term xml:lang="lat">laokritai</term> from encroachment on the part of the <term xml:lang="lat">chrematistai</term>. Already close to disappearing in the late II BCE, the
                    <term xml:lang="lat">laokritai</term> courts are effectively shut down in the I
                BCE. This indicates that the attempt to revive them was unsuccessful.</p>
                <p xml:id="d32_p6">According to <bibl>
                    <author>Pestman</author>
                    <ptr target="gramm:pestman1985"/>
                </bibl>, however, [<ref type="external" target="https://papyri.info/trismegistos/2938">2938</ref> l.207-220] illustrates
                primarily the gradual shift in the criteria used for identifying ethnic groups. This
                was important because different legal systems coexisted in the country: the Egyptian
                population had retained its laws and customs inherited from the Pharaonic period
                    (<term xml:lang="grc">κατὰ τοὺς τῆς χώρας νόμους</term>
                <gloss>according to the laws of the country</gloss>) [<ref type="external" target="https://papyri.info/trismegistos/2938">2938</ref> l.219-220], and the
                immigrants brought another legal system to Egypt.</p>
                <p xml:id="d32_p7">Whereas it was relatively easy to distinguish an Egyptian from a
                    Greek in the III BCE, such was no longer the case in the II BCE. Therefore, the
                    criterion for directing an individual towards either the <term xml:lang="lat">laokritai</term> or the <term xml:lang="lat">chrematistai</term> was that
                    of language use: contracts written in demotic Egyptian were placed under the
                    authority of the <term xml:lang="lat">laokritai</term>, and those written in
                    Greek were entrusted to the <term xml:lang="lat">chrematistai</term>; and in a
                    transitory period, lawsuits of Egyptians against Egyptians about contracts
                    written in Greek remained under the jurisdiction of the <term xml:lang="lat">laokritai</term>.</p>
                <p xml:id="d32_p8">Whatever the outcome of this juristic debate, the <term xml:lang="lat">chrematistai</term> are better documented than the other
                    Ptolemaic courts due to the preservation of several reports containing their
                    judicial sentences. The <term xml:lang="lat">chrematistai</term> appear in the
                    mid-III BCE [<ref type="internal" target="1939.xml">1939</ref> 254 BCE, Arsinoite; <ref type="internal" target="847.xml">847</ref> 254 BCE,
                    Krokodilopolis; <ref type="external" target="https://papyri.info/trismegistos/849">849</ref> 254 BCE; <ref type="internal" target="1796.xml">1796</ref>
                    245-244 BCE, Philadelphia] and at first do not constitute a permanent court of
                    justice: they are appointed by the king according to current needs <bibl>
                        <author>Wolff</author>
                        <ptr target="gramm:wolff1962"/>
                        <citedRange>71</citedRange>
                    </bibl>. In the second century, a fixed court is established in every nome, with
                    three judges each. Individuals may submit petitions, such as those found in the so-called Archive of the Undertakers (<term xml:lang="lat">taricheutai</term>) of Tanis
                    <bibl>
                        <author>TM Arch 551</author>
                        <ptr target="gramm:baetensClarysse2021"/>
                    </bibl>, directly to the <term xml:lang="lat">chrematistai</term>;
                    there is no address to the king, even nominal. This court was discontinued when
                    Egypt became a Roman province.</p>
                <p xml:id="d32_p9">The jurisdiction of the <term xml:lang="lat">chrematistai</term>
                    is wide, their office corresponding to a delegation of power from the king
                    himself in matters relating to justice. This is reflected in their full
                        title: <term xml:lang="grc">χρηματισταὶ τῶν τὰς βασιλικάς, [judge # 1],
                        [judge # 2], [judge # 3], οἱ τὰ βασιλικὰ καὶ προσοδικὰ καὶ ἰδιωτικὰ
                        κρίναντες</term>
                    <gloss>those in charge of handling (the affairs pertaining) to the royal
                        (possessions?), [judge # 1], [judge # 2], [judge # 3], those deciding on
                        matters relating to the king, revenue and private persons</gloss> [<ref type="external" target="https://papyri.info/trismegistos/3510">3510</ref>
                    147 or 136 or 83 BCE, Memphis; <ref type="external" target="https://papyri.info/trismegistos/5240">5240</ref> 154 or 143 BCE,
                    Krokodilopolis; <bibl>
                        <author>Wolff</author>
                        <ptr target="gramm:wolff1962"/>
                        <citedRange>65</citedRange>
                    </bibl>].</p>
                <p xml:id="d32_p10">Sentences given by the <term xml:lang="lat">chrematistai</term>
                are recorded by an <term xml:lang="grc">εἰσαγωγεύς</term> (<term xml:lang="lat">eisagogeus</term>
                <gloss>the person in charge of introducing the case</gloss>). When enforcing the
                decision of the <term xml:lang="lat">chrematistai</term>, the <term xml:lang="lat">eisagogeus</term> may send a letter to another official, adding a copy of the
                report after the order [<ref type="external" target="https://papyri.info/trismegistos/3510">3510</ref>; <ref type="internal" target="5580.xml">5580</ref> 146-145 or 135-134 BCE, Krokodilopolis]. Also, a
                copy of a judicial sentence (<term xml:lang="grc">ἀπὸ καταδίκης χρηματισμός</term>
                    [<ref type="external" target="https://papyri.info/trismegistos/4906">4906</ref>
                l.2-3, ca. 51 BCE, Herakleopolite nome]) may be produced in support of a new case,
                for instance embedded within the text of a petition [<ref type="external" target="https://papyri.info/trismegistos/4905">4905</ref> ca. 51 BCE,
                Herakleopolite nome; <ref type="internal" target="44623.xml">44623</ref> ca. 134 BCE,
                Herakleopolis]. This explains why, in most instances, available reports of official
                proceedings – judicial and administrative – consist of copies, and not of the
                original records, which are lost. Enough evidence is preserved, however, to sketch
                an outline of their typology.</p>
                <p xml:id="d32_p11">Because the typology (structure, format, and layout) of judicial and
                administrative reports is very similar, an individual may resort to both authorities
                in order to settle the same case: e.g. [<ref type="external" target="https://papyri.info/trismegistos/3563">3563</ref> 117 BCE, Thebes], a
                report of a hearing before officials, which incidentally also preserves a mention of
                a petition submitted to the <term xml:lang="lat">chrematistai</term> (col. 2,
                l.4-7).</p>
            <div xml:id="d32_div1_structure" type="section" n="1">
                <head xml:id="structure">Structure</head>
                <p xml:id="d32_p12">The preserved copies seem to have retained the original
                    structure of the reports: <list type="unordered">
                        <item>
                            <seg xml:id="d32_s1" type="syntax" corresp="../authority-lists.xml#date">[date@start] [place]</seg> [<ref type="external" target="https://papyri.info/trismegistos/4906" corresp="#d32_s1">4906</ref> l.15; <ref type="external" target="https://papyri.info/trismegistos/3562" corresp="#d32_s1">3562</ref> l.1, 119 BCE, Thebes].</item>
                        <item>Names and titles of those in charge of the case (appointed judges or
                            officials from the administration) [<ref type="external" target="https://papyri.info/trismegistos/3510">3510</ref> l.4-6;
                                <ref type="external" target="https://papyri.info/trismegistos/156">156</ref> fr. B, 134 BCE, Thebes].</item>
                        <item>
                            <seg xml:id="d32_s2" type="syntax" corresp="../authority-lists.xml#main-text">[<term xml:lang="grc">καταστάντος</term>][name]</seg>
                            <gloss corresp="#d32_s2">N having stood (before the court)</gloss> [<ref type="external" target="https://papyri.info/trismegistos/43085" corresp="#d32_s2">43085</ref> mid-II BCE, Herakleopolite nome; <ref type="external" target="https://papyri.info/trismegistos/3563" corresp="#d32_s2">3563</ref> l.9]. This is the standard formula, but
                            circumstances may call for an expansion, e.g. <term xml:lang="grc">πλειόνων γενομένων καταστάσεων Βερενίκης πρὸς Σωσίβιον καὶ τοῦ
                                Σωσιβίου πρὸς τὴν Βερενίκην</term>
                            <gloss>Since there were several appearances of Berenike against
                                Sosibios, and of Sosibios against Berenike ...</gloss> [<ref type="external" target="https://papyri.info/trismegistos/4906">4906</ref> l.18-19]. In the case of an informal copy [<ref type="external" target="https://papyri.info/trismegistos/43085">43085</ref>], the text may start directly with <term xml:lang="grc">καταστάντος</term>, without the preceding introductory data. This
                            report was used as supporting evidence for a petition [<ref type="external" target="https://papyri.info/trismegistos/43084">43084</ref> 170-156 BCE (?), Herakleopolite nome]; see the introduction to <bibl>
                                    <author>P.Gen. III</author>
                                    <ptr target="gramm:schubert1996"/>
                                    <citedRange>no. 126-127</citedRange>
                                </bibl> for an overview.</item>
                        <item>The case is then laid out, in indirect speech, in marked contrast with
                                [<ref type="descr" target="#gt_officialProcR">reports of official
                                proceedings (Roman)</ref>], where the exchange is normally quoted
                                <term xml:lang="lat">verbatim</term>. Participial clauses in the
                            genitive absolute abound, combined with relative clauses [<ref type="external" target="https://papyri.info/trismegistos/43085">43085</ref>], in accordance with the current legal language of the
                            time.</item>
                        <item>The judge or official presiding over the hearing delivers the
                            decision [<ref type="external" target="https://papyri.info/trismegistos/3510">3510</ref> l.23: <term xml:lang="grc">σ[υ]νεκρίναμ[ε]ν</term>
                            <gloss>we have given our sentence</gloss>; <ref type="external" target="https://papyri.info/trismegistos/156">156</ref> l.76-86; <ref type="external" target="https://papyri.info/trismegistos/3562">3562</ref> l.66-69;
                                <ref type="external" target="https://papyri.info/trismegistos/3596">3596</ref> l.15-19 119 BCE, Thebes]. The sentence can be quite
                            lengthy, e.g. [<ref type="internal" target="5580.xml">5580</ref>
                            l.30-53].</item>
                        <item>The <term xml:lang="lat">eisagogeus</term> may be named at the end,
                            with his signature in his own separate hand [<ref type="external" target="https://papyri.info/trismegistos/3510">3510</ref>]. This
                            implies that, although he is the nominal author of the report, the
                            actual copying is done by a subordinate scribe. He checks the entire
                            document and adds <term xml:lang="grc">ἀνέγνωσται</term>
                            <gloss>it has been read over</gloss> [<ref type="external" target="https://papyri.info/trismegistos/3510">3510</ref>; <ref type="external" target="https://papyri.info/trismegistos/5284">5284</ref> 67 BCE (?), Oxyrhynchus (?)]. This control mark is found
                            again in [<ref type="descr" target="#gt_officialProcR">reports of
                                official proceedings (Roman)</ref>], in the active <term xml:lang="grc">ἀνέγνων</term>
                            <gloss>I have read over</gloss>.</item>
                    </list>
                </p>
            </div>
            <div xml:id="d32_div2_format" type="section" n="2">
                <head xml:id="format">Format</head>
                <p xml:id="d32_p13">Reports of official proceedings being often quite long, they are
                    copied on horizontal rolls displaying several columns of
                    writing. This practice is maintained in [<ref type="descr" target="#gt_officialProcR">reports of official proceedings (Roman)</ref>].
                    The reports are copies, produced by trained scribes, and they are all written in
                    one hand, with the exception of the control mark added by the <term xml:lang="lat">eisagogeus</term> (see above).</p>
                <p xml:id="d32_p14">[<ref type="external" target="https://papyri.info/trismegistos/3562">3562</ref>] consists of a
                    fully preserved roll (H. 20.5 x W. 73cm), made of four sheets pasted together
                    (three <term xml:lang="lat">kolleseis</term>). The text is distributed among
                    three columns; the writing follows the direction of fibres. The same general
                    features are to be found in [<ref type="external" target="https://papyri.info/trismegistos/3563">3563</ref>] (H. 31-35 x W.
                    192cm), made of 14 sheets for 10 columns (each ca. 17-20cm wide) written in the
                    direction of fibres.</p>
                <p xml:id="d32_p15">This pattern is confirmed by [<ref type="internal" target="5580.xml">5580</ref>], consisting of two fragments. The roll in its
                    original dimensions, taking into account a part of the text that is missing,
                    must have had a height of ca. 30cm, and a width of ca. 60cm. The text is
                    distributed among three columns, the two first ca. 19cm wide, the third ca.
                    12.5cm; the writing follows the direction of fibres.</p>
            </div>
            <div xml:id="d32_div3_layout" type="section" n="3">
                <head xml:id="layout">Layout</head>
                <p xml:id="d32_p16">In [<ref type="internal" target="5580.xml">5580</ref>], several
                    sections are separated by a <term xml:lang="lat">paragraphos</term>: 1) letter
                    requesting that the sentence be enforced; 2) opening section of the report; 3)
                    description of the case itself; 4) control mark added by the <term xml:lang="lat">eisagogeus</term>. Section (2) is also highlighted by an
                        <term xml:lang="lat">eisthesis</term> of the whole paragraph, and sub-parts
                    of (3) are marked by the <term xml:lang="lat">ekthesis</term> of the initial
                    line of the paragraph (col. 2, l.26, 30, 37). The date of the initial request
                    (col. 1, l.5), as well as <term xml:lang="lat">eisagogeus</term>’ control mark
                    (col. 3, l.54), are offset to the right.</p>
            </div>
        </body>
    </text>
</TEI>