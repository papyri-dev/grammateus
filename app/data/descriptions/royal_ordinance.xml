<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="../schema/gramm.rng"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="d39">
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title>Description of Greek Documentary Papyri: Royal Ordinance</title>
                <author corresp="../authority-lists.xml#SF">
                    <forename>Susan</forename>
                    <surname>Fogarty</surname>
                </author>
                <author corresp="../authority-lists.xml#PS">
                    <forename>Paul</forename>
                    <surname>Schubert</surname>
                </author>
                <respStmt>
                    <resp>Encoded in TEI P5 by</resp>
                    <name corresp="../authority-lists.xml#EN">
                        <forename>Elisa</forename>
                        <surname>Nury</surname>
                    </name>
                </respStmt>
                <respStmt>
                    <resp>With the collaboration of</resp>
                    <name corresp="../authority-lists.xml#LF">
                        <forename>Lavinia</forename>
                        <surname>Ferretti</surname>
                    </name>
                </respStmt>
                <funder>
                    <orgName key="SNF">Swiss National Science Foundation</orgName> - <ref target="https://data.snf.ch/grants/grant/182205">Project 182205</ref>
                </funder>
            </titleStmt>
            <publicationStmt>
                <authority>Grammateus Project</authority>
                <idno type="DOI">10.26037/yareta:73ic5bjk2jdp3nyvk637nltouy</idno>
                <availability>
                    <licence target="https://creativecommons.org/licenses/by-nc/4.0/">Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)</licence>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <p>Born digital document. Encoded from a Microsoft Word document.</p>
            </sourceDesc>
        </fileDesc>
        <encodingDesc>
            <classDecl>
                <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:tei="http://www.tei-c.org/ns/1.0" href="../grammateus_taxonomy.xml"/>
            </classDecl>
            <p>This file is encoded to comply with TEI Guidelines P5, Version 4.0.0.
                    <ref target="https://tei-c.org/guidelines/p5/">https://tei-c.org/guidelines/p5/</ref>
            </p>
        </encodingDesc>
        <profileDesc>
            <langUsage>
                <language ident="lat">Latin</language>
                <language ident="grc">Greek</language>
            </langUsage>
            <textClass>
                <catRef n="type" scheme="#grammateus_taxonomy" target="#gt_os"/>
                <catRef n="subtype" scheme="#grammateus_taxonomy" target="#gt_announcement"/>
                <catRef n="variation" scheme="#grammateus_taxonomy" target="#gt_decree"/>
            </textClass>
        </profileDesc>
        <revisionDesc>
            <change who="../authority_lists.xml#EN" when="2023">Archived on Yareta</change>
            <change who="../authority-lists.xml#EN" when="2022-03-10">TEI file created</change>
            <change who="../authority-lists.xml#PS ../authority-lists.xml#SF" when="2021-11-22">Word
                document</change>
        </revisionDesc>
    </teiHeader>
    <text>
        <body>

            <head>Royal ordinance</head>
            <head type="subtitle">
                <term xml:lang="grc">πρόσταγμα</term>
            </head>
            <p xml:id="d39_p1">A royal ordinance (<term xml:lang="grc">πρόσταγμα</term>) is a decree
                or command from the Ptolemaic period establishing a law or regulation, or setting a
                precedent.</p>
            <p xml:id="d39_p2">The work of reference for royal ordinances is <bibl>
                    <author>Lenger</author>
                    <ptr target="gramm:lenger1980"/>
                </bibl> - a collection of decrees, royal letters, fragments, and references to
                decrees in other documents - along with the supplementary <bibl>
                    <author>Lenger</author>
                    <ptr target="gramm:lenger1990"/>
                </bibl>. It should be noted that the terms <q>decree</q> and <q>ordinance</q> are
                used interchangeably in the literature.</p>
            <p xml:id="d39_p3">Royal ordinances distinctively have some form of <term xml:lang="grc">προστάσσω</term>
                <gloss>I decree/order</gloss> as the main verb and concern general issues. They are to be
                distinguished from <term xml:lang="grc">διαγράμματα</term>, which are
                contemporaneous laws and regulations, but which are not ostensibly issued by the
                king and are anonymous; on these see <bibl>
                    <author>Lenger</author>
                    <ptr target="gramm:lenger1980"/>
                    <citedRange>xxi and n.2</citedRange>
                </bibl>. They are also to be distinguished from royal letters which do not tend have
                    <term xml:lang="grc">προστάσσω</term> as the main verb and usually relate to
            individual requests or petitions [<ref target="8669.xml" type="internal">8669</ref> 157 BCE, Soknopaiou Nesos], see <bibl>
                    <author>Modrzejewski</author>
                    <ptr target="gramm:modrzejewski1951"/>
                    <citedRange>196-197</citedRange>
                </bibl>.</p>
            <p xml:id="d39_p4">Royal ordinances/decrees are usually delivered on the initiative of the king <bibl>
                    <author>Lenger</author>
                    <ptr target="gramm:lenger1980"/>
                    <citedRange>xxiii</citedRange>
                </bibl>, and concern a wide range of issues, e.g. taxation [<ref target="https://papyri.info/ddbdp/p.col;4;120" type="external">2300</ref>
                229-228 BCE, Alexandria], registration of slaves [<ref target="https://papyri.info/ddbdp/sb;5;8008" type="external">5705</ref> col.ii,
                262-260 BCE], or amnesty [<ref target="https://papyri.info/ddbdp/bgu;4;1185" type="external">5554</ref> 61-60 BCE, Herakleopolite nome]. A long roll found in
                the archive of
                Menches <bibl>
                    <author>TM Arch 140</author>
                    <ptr target="gramm:vandorpe2012"/>
                </bibl>, village scribe of Kerkeosiris, is a copy of a number of decrees
                of Ptolemy VIII Euergetes II and his queens Cleopatra II and III, the first of which
                grants an amnesty after recent unrest, followed by others covering land
                administration, tax fraud, and official corruption [<ref target="https://papyri.info/ddbdp/p.tebt;1;5" type="external">2938</ref> 118
                BCE, Kerkeosiris], see <bibl>
                    <author>Bagnall and Derow</author>
                    <ptr target="gramm:bagnallDerow2004"/>
                    <citedRange>no.54</citedRange>
                </bibl>.</p>
            <p xml:id="d39_p5">Many of the extant decrees survive as copies embedded in other texts,
                e.g. [<ref target="https://papyri.info/ddbdp/p.tebt;3.1;700" type="external">5311</ref> l.22-55, 124 BCE, Tebtunis] included as part of the documentation
                for the sale of some land, or as an attachment to a letter [<ref target="https://papyri.info/ddbdp/p.yale;1;56" type="external">5540</ref> 100
                BCE], or as an extract or allusion, e.g. [<ref target="https://papyri.info/ddbdp/sb;20;15113" type="external">8125</ref> l.9,
                146 BCE, Herakleopolite nome]. Three decrees form part of a dossier in support of a
                lawsuit [<ref target="https://papyri.info/ddbdp/sb;6;9454_1" type="external">5775</ref>].</p>
            <div xml:id="d39_div1_structure" type="section" n="1">
                <head xml:id="structure">Structure</head>
                <p xml:id="d39_p6">The introduction can be presented as a simple statement: </p>
                <p xml:id="d39_p7">
                    <seg xml:id="d39_s3" type="syntax" corresp="../authority-lists.xml#introduction">[<term xml:lang="grc">βασιλέως προστάξαντος</term>]</seg>
                    <gloss corresp="#d39_s3">by order of the king</gloss> [<ref corresp="#d39_s3" target="https://papyri.info/ddbdp/sb;6;9454_2" type="external">5775_2</ref> 270-269 BCE, Gurob], or the king and queen
                        [<ref type="internal" target="3643.xml">3643</ref> 114 BCE, Tebtunis; <ref type="internal" target="4813.xml">4813</ref> 50-49 BCE, Herakleopolite
                    nome].</p>
                <p xml:id="d39_p8">Some decrees may follow this statement with the names of the
                    person responsible for the transmission of the decree at local level, and his
                    agent: </p>
                <p xml:id="d39_p9">
                    <seg xml:id="d39_s1" type="syntax" corresp="../authority-lists.xml#introduction">[<hi rend="italic">name</hi>
                        &lt;nom.&gt;] <term xml:lang="grc">ἀναγγείλαντος</term> [<term xml:lang="grc">παρ̣ά</term> from <hi rend="italic">name</hi>
                        &lt;gen.&gt;]</seg>
                    <gloss corresp="#d39_s1">announced by <hi rend="italic">N</hi> in the name of <hi rend="italic">N</hi>
                    </gloss> [<ref target="https://papyri.info/ddbdp/sb;6;9454_2" type="external" corresp="#d39_s1">5775_2</ref>], or this information may be placed before
                    the opening statement [<ref target="https://papyri.info/ddbdp/sb;6;9454_1" type="external">5775_1</ref>], see <bibl>
                        <author>Lenger</author>
                        <ptr target="gramm:lenger1980"/>
                        <citedRange>15</citedRange>
                    </bibl>. As well as to the general public, the introduction can be addressed to
                    a specific group, e.g. [<ref target="https://papyri.info/ddbdp/p.col;4;120" type="external">2300</ref>] is aimed specifically at owners or
                    administrators of property.</p>
                <p xml:id="d39_p10">This is followed by the main text which can occasionally be
                    quite short [<ref type="internal" target="3643.xml">3643</ref>], after which
                    comes the <seg xml:id="d39_s2" type="syntax" subtype="date" corresp="../authority-lists.xml#date">[date@end]</seg>.</p>
                <p xml:id="d39_p11">After the initial opening, the copies of decrees produced by
                    Menches introduces each new one with <term xml:lang="grc">προστετάχασι δέ</term>
                    <gloss>and they have decreed</gloss> [<ref target="https://papyri.info/ddbdp/p.tebt;1;5" type="external">2938</ref>].</p>
                <p xml:id="d39_p12">One decree is followed with a note by the <term xml:lang="lat">topogrammateus</term> Horos where he indicates that he has publicly
                    displayed it [<ref type="internal" target="4813.xml">4813</ref>], see <bibl>
                        <author>Lenger</author>
                        <ptr target="gramm:lenger1980"/>
                        <citedRange>73, 262</citedRange>
                    </bibl>. On the public posting of official announcements, see <bibl>
                        <author>Jördens</author>
                        <ptr target="gramm:jordens2001"/>
                    </bibl> and <bibl>
                        <author>Schubert</author>
                        <ptr target="gramm:schubert2022a"/>
                    </bibl>.</p>
            </div>
            <div xml:id="d39_div2_format" type="section" n="2">
                <head xml:id="format">Format</head>
                <p xml:id="d39_p13">In most cases the decrees that have survived are copies of an
                    original and as such there is no certainty regarding the format and layout. The
                    surviving versions were recorded in <term xml:lang="lat">pagina</term> format
                    written with the vertical fibres [<ref type="internal" target="2299.xml">2299</ref>; <ref target="https://papyri.info/ddbdp/p.col;4;120" type="external">2300</ref>] or against the horizontal fibres [<ref type="internal" target="4813.xml">4813</ref>], or on a squarish sheet [<ref type="internal" target="3643.xml">3643</ref>]. Some decrees can be found on
                    a roll [<ref target="https://papyri.info/ddbdp/sb;5;8008" type="external">5705</ref>; <ref target="https://papyri.info/ddbdp/p.tebt;1;5" type="external">2938</ref>]. </p>
            </div>
            <div xml:id="d39_div3_layout" type="section" n="3">
                <head xml:id="layout">Layout</head>
                <p xml:id="d39_p14">The opening statement often appears on its own line [<ref type="internal" target="2299.xml">2299</ref>; <ref type="internal" target="4813.xml">4813</ref>; <ref target="https://papyri.info/ddbdp/bgu;6;1211" type="external">4527</ref>;
                        <ref target="https://papyri.info/ddbdp/sb;16;12519" type="external">4132</ref> II BCE, Arsinoite nome (two on the same sheet)], sometimes in
                        <term xml:lang="lat">ekthesis</term> to the rest of the text [<ref type="internal" target="2299.xml">2299</ref>; <ref type="internal" target="4813.xml">4813</ref>]. The main text is displayed as a single block;
                    the date separated at the end [<ref type="internal" target="2299.xml">2299</ref>; <ref target="https://papyri.info/ddbdp/p.col;4;120" type="external">2300</ref>; <ref type="internal" target="3643.xml">3643</ref>]. Sometimes
                    there is a small line drawn between the main text and date [<ref type="internal" target="3643.xml">3643</ref>; <ref type="internal" target="4813.xml">4813</ref>] and between two decrees [<ref target="https://papyri.info/ddbdp/sb;16;12519" type="external">4132</ref>].
                    Decrees can be displayed in columns [<ref target="https://papyri.info/ddbdp/bgu;4;1185" type="external">5554</ref>;
                        <ref target="https://papyri.info/ddbdp/sb;5;8008" type="external">5705</ref>] (two columns); the copies of decrees found in the Menches archive
                    run to an impressive ten columns [<ref target="https://papyri.info/ddbdp/p.tebt;1;5" type="external">2938</ref>].</p>
            </div>
        </body>
    </text>
</TEI>