<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="../schema/gramm.rng"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="d38">
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title>Description of Greek Documentary Papyri: Registration</title>
                <author corresp="../authority-lists.xml#SF">
                    <forename>Susan</forename>
                    <surname>Fogarty</surname>
                </author>
                <author corresp="../authority-lists.xml#PS">
                    <forename>Paul</forename>
                    <surname>Schubert</surname>
                </author>
                <respStmt>
                    <resp>Encoded in TEI P5 by</resp>
                    <name corresp="../authority-lists.xml#EN">
                        <forename>Elisa</forename>
                        <surname>Nury</surname>
                    </name>
                </respStmt>
                <respStmt>
                    <resp>With the collaboration of</resp>
                    <name corresp="../authority-lists.xml#LF">
                        <forename>Lavinia</forename>
                        <surname>Ferretti</surname>
                    </name>
                </respStmt>
                <funder>
                    <orgName key="SNF">Swiss National Science Foundation</orgName> - <ref target="https://data.snf.ch/grants/grant/182205">Project 182205</ref>
                </funder>
            </titleStmt>
            <publicationStmt>
                <authority>Grammateus Project</authority>
                <idno type="DOI">10.26037/yareta:yv2untrmibaxtagh46r5waeq4u</idno>
                <availability>
                    <licence target="https://creativecommons.org/licenses/by-nc/4.0/">Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)</licence>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <p>Born digital document. Encoded from a Microsoft Word document.</p>
            </sourceDesc>
        </fileDesc>
        <encodingDesc>
            <classDecl>
                <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:tei="http://www.tei-c.org/ns/1.0" href="../grammateus_taxonomy.xml"/>
            </classDecl>
            <p>This file is encoded to comply with TEI Guidelines P5, Version 4.0.0.
                    <ref target="https://tei-c.org/guidelines/p5/">https://tei-c.org/guidelines/p5/</ref>
            </p>
        </encodingDesc>
        <profileDesc>
            <langUsage>
                <language ident="lat">Latin</language>
                <language ident="grc">Greek</language>
            </langUsage>
            <textClass>
                <catRef n="type" scheme="#grammateus_taxonomy" target="#gt_ti"/>
                <catRef n="subtype" scheme="#grammateus_taxonomy" target="#gt_statement"/>
                <catRef n="variation" scheme="#grammateus_taxonomy" target="#gt_registration"/>
            </textClass>
        </profileDesc>
        <revisionDesc>
            <change who="../authority_lists.xml#EN" when="2023">Archived on Yareta</change>
            <change who="../authority-lists.xml#EN" when="2022-04-28">TEI file created</change>
            <change who="../authority-lists.xml#PS ../authority-lists.xml#SF" when="2022-12-14">Word
                document</change>
        </revisionDesc>
    </teiHeader>
    <text>
        <body>
            <head>Registration</head>
            <p xml:id="d38_p1">A document of registration is an application to publicly register a
                private action or event. As with a [<ref type="descr" target="#gt_contractProposal">proposal to contract</ref>] a registration is styled as a request and as such
                is different to a formal [<ref type="descr" target="#gt_decl">declaration</ref>].
                These registrations are Roman period documents spanning I – III CE. The same
                typology is used for a variety of content such as applications to register a birth
                or an <term xml:lang="lat">ephebe</term> [<ref type="internal" target="21779.xml">21779</ref> 209 CE, (a 3 year old); <ref type="internal" target="22242.xml">22242</ref> 291 CE, (a 13 year old), both Oxyrhynchus], registration of an
                apprenticeship [<ref type="internal" target="79360.xml">79360</ref> 70 CE,
                Oxyrhynchus], the transfer of property [<ref type="internal" target="18694.xml">18694</ref> 224 CE, Herakleopolite nome (land); <ref target="https://papyri.info/ddbdp/stud.pal;13;pg1/2" type="external">14000</ref>
                124 CE, Arsinoite nome (a slave)], registration of a mortgage [<ref target="https://papyri.info/ddbdp/p.kron;;18" type="external">11539</ref> 143
                CE, Tebtunis]. There is also an example with this typology of an application for the
                    <term xml:lang="lat">anakrisis</term> of a slave [<ref target="https://papyri.info/ddbdp/p.oxy;55;3784" type="external">22508</ref>
                227/8–281/2 CE, Oxyrhynchus]. These documents are addressed to various officials
                depending on the process required. On the registration of a birth or <term xml:lang="lat">ephebe</term>, see <bibl>
                    <author>Cohen</author>
                    <ptr target="gramm:cohen1996"/>
                </bibl> and <bibl>
                    <author>Kruit</author>
                    <ptr target="gramm:kruit1998"/>
                    <citedRange>esp. 38 n.3 for amended list</citedRange>
                </bibl>. On apprenticeships, see <bibl>
                    <author>Forselv</author>
                    <ptr target="gramm:forselv1998"/>
                </bibl>.</p>
            <div xml:id="d38_div1_structure" type="section" n="1">
                <head xml:id="structure">Structure</head>
                <p xml:id="d38_p2">The document opens with the standard hypomnematic address: <seg xml:id="d38_s1" type="syntax" corresp="../authority-lists.xml#introduction">[to <hi rend="italic">name</hi> &lt;dat&gt;][from <term xml:lang="grc">παρά</term>
                        <hi rend="italic">name</hi> &lt;gen&gt;]</seg> and follows with
                <seg xml:id="d38_s3" type="syntax" corresp="../authority-lists.xml#main-text">[<term xml:lang="grc">βούλομαι</term> + infinitive]</seg>.</p>
                <p xml:id="d38_p3">All applications to register a birth or an <term xml:lang="lat">ephebe</term> found in Oxyrhynchus have the formulation <term xml:id="d38_t1" corresp="#d38_s3" xml:lang="grc">βούλομαι ἀπογραφῆναι... <hi rend="italic">N</hi>
                    </term> 
                    <gloss corresp="#d38_t1">I wish that <hi rend="italic">N</hi> be registered</gloss> [<ref target="https://papyri.info/ddbdp/p.oxy;3;479" type="external" corresp="#d38_s3 #d38_t1">20615</ref>
                    157 CE] or <term xml:lang="grc" xml:id="d38_t2" corresp="#d38_s3">βούλομαι ἀπογράψασθαι</term>
                    <gloss corresp="#d38_t2">I wish to register</gloss> [<ref type="internal" target="22242.xml" corresp="#d38_s3 #d38_t2">22242</ref>].
                    Documents from the Arsinoite nome during the II-III CE make a more formal
                    declaration, similar to a [<ref type="descr" target="#gt_decl_census">census</ref>] declaration, with <term xml:lang="grc">ἀπογράφομαι</term>
                    <gloss>I register</gloss>, e.g. [<ref target="https://papyri.info/ddbdp/stud.pal;22;18" type="external">15094</ref> 148-149 CE, Soknopaiou Nesos].</p>
                <p xml:id="d38_p4">There can be a number of different addressees, reflecting the
                    rapid administrative changes of the period, e.g. to the secretary of the city
                        <term xml:lang="grc">(γραμματεὺς πόλεως</term>) [<ref type="internal" target="22245.xml">22245</ref> 171 CE] or of the local area (<term xml:lang="grc">ἀμφοδογραμματεύς</term>) [<ref type="internal" target="21779.xml">21779</ref>], to the <term xml:lang="lat">phylarch</term>
                        (<term xml:lang="grc">φυλάρχης</term>) [<ref target="https://papyri.info/ddbdp/psi;12;1257" type="external">17417</ref>
                    249-250 CE] or administrators of the office of the <term xml:lang="lat">phylarch</term> [<ref target="https://papyri.info/ddbdp/p.oxy;46;3295" type="external">15760</ref> 285 CE], to the board of the <term xml:lang="grc">λαόγραφος (κοινόν τῶν λαογράφων)</term> [<ref type="internal" target="22242.xml">22242</ref>], or to the <term xml:lang="lat">systates</term> (<term xml:lang="grc">συστάτης</term>) [<ref type="internal" target="20910.xml">20910</ref> 291 CE] or board of <term xml:lang="lat">systates</term> (<term xml:lang="grc">κοινόν τῶν
                        συστατῶν</term>) [<ref target="https://papyri.info/ddbdp/p.oxy;43;3137" type="external">16016</ref> 295 CE]. </p>
                <p xml:id="d38_p5">For the registration of an apprenticeship the phrase is <term xml:id="d38_t3" corresp="#d38_s3" xml:lang="grc">βούλομαι ἐκδόσθαι</term>
                    <gloss corresp="#d38_t3">I wish to apprentice (lit. hand over)</gloss> and the addressee is the tax farmer
                    concerned with the trade, e.g. [<ref type="internal" target="21335.xml" corresp="#d38_s3 #d38_t3">21335</ref> 62 CE Oxyrhynchus] an apprentice weaver.</p>
                <p xml:id="d38_p6">Documents with applications to register the transfer of
                    ownership of a property have <term xml:lang="grc" corresp="#d38_s3" xml:id="d38_t4">βούλομαι ἐξοικονομῆσαι</term>
                    <gloss corresp="#d38_t4">I wish to dispose of</gloss> [<ref type="internal" target="18694.xml" corresp="#d38_s3 #d38_t4">18694</ref>; <ref target="https://papyri.info/ddbdp/stud.pal;13;pg1/2" corresp="#d38_s3 #d38_t4" type="external">14000</ref>]. For the cession of catoecic land the phrase
                        is <term xml:lang="grc" corresp="#d38_s3" xml:id="d38_t5">βούλομαι παραχωρῆσαι</term>
                <gloss corresp="#d38_t5">I wish to cede</gloss> [<ref type="internal" target="11682.xml" corresp="#d38_s3 #d38_t5">11682</ref> 129 CE, Arsinoite nome]. Registrations of a mortgage have <term xml:lang="grc" corresp="#d38_s3" xml:id="d38_t6">βούλομαι ὑπαλλάξαι</term>
                <gloss corresp="#d38_t6">I wish to mortgage</gloss> [<ref target="https://papyri.info/ddbdp/p.kron;;18" corresp="#d38_s3 #d38_t6" type="external">11539</ref>, in this case the mortgage of some catoecic
                land] or [<ref type="internal" target="13718.xml" corresp="#d38_s3 #d38_t6">13718</ref> 116 CE, Ptolemais
                    Euergetis, the mortgage of a slave]. Some of these registrations may begin the
                    main text with a relative clause indicating the object to be registered, before
                        <term xml:lang="grc">βούλομαι</term> + infinitive, e.g. [<ref type="internal" target="27757.xml">27757</ref> II CE, Arsinoite nome; <ref type="internal" target="9142.xml">9142</ref> 67 CE, Karanis]. </p>
                <p xml:id="d38_p7">The <seg xml:id="d38_s2" type="syntax" corresp="../authority-lists.xml#date">[date@end]</seg> invariably follows the
                    main text. Subscriptions may be appended underneath by the applicants [<ref type="internal" target="22242.xml">22242</ref>; <ref type="internal" target="27757.xml">27757</ref>]; an official may subscribe a signature [<ref type="internal" target="21335.xml">21335</ref>] or deliver an instruction
                        [<ref type="internal" target="9142.xml">9142</ref>]. Official signatures may
                    also be found at the top of the sheet e.g. [<ref type="internal" target="11682.xml">11682</ref>; <ref target="https://papyri.info/ddbdp/stud.pal;13;pg1/2" type="external">14000</ref>; <ref target="https://papyri.info/ddbdp/p.kron;;18" type="external">11539</ref>].</p>
            </div>
            <div xml:id="d38_div2_format" type="section" n="2">
                <head xml:id="format">Format</head>
                <p xml:id="d38_p8">These Roman period documents are in <term xml:lang="lat">pagina</term> format with the writing along the fibres. A registration of
                    an apprentice is particularly tall and narrow [<ref type="internal" target="21335.xml">21335</ref> H. 36.5 x W. 8.5cm].</p>
            </div>
            <div xml:id="d38_div3_layout" type="section" n="3">
                <head xml:id="layout">Layout</head>
                <p xml:id="d38_p9">The text is laid out on the sheet in a single block; the <term xml:lang="grc">παρά</term> clause always begins a new line sometimes with an
                    enlarged first letter [<ref type="internal" target="13718.xml">13718</ref>] or
                    in <term xml:lang="lat">ekthesis</term> [<ref type="internal" target="22242.xml">22242</ref>]. The second line of the addressee may be indented before the
                        <term xml:lang="grc">παρά</term> clause [<ref type="internal" target="11682.xml">11682</ref>; <ref type="internal" target="13718.xml">13718</ref>], or there can be a space between the two [<ref type="internal" target="18694.xml">18694</ref>]. There can be a stroke [<ref type="internal" target="9142.xml">9142</ref>] or a space [<ref type="internal" target="18694.xml">18694</ref>] after the main text, and before the
                    subscription [<ref type="internal" target="20011.xml">20011</ref> 287 CE,
                    Oxyrhynchus]. The subscriptions in [<ref type="internal" target="18694.xml">18694</ref>] run into a second column. The registration of the slave in
                        [<ref type="internal" target="13718.xml">13718</ref>] was later cancelled by
                    a series of crosses over the text, i.e. <term xml:lang="grc">κεχιασμένον</term>
                    <gloss>marked with a <term xml:lang="lat">chi</term>
                    </gloss>.</p>
            </div>
        </body>
    </text>
</TEI>