<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="../schema/gramm.rng"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="d34">
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title>Description of Greek Documentary Papyri: Proclamation</title>
                <author corresp="../authority-lists.xml#SF">
                    <forename>Susan</forename>
                    <surname>Fogarty</surname>
                </author>
                <author corresp="../authority-lists.xml#PS">
                    <forename>Paul</forename>
                    <surname>Schubert</surname>
                </author>
                <respStmt>
                    <resp>Encoded in TEI P5 by</resp>
                    <name corresp="../authority-lists.xml#EN">
                        <forename>Elisa</forename>
                        <surname>Nury</surname>
                    </name>
                </respStmt>
                <respStmt>
                    <resp>With the collaboration of</resp>
                    <name corresp="../authority-lists.xml#LF">
                        <forename>Lavinia</forename>
                        <surname>Ferretti</surname>
                    </name>
                </respStmt>
                <funder>
                    <orgName key="SNF">Swiss National Science Foundation</orgName> - <ref target="https://data.snf.ch/grants/grant/182205">Project 182205</ref>
                </funder>
            </titleStmt>
            <publicationStmt>
                <authority>Grammateus Project</authority>
                <idno type="DOI">10.26037/yareta:dvr7tlrbqfgklpicyq2b7uavh4</idno>
                <availability>
                    <licence target="https://creativecommons.org/licenses/by-nc/4.0/">Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)</licence>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <p>Born digital document. Encoded from a Microsoft Word document.</p>
            </sourceDesc>
        </fileDesc>
        <encodingDesc>
            <classDecl>
                <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:tei="http://www.tei-c.org/ns/1.0" href="../grammateus_taxonomy.xml"/>
            </classDecl>
            <p>This file is encoded to comply with TEI Guidelines P5, Version 4.0.0.
                    <ref target="https://tei-c.org/guidelines/p5/">https://tei-c.org/guidelines/p5/</ref>
            </p>
        </encodingDesc>
        <profileDesc>
            <langUsage>
                <language ident="lat">Latin</language>
                <language ident="grc">Greek</language>
            </langUsage>
            <textClass>
                <catRef n="type" scheme="#grammateus_taxonomy" target="#gt_os"/>
                <catRef n="subtype" scheme="#grammateus_taxonomy" target="#gt_announcement"/>
                <catRef n="variation" scheme="#grammateus_taxonomy" target="#gt_proclamation"/>
            </textClass>
        </profileDesc>
        <revisionDesc>
            <change who="../authority_lists.xml#EN" when="2023">Archived on Yareta</change>
            <change who="../authority-lists.xml#EN" when="2022-04-25">TEI file created</change>
            <change who="../authority-lists.xml#PS ../authority-lists.xml#SF" when="2022-11-22">Word
                document</change>
        </revisionDesc>
    </teiHeader>
    <text>
        <body>
            <head>Proclamation</head>
            <p xml:id="d34_p1">A proclamation is a formal announcement, distinct from an [<ref type="descr" target="#gt_edict">edict</ref>] or a [<ref type="descr" target="#gt_decree">royal ordinance</ref>], and can be issued by high ranking
                officials of the administration and judiciary; some of these documents revolve
                around the verb <term xml:lang="grc">παραγγέλλω</term>
                <gloss>transmit a message/give an order</gloss>. As with all announcements, they were
                publicly posted to be seen by all, see <listBibl>
                    <bibl>
                        <author>Jördens</author>
                        <ptr target="gramm:jordens2001"/>
                    </bibl>
                    <bibl>
                        <author>Schubert</author>
                        <ptr target="gramm:schubert2022a"/>
                    </bibl>
                </listBibl>. The term for a proclamation from the <term xml:lang="lat">strategos</term> is <term xml:lang="grc">παράγγελμα</term>, but there are few
                references to this word, see <bibl>
                    <author>Stroppa</author>
                    <ptr target="gramm:stroppa2017"/>
                    <citedRange>1-2</citedRange>
                </bibl>. Announcements by the <term xml:lang="lat">strategos</term> also fall under
                the general term <term xml:lang="grc">πρόγραμμα</term>, regardless of their content <listBibl>
                    <bibl>
                        <author>Stroppa</author>
                        <ptr target="gramm:stroppa2017"/>
                        <citedRange>2</citedRange>
                    </bibl>
                    <bibl>
                        <author>Stroppa</author>
                        <ptr target="gramm:stroppa2004"/>
                    </bibl>
                </listBibl>. However, this same word can also have a more specific meaning as a
                stage in the nomination to a liturgy where the notice of appointment to a liturgy is
                confirmed through a <term xml:lang="grc">πρόγραμμα</term>; some of these may also
                include the verb <term xml:lang="grc">παραγγέλλω</term>, see [<ref type="descr" target="#gt_programma">approval of nomination</ref>]. </p>
            <p xml:id="d34_p2">Other proclamations are orders or judicial summonses, e.g. to appear
                before the <term xml:lang="lat">strategos</term> to answer a charge [<ref type="internal" target="5699.xml">5699</ref> 47 BCE, Herakleopolite nome], or to
                seize a fugitive slave [<ref type="external" target="https://papyri.info/ddbdp/bgu;8;1774">4855</ref> 64-44 BCE,
                Herakleopolite nome]; the term for such a summons is <term xml:lang="grc">παραγγελία</term>, <bibl>
                    <author>Schäfer</author>
                    <ptr target="gramm:schafer1933"/>
                </bibl>.</p>
            <p xml:id="d34_p3">Many of the extant self-contained proclamations come from III CE
                Oxyrhynchus. Proclamations can be embedded in other types of documents, usually
                    [<ref type="descr" target="#gt_circular">circular letters</ref>], ensuring the
                announcement can reach more than one official, e.g. [<ref type="internal" target="78769.xml">78769</ref> 111 BCE, Tebtunis]. Some are to be found in the
                register of correspondence of the <term xml:lang="lat">strategos</term> of Panopolis,
                e.g. [<ref type="external" target="https://papyri.info/ddbdp/p.panop.beatty;;1">44881</ref> col.viii. l.205-212]. A copy of a proclamation by a <term xml:lang="lat">iuridicus</term> to register all private stocks of corn is
                followed by a copy of the submitted registration [<ref type="internal" target="16446.xml">16446</ref> 246 CE, Oxyrhynchus].</p>
            <p xml:id="d34_p4">A variety of purposes are served through these announcements – a
                    <term xml:lang="lat">prytanis</term> and a <term xml:lang="lat">strategos</term>
                convene meetings [<ref type="internal" target="21574.xml">21574</ref> 254 CE; <ref type="external" target="https://papyri.info/ddbdp/p.oxy;12;1412">21822</ref> 284
                CE, both Oxyrhynchus]; a <term xml:lang="lat">strategos</term> announces a crop
                survey [<ref type="internal" target="13451.xml">13451</ref> 226 CE, Arsinoite nome],
                or orders banks to use the new coinage [<ref type="internal" target="21821.xml">21821</ref> 260 CE, Oxyrhynchus]. </p>
            <div xml:id="d34_div1_structure" type="section" n="1">
                <head xml:id="structure">Structure</head>
                <p xml:id="d34_p5">A proclamation can open with a simple <seg xml:id="d34_s1" type="syntax" corresp="../authority-lists.xml#introduction">[<hi rend="italic">name</hi> &lt;nom.&gt;]</seg> followed by <seg xml:id="d34_s2" type="syntax" corresp="../authority-lists.xml#introduction">[<hi rend="italic">place</hi>]</seg> indicating the authority issuing
                    the order [<ref type="internal" target="21821.xml" corresp="#d34_s1 #d34_s2">21821</ref>]; one <term xml:lang="lat">prytanis</term> cites an impressive
                    array of former titles [<ref target="https://papyri.info/ddbdp/p.oxy;12;1412" type="external">21822</ref>]. This is followed immediately by the main text
                    and a <seg xml:id="d34_s3" type="syntax" corresp="../authority-lists.xml#date">[date@end]</seg>; the <term xml:lang="lat">prytanis</term> further endorses the
                    proclamation with <term xml:lang="grc">ἐσημειωσάμην</term>
                    <gloss>I have signed</gloss> [<ref type="internal" target="21821.xml" corresp="#d34_s1 #d34_s2">21821</ref> l.20]; see also [<ref type="internal" target="21574.xml">21574</ref> l.20].</p>
                <p xml:id="d34_p6">Other proclamations follow the opening <seg xml:id="d34_s4" type="syntax" corresp="../authority-lists.xml#introduction">[<hi rend="italic">name</hi> &lt;nom.&gt;]</seg> and <seg xml:id="d34_s5" type="syntax" corresp="../authority-lists.xml#introduction">[place]</seg> with <seg type="syntax" corresp="../authority-lists.xml#main-text" xml:id="d34_s7">[<term xml:lang="grc">παραγγέλλεται</term>]</seg>
                    <gloss corresp="#d34_s7">it is proclaimed/announced</gloss> and continue with the main text [<ref type="internal" target="13451.xml" corresp="#d34_s4 #d34_s5">13451</ref>].</p>
                <p xml:id="d34_p7">Some III CE proclamations do not contain the verb <term xml:lang="grc">παραγγέλλω</term> but open the announcement with <seg xml:id="d34_s6" type="syntax" corresp="../authority-lists.xml#main-text">
                        <term xml:lang="grc">ἐξ αὐθεντείας</term> [<hi rend="italic">name</hi>
                        &lt;gen.&gt;]</seg>
                    <gloss corresp="#d34_s6">on the authority of <hi rend="italic">N</hi>
                    </gloss> followed
                    by a title [<ref type="internal" target="16446.xml" corresp="#d34_s6">16446</ref>, a <term xml:lang="lat">iuridicus</term>; <ref type="internal" target="16869.xml" corresp="#d34_s6">16869</ref> 245-249 CE, Oxyrhynchus, a
                        <term xml:lang="lat">rationalis</term> and <term xml:lang="lat">procurator</term>]. The editor of [<ref type="internal" target="16869.xml">16869</ref>] states that <term xml:lang="grc">ἐξ αὐθεντείας</term>
                    <quote>looks like a latinism</quote> (note to line 1); this is borne out by line 9,
                    where <term xml:lang="grc">Ῥωμαικά</term> suggests that the original
                    proclamation was posted in Latin. The same opening is also found in the copies
                    of proclamations in the register of correspondence of the Panopolite <term xml:lang="lat">strategos</term> [<ref target="https://papyri.info/ddbdp/p.panop.beatty;;2" type="external">44882</ref> col.iv. l.92-97, col.vi. l.156-160, col.ix. l.222-244]. Documents with
                    this opening carry an order to post the proclamation in public, between the main
                    text and the date, <term xml:lang="grc">προθές</term>
                    <gloss>post</gloss>.</p>
                <p xml:id="d34_p8">A summons can open with the more general <term xml:lang="grc">διὰ προγράμματος</term>
                    <gloss>through/by way of public notice</gloss> [<ref type="internal" target="5699.xml">5699</ref>; <ref type="external" target="https://papyri.info/ddbdp/sb;5;7610">5700</ref> 51 BCE,
                    Herakleopolite nome].</p>
            </div>
            <div xml:id="d34_div2_format" type="section" n="2">
                <head xml:id="format">Format</head>
                <p xml:id="d34_p9">In most cases the proclamations that have survived are copies of
                    an original and as such there is no certainty regarding the format and layout.
                    These copies are written in <term xml:lang="lat">pagina</term> format along the
                    fibres [<ref type="internal" target="13451.xml">13451</ref>; <ref type="internal" target="21821.xml">21821</ref>]; or horizontally oriented
                    with the writing along the fibres [<ref type="internal" target="16869.xml">16869</ref>; <ref type="internal" target="31285.xml">31285</ref>]. One
                    notice is written on a narrow piece of papyrus [<ref type="internal" target="21574.xml">21574</ref> H. 21.5 x W. 7.2cm] with a <term xml:lang="lat">kollesis</term> and change of fibre direction.</p>
            </div>
            <div xml:id="d34_div3_layout" type="section" n="3">
                <head xml:id="layout">Layout</head>
                <p xml:id="d34_p10">Most documents appear to be written as a single block of text
                    with no differentiation between the opening and the main text. One example
                    places the opening in <term xml:lang="lat">ekthesis</term> to the rest of the
                    text [<ref type="internal" target="31285.xml">31285</ref>]; on another the
                    scribe has written the second line of the opening in <term xml:lang="lat">eisthesis</term> to the rest of the text [<ref type="internal" target="21574.xml">21574</ref>]. There can be a space between the end of the
                    text and the date [<ref type="internal" target="13451.xml">13451</ref>]. Some
                    documents may have an indented transitional clause before a list [<ref type="internal" target="16869.xml">16869</ref>; <ref type="internal" target="31285.xml">31285</ref>].</p>
            </div>
        </body>
    </text>
</TEI>