xquery version "3.1";

import module namespace gramm="http://grammateus.ch/gramm" at "modules/gramm.xql";
import module namespace URI="http://grammateus.ch/URI" at "modules/URI.xql";

declare namespace tei = "http://www.tei-c.org/ns/1.0";



for $doc in collection($URI:papyri)
let $hgv_data := gramm:doc-expand-keywords($doc)

return
    (: If HGV keywords exists :)
    if ($hgv_data//tei:keywords[@scheme eq "hgv"]) then
        update replace $doc//tei:keywords[@scheme eq "hgv"] with $hgv_data//tei:keywords[@scheme eq "hgv"]
    (: If HGV keywords does not exist, then log that info somewhere :)
    else(util:log('WARN', concat('HGV keywords could not be found for ', $doc//tei:idno[@type eq 'TM'])))

