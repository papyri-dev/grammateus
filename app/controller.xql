xquery version "3.0";

import module namespace x="http://grammateus.ch/pages/xmlpage" at "modules/pages/xmlpage.xql";
import module namespace g="http://grammateus.ch/global" at "modules/global.xql";


declare variable $exist:path external;
declare variable $exist:resource external;
declare variable $exist:controller external;
declare variable $exist:prefix external;
declare variable $exist:root external;


if ($exist:path eq "/" or starts-with(lower-case($exist:path), "/home") 
			or starts-with(lower-case($exist:path), "/index")) then
	<dispatch xmlns="http://exist.sourceforge.net/NS/exist">
	    <forward url="{$exist:controller}/modules/page.xql">
	     	<add-parameter name="pagetype" value="home"/>
	     	<add-parameter name="resource" value="{$exist:resource}"/>
	    </forward>
	</dispatch>
	
else if ($exist:path eq "/" or starts-with(lower-case($exist:path), "/new")) then
	<dispatch xmlns="http://exist.sourceforge.net/NS/exist">
	    <forward url="{$exist:controller}/modules/page.xql">
	     	<add-parameter name="pagetype" value="newhome"/>
	     	<add-parameter name="resource" value="{$exist:resource}"/>
	    </forward>
	</dispatch>
	
else if (starts-with(lower-case($exist:path), "/introduction/general") or starts-with(lower-case($exist:path), "/introduction/concepts")) then
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <forward url="{$exist:controller}/modules/page.xql">
        	<add-parameter name="pagetype" value="introduction"/>
        	<add-parameter name="resource" value="{$exist:resource}"/>
        </forward>
    </dispatch>

else if (starts-with(lower-case($exist:path), "/typology")) then
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <forward url="{$exist:controller}/modules/page.xql">
        	<add-parameter name="pagetype" value="typology"/>
        	<add-parameter name="resource" value="{$exist:resource}"/>
        </forward>
    </dispatch>    

else if (starts-with(lower-case($exist:path), "/class")) then
    if (doc-available(x:path("classification", $exist:resource))) 
    then
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <forward url="{$exist:controller}/modules/page.xql">
        	<add-parameter name="pagetype" value="classification"/>
        	<add-parameter name="resource" value="{$exist:resource}"/>
        </forward>
    </dispatch>
    else
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
	    <forward url="{$exist:controller}/modules/page.xql">
	    	<add-parameter name="pagetype" value="error"/>
	    	<add-parameter name="resource" value="{$exist:resource}"/>
	    	<add-parameter name="exception" value="nodescr"/>
	    </forward>
	</dispatch>
    
else if (starts-with(lower-case($exist:path), "/descr")) then
    (: either description exists, or it is an "empty set" like statement and the @xml:id exists in the typology :)
    if (doc-available(x:path("descriptions", $exist:resource)) or doc('/db/apps/grammateus/data/grammateus_taxonomy.xml')//id($exist:resource)) 
    then
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <forward url="{$exist:controller}/modules/page.xql">
        	<add-parameter name="pagetype" value="descriptions"/>
        	<add-parameter name="resource" value="{$exist:resource}"/>
        </forward>
    </dispatch>
    else
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
	    <forward url="{$exist:controller}/modules/page.xql">
	    	<add-parameter name="pagetype" value="error"/>
	    	<add-parameter name="resource" value="{$exist:resource}"/>
	    	<add-parameter name="exception" value="nodescr"/>
	    </forward>
	</dispatch>
    
else if (starts-with(lower-case($exist:path), "/papyri.html")) then
	<dispatch xmlns="http://exist.sourceforge.net/NS/exist">
	    <forward url="{$exist:controller}/modules/page.xql">
	   	    <add-parameter name="pagetype" value="papyri"/>
	    </forward>
	</dispatch>
    
else if (starts-with(lower-case($exist:path), "/doc")) then
    (: document as xml resource :)
    if (ends-with(lower-case($exist:path), "/source")) then
        let $papyrus := subsequence(tokenize($exist:path, "/"), 3, 1)
        return
        (: check if the document is in our database :)
        if (doc-available($g:phase1||$papyrus||".xml")) then
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <forward url="{$exist:controller}/data/papyri/phase1/{$papyrus}.xml"></forward>
        </dispatch>
        else if (doc-available($g:byzantine||$papyrus||".xml")) then
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <forward url="{$exist:controller}/data/papyri/byzantine/{$papyrus}.xml"></forward>
        </dispatch>
        else
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <forward url="{$exist:controller}/modules/page.xql">
                <add-parameter name="pagetype" value="error"/>
                <add-parameter name="resource" value="{$papyrus}"/>
                <add-parameter name="exception" value="nodoc"/>
            </forward>
        </dispatch>
    (: document as HTML display :)
    else if (doc-available("/db/apps/grammateus/data/papyri/phase1/"||$exist:resource||".xml"))
    then
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <forward url="{$exist:controller}/modules/page.xql">
        	<add-parameter name="pagetype" value="doc"/>
        	<add-parameter name="resource" value="{$exist:resource}"/>
        </forward>
    </dispatch>
    else if (doc-available("/db/apps/grammateus/data/papyri/byzantine/"||$exist:resource||".xml"))
    then
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <forward url="{$exist:controller}/modules/page.xql">
        	<add-parameter name="pagetype" value="doc"/>
        	<add-parameter name="resource" value="{$exist:resource}"/>
        </forward>
    </dispatch>
    else
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <forward url="{$exist:controller}/modules/page.xql">
            <add-parameter name="pagetype" value="error"/>
            <add-parameter name="resource" value="{$exist:resource}"/>
            <add-parameter name="exception" value="nodoc"/>
        </forward>
    </dispatch>

else if (starts-with(lower-case($exist:path), "/browse")) then
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <forward url="{$exist:controller}/modules/page.xql">
        	<add-parameter name="pagetype" value="browse"/>
        	<add-parameter name="resource" value="{$exist:resource}"/>
        </forward>
    </dispatch>
    
else if (starts-with(lower-case($exist:path), "/syntax")) then
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <forward url="{$exist:controller}/modules/page.xql">
        	<add-parameter name="pagetype" value="syntax"/>
        	<add-parameter name="resource" value="{$exist:resource}"/>
        </forward>
    </dispatch>

else if (starts-with(lower-case($exist:path), "/compare")) then
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <forward url="{$exist:controller}/modules/page.xql">
        	<add-parameter name="pagetype" value="compare"/>
        	<add-parameter name="resource" value="{$exist:resource}"/>
        </forward>
    </dispatch>

else if (starts-with(lower-case($exist:path), "/biblio")) then
	<dispatch xmlns="http://exist.sourceforge.net/NS/exist">
	    <forward url="{$exist:controller}/modules/page.xql">
	   	    <add-parameter name="pagetype" value="biblio"/>
	   	    <add-parameter name="resource" value="{$exist:resource}"/>
	    </forward>
	</dispatch>
	
else if (starts-with(lower-case($exist:path), "/howto-cite")) then
	<dispatch xmlns="http://exist.sourceforge.net/NS/exist">
	    <forward url="{$exist:controller}/modules/page.xql">
	   	    <add-parameter name="pagetype" value="howto-cite"/>
	   	    <add-parameter name="resource" value="{$exist:resource}"/>
	    </forward>
	</dispatch>
	
else if (starts-with(lower-case($exist:path), "/team")) then
	<dispatch xmlns="http://exist.sourceforge.net/NS/exist">
	    <forward url="{$exist:controller}/modules/page.xql">
	   	    <add-parameter name="pagetype" value="team"/>
	   	    <add-parameter name="resource" value="{$exist:resource}"/>
	    </forward>
	</dispatch>
	
else if (starts-with(lower-case($exist:path), "/rights")) then
	<dispatch xmlns="http://exist.sourceforge.net/NS/exist">
	    <forward url="{$exist:controller}/modules/page.xql">
	   	    <add-parameter name="pagetype" value="rights"/>
	   	    <add-parameter name="resource" value="{$exist:resource}"/>
	    </forward>
	</dispatch>
	
else if (starts-with(lower-case($exist:path), "/tutorial")) then
	<dispatch xmlns="http://exist.sourceforge.net/NS/exist">
	    <forward url="{$exist:controller}/modules/page.xql">
	   	    <add-parameter name="pagetype" value="tutorials"/>
	   	    <add-parameter name="resource" value="{$exist:resource}"/>
	    </forward>
	</dispatch>
	
else if (starts-with(lower-case($exist:path), "/resource")) then
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
       <cache-control cache="yes"/>
   </dispatch>
   
else if (starts-with(lower-case($exist:path), "/data")) then
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
       <cache-control cache="yes"/>
   </dispatch>
(: access to export scripts :)
else if (starts-with(lower-case($exist:path), "/export")) then
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
       <cache-control cache="yes"/>
   </dispatch>
(: robots.txt :)
else if ($exist:path eq "/robots.txt") then
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
       <cache-control cache="yes"/>
   </dispatch>
else 
	<dispatch xmlns="http://exist.sourceforge.net/NS/exist">
	    <forward url="{$exist:controller}/modules/page.xql">
	    	<add-parameter name="pagetype" value="error"/>
	    	<add-parameter name="exception" value="nopage"/>
	    </forward>
	</dispatch>
(:else:)
(:    (: everything else is passed through :):)
(:    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">:)
(:        <cache-control cache="yes"/>:)
(:    </dispatch>:)
