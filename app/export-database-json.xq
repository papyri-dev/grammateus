xquery version "3.1";

import module namespace g="http://grammateus.ch/global" at "modules/global.xql";
import module namespace gramm="http://grammateus.ch/gramm" at "modules/gramm.xql";
import module namespace show="http://grammateus.ch/show" at "modules/show.xql";

declare namespace tei = "http://www.tei-c.org/ns/1.0";

(:declare option exist:serialize "method=json media-type=text/javascript";:)
declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare option output:method "json";
declare option output:media-type "application/json";

(: extracting json array for graph plotting with:
 : TM  number
 : width
 : height
 : type
 : subtype
 : place of origin (HGV)
 : date (HGV)
 : fibres direction
 : shape :)
for $doc in collection($g:papyri)//tei:TEI
return
        [$doc//tei:idno[@type eq 'TM']/text(), 
        $doc//tei:width/text(),
        $doc//tei:height/text(),
        $doc//tei:keywords[@n eq 'text-type']/tei:term/text(),
        $doc//tei:keywords[@n eq 'text-subtype']/tei:term/text(),
        $doc//tei:origPlace//text(),
        show:date($doc),
        gramm:fibres($doc),
        gramm:shape($doc//tei:width/text() div $doc//tei:height/text())]