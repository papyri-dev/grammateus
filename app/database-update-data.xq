xquery version "3.1";

import module namespace config="http://grammateus.ch/config" at "modules/config.xqm";
import module namespace g="http://grammateus.ch/global" at "modules/global.xql";

(: update database with latest data - adapted for byzantine workflow :)

 
(: remove all byzantine papyri and create empty 'papyri' folder :)
let $del := xmldb:remove("/db/apps/grammateus/data/papyri/byzantine")
let $add := xmldb:create-collection("/db/apps/grammateus/data/papyri", "byzantine")

(: Update authority lists + external links + taxonomy :)
let $updated_files_1 := xmldb:store-files-from-pattern($config:data-root, "C:\Users\nury\Documents\grammateus\data", "*.xml", "text/xml", true(), "list_*")

(: Update papyri files :)
let $updated_files_2 := xmldb:store-files-from-pattern($g:byzantine, "C:\Users\nury\Documents\grammateus\data\papyri\byzantine", "*.xml")



return $updated_files_2