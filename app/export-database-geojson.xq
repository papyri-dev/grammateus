xquery version "3.1";

import module namespace g="http://grammateus.ch/global" at "modules/global.xql";
import module namespace gramm="http://grammateus.ch/gramm" at "modules/gramm.xql";
import module namespace show="http://grammateus.ch/show" at "modules/show.xql";
import module namespace functx="http://www.functx.com";
import module namespace console="http://exist-db.org/xquery/console";

declare namespace tei = "http://www.tei-c.org/ns/1.0";

(:declare option exist:serialize "method=json media-type=text/javascript";:)
declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare option output:method "json";
declare option output:media-type "application/json";


declare function local:get-century($date) {
    let $year := year-from-date(xs:date($date))
    let $century :=
        if((0001 <= $year and $year <= 0100) or (-0100 <= $year and $year <= -0000)) then
            '1'
        else if((0101 <= $year and $year <= 0200) or (-0200 <= $year and $year <= -0101)) then
            '2'
        else if((0201 <= $year and $year <= 0300) or (-0300 <= $year and $year <= -0201)) then
            '3'
        else if((0301 <= $year and $year <= 0400) or (-0400 <= $year and $year <= -0301)) then
            '4'
        else if((0401 <= $year and $year <= 0500) or (-0500 <= $year and $year <= -0401)) then
            '5'
        else if((0501 <= $year and $year <= 0600) or (-0600 <= $year and $year <= -0501)) then
            '6'
        else if((0601 <= $year and $year <= 0700) or (-0700 <= $year and $year <= -0601)) then
            '7'
        else if((0701 <= $year and $year <= 0800) or (-0800 <= $year and $year <= -0701)) then
            '8'
        else('error!')
    return $century
};
    
declare function local:get-era-bce($date) {
    let $era := if ($date < xs:date("0001-01-01")) then 'BCE' else 'CE'
    return $era
};

declare function local:get-era-minus($date) {
    let $era := if ($date < xs:date("0001-01-01")) then '-' else ()
    return $era
};

declare function local:pleiades($links) {
    for $l in $links
    return 
        if(matches($l, "pleiades")) then $l 
        (: provisional links until corrected in HGV and/or pleiades
        Hermopolites
        Heliopolites
        Diospolites Kato
        Apollonopolites Heptakomias
        Peri Thebas:)
        else if (matches($l, "place/2720$")) then "https://pleiades.stoa.org/places/756575"
        else if (matches($l, "place/3088$")) then "https://pleiades.stoa.org/places/727118"
        else if (matches($l, "place/3224$")) then "https://pleiades.stoa.org/places/727112"
        else if (matches($l, "place/1690$")) then "https://pleiades.stoa.org/places/786131"
        else ()
};

declare function local:tm($links) {
    for $l in $links
    return 
        if(matches($l, "trismegistos")) then $l else ()
};

declare function local:coord_settlement($settlement) {
    let $links := $settlement/@ref/string() => tokenize(" ")
    let $pleiades := local:pleiades($links)
    return 
        if ($pleiades)
        then
            let $doc := json-doc($pleiades||"/json")
            return map:get($doc, "reprPoint")
        else ()
};

declare function local:coord_nome($nome) {
    let $links := $nome/@ref/string() => tokenize(" ")
    let $pleiades := local:pleiades($links)
    return 
        if ($pleiades)
        then
            let $doc := json-doc($pleiades||"/json")
            return map:get($doc, "reprPoint")
        else ()
};

(: geojson data for peripleo :)
map{
    "@id": "https://gitlab.unige.ch/Adrien.Dejean/application-info",
    "type": "FeatureCollection",
    "@context": "http://linkedpasts.org/assets/linkedplaces-context-v1.jsonld",
    "features": 
        for $prov in collection($g:papyri)//tei:provenance | 
                     collection($g:papyri)//tei:origPlace[not(ancestor::tei:TEI//tei:provenance)]
        (: identifiers :)
        let $doc := doc(base-uri($prov)) 
        let $idno := base-uri($prov)
                     => functx:substring-after-last("/") 
                     => substring-before(".xml")
        let $idno_tm := gramm:tm($doc)
        let $idno_hgv := gramm:hgv($doc)
        
        (: typology :)
        let $f_type := gramm:get-catDesc($doc//tei:catRef[@n eq "format-type"])/normalize-space()
        let $f_subtype := gramm:get-catDesc($doc//tei:catRef[@n eq "format-subtype"])/normalize-space()
        let $f_variation := gramm:get-catDesc($doc//tei:catRef[@n eq "format-variation"])/normalize-space()
        let $type := map:entry("type", $f_type)
        let $subtype := map:entry("subtype", $f_subtype)
        let $variation := 
            if ($doc//tei:catRef[@n eq "format-variation"][@target]) 
            then map:entry("variation", $f_variation) 
            else ()
        
        (: select provenance more likely when there are two, or the first :)
        (: todo: change, select in priority nome when twice the same :)
        let $p := if (count($prov//tei:p) = 1) then $prov//tei:p
            else if ($prov//tei:p[1][not(tei:placeName[@cert eq "low"])]
                    and $prov//tei:p[2]/tei:placeName[@cert eq "low"])
            then $prov//tei:p[1]
            else if ($prov//tei:p[2][not(tei:placeName[@cert eq "low"])]
                    and $prov//tei:p[1]/tei:placeName[@cert eq "low"])
            then $prov//tei:p[2]
            else $prov//tei:p[1]
        
        (: properties :)
        let $origPlace := $doc//tei:origPlace/normalize-space()
        let $provenance := if ($prov/@type) then $prov/@type/string() else "unknown"
        let $title := gramm:title($doc)
        let $settlement := $p//tei:placeName[not(@subtype)]
        let $nome := $p//tei:placeName[@subtype eq "nome"]
        let $region := $p//tei:placeName[@subtype eq "region"]
        
        (: settlement / nome / region as map entries :)
        let $s := if ($settlement) then map:entry("settlement", $settlement/string()) else ()
        let $n := if ($nome) then map:entry("nome", $nome/string()) else ()
        let $r := if ($region) then map:entry("region", $region/string()) else ()
        
        (: coordinates :)
        (: try use switch? :)
        let $coord_default_unknown := [ 29.157504099106227, 34.1739331756946 ]
        let $coord_default_egypt := ()
        let $level := 
            if (exists(local:coord_settlement($settlement))) 
            then "settlement" 
            else if (exists(local:coord_nome($nome))) then "nome"
            else ()
        
        let $coord_data :=
            switch ($level)
            case "settlement" return ($settlement/normalize-space(), local:coord_settlement($settlement))
            case "nome" return ($nome/normalize-space(), local:coord_nome($nome))
            default return ("Unknown", $coord_default_unknown)
        
        (: provisional datation info. 
        using only first origDate in case of multiple choices :)
        let $origDate := string-join($doc//tei:origDate/normalize-space(), ",<br/>")
        let $notBefore := $doc//tei:origDate[1]/@notBefore/string()
        let $notAfter := $doc//tei:origDate[1]/@notAfter/string()
        let $when := $doc//tei:origDate[1]/@when/string()
        
        (: terminus post quem :)
        let $tpq := if($when) then $when 
            else if($notBefore) then $notBefore
            else  
                let $date_nA := gramm:string-to-date($notAfter)
                return 
                    if(year-from-date($date_nA) eq 0050) 
                    then $date_nA - xs:yearMonthDuration("P49Y") (: avoid year 0 :)
                    else $date_nA - xs:yearMonthDuration("P50Y")
        
        (: terminus ante quem :)
        let $taq := if($when) then $when
            else if($notAfter) then $notAfter
            else  
                let $date_nB := gramm:string-to-date($notBefore)
                return 
                    if(year-from-date($date_nB) eq -0050) 
                    then $date_nB - xs:yearMonthDuration("P49Y") (: avoid year 0 :)
                    else $date_nB - xs:yearMonthDuration("P50Y")
        
        (: use tpq/taq to calculate :)
        let $cent_1 := xs:integer(local:get-era-minus(xs:date(gramm:string-to-date($tpq))) || local:get-century(xs:date(gramm:string-to-date($tpq))))
        let $cent_2 := xs:integer(local:get-era-minus(xs:date(gramm:string-to-date($taq))) || local:get-century(xs:date(gramm:string-to-date($taq))))
        let $centuries_list := for $i in $cent_1 to $cent_2 return string($i)
        
        (: description :)
        let $provenance_list :=
            for $p in $prov//tei:p
            return
                string-join($p//tei:placeName/normalize-space(), ", ")||"<br/>"
        
        let $provenance_text := 
        if ($provenance eq "unknown") then "<b>Unknown provenance.</b><br/>" 
        else "<b>"|| functx:capitalize-first($provenance) || "</b>: " || string-join($provenance_list)
        
        let $description := 
        "<b>"|| "Typology" ||"</b>: " ||
        string-join(($f_subtype, $f_variation), ", ") || 
        "<br/><br/>" ||
         $provenance_text ||
        "<br/>" ||
        "<b>Date</b>: " || $origDate
        
        
        (: links :)
        let $links := $p//*/@ref/string() => string-join(" ") => tokenize(" ")
        
        return 
            map{
                "@id": $idno||"_"||$provenance,
                "type": "Feature",
                "properties":map:merge((
                    map:entry("url", "https://grammateus.unige.ch/doc/"||$idno),
                    map:entry("title", $title),
                    map:entry("label", "TM "||$idno_tm),
                    map:entry("origplace",$origPlace),
                    map:entry("origDate",$origDate),
                    map:entry("provenance",$provenance),
                    map:entry("centuries", $centuries_list=>functx:value-except("0")),
                    $s, $n, $r, $type, $subtype, $variation
                    )),
                "geometry": map{
                    "type": "Point",
                    "label": $coord_data[1],
                    "coordinates": $coord_data[2]
                },
                "when": map{
                    "timespans": [
                    map{
                      "start": map{ "in": $tpq},
                      "end" : map{"in": $taq}
                    }   
                    ]},
                "descriptions": [map{
                    "value":$description
                    }],
                "links":
                 (for $l in $links
                 return
                     map{
                         "type": "closeMatch",
                         "identifier": $l},
                map{
                    "type": "subjectOf",
                    "identifier": "https://trismegistos.org/text/"||$idno_tm},
                map{
                    "type": "subjectOf",
                    "identifier": "https://aquila.zaw.uni-heidelberg.de/hgv/"||$idno_hgv}
                 )
    }
    
}